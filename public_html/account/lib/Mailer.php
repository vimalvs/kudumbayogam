<?php
namespace Account;
class Mailer {
	
	public static function sendConfirmationEmail($user_email, $user, &$msg) {
		// Send Validation email
		$token_salt = UniqueID(6);
		$token_hash = $user->hash($user_email, $token_salt);
		$email = new \EmailTemplate(array('to' => array($user->name, $user_email), 'subject' => 'Confirm Email Address'));
		$email->setTemplate(ROOT . '/account/templates/mail_account_confirm_email.tpl.php')->addData(compact('token_salt', 'token_hash', 'user_email') + $user->getData());
		$status = $email->send();
		// $tpl = \SiteManager::getTemplate();
		if ($status) {
			if ($user->verified) {
				$msg = "A confirmation email was sent to <strong>$user_email</strong>. Your email will not be changed until you confirm your email address.";
			} else {
				$msg = "A confirmation email was sent to <strong>$user_email</strong>. Please confirm your email address to enjoy the site fully.";
			}
		} else {
			$msg = 'Failed to send confirmation email. <a href="/account/resend-confirmation">Try Again</a>';
		}
		return $status;
	}
	
	public static function sendWelcomeEmail($user, &$msg) {
		// Send Validation email
		$token_salt = UniqueID(6);
		$token_hash = $user->hash($user->email, $token_salt);
		$user_email = $user->email;
		$email = new \EmailTemplate(array('to' => array($user->name, $user_email), 'subject' => "Just one more step to get started on " . SITE_NAME . ", " . $user->name));
		$email->setTemplate(ROOT . '/account/templates/mail_welcome_email.tpl.php')->addData(compact('token_salt', 'token_hash', 'user_email') + $user->getData());
		$status = $email->send();
		if ($status) {
			$msg = "A confirmation email was sent to <strong>{$user_email}</strong>. Please confirm your email address to enjoy the site fully.";
		} else {
			$msg = 'Failed to send confirmation email. <a href="/account/resend-confirmation">Try Again</a>';
		}
		return $status;
	}
	
	public static function sendPasswordResetEmail($user, &$msg) {
		$token = \Verification\Token::generate(array('user_email' => $user->email, 'user_id' => $user->id));
		if ($token) {
			$email = new \EmailTemplate(array('to' => array($user->name, $user->email), 'subject' => 'Reset your ' . SITE_NAME . ' password'));
			$email->setTemplate(ROOT . '/account/templates/mail_auth_password_retrieve.tpl.php')->addData($user->getData() + array('token' => $token));
			$status = $email->send();
		} else {
			$status = FALSE;
		}
		if ($status) {
			$msg = 'We\'ve sent you an email describing how to reset your password. The url in the email will expire after 48 hours. Please check your spam folder if the email doesn\'t appear within a few minutes.';
		} else {
			$msg = 'The verification email could not be sent. Please try again later.';
		}
		return $status;
	}
}