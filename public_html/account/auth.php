<?php
define('SCRIPT_NAME', '[MB] Auth Management');
define('REQUIRE_DB', 1);
define('REQUIRE_TEMPLATE', 1);
// define('ENABLE_MOBILE_REDIRECTION', 1);

use \Auth\UserAuth as UserAuth;

include '../include/prepend.php';
include '../include/password.inc.php';

header("X-Frame-Options: DENY");
$theme = Theme::load('Classic', 'Mobile', 'Basic');

$tpl = $theme->getTemplate();
$tpl->setTemplate(AJAX_REQUEST ? 'auth-ajax' : 'auth');
$tpl->setOption('print_message', PHPTemplate::PRINT_MESSAGE_GENERAL);

$arProviderAuthType = array('facebook' => UserAuth::AUTH_FACEBOOK, 'google' => UserAuth::AUTH_GOOGLE, 'yahoo' => UserAuth::AUTH_YAHOO);

extractCleanVars('mode', 'action', 'redirect');

if (empty($redirect)) $redirect = empty($_SERVER['HTTP_REFERER']) ? '/' : $_SERVER['HTTP_REFERER'];
if (strpos($redirect, '/account/auth') !== FALSE) $redirect = '/';
$redirect_decoded = htmlspecialchars_decode($redirect);

if (empty($mode)) $mode = 'auth';
if ($mode === 'auth' && empty($action)) $action = 'login';

$tpl_view = str_replace('-', '_', "{$mode}_{$action}");
$userAuth = \Auth\Session::getInstance();

if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST' && ($mode !== 'oauth2' || $action === 'auth')) {
	$request_nonce = getCleanPostVar('nonce');
	if (!\Verification\Nonce::verify($request_nonce, ($tpl_view === 'auth_check_email') ? 'account_register' : $tpl_view)) {
		$tpl->addMessage('Request session expired. Failed to validate the request, retry.');
	}
}

if ($tpl->hasError()) {
	// Do Nothing
} elseif ($mode === 'auth') {
	if (empty($action)) $action = 'login';
	if ($action === 'login') {
		/**
		 * Authentication :: Login User
		 */
		extractCleanPostVars('auth_email', 'auth_password', 'auth_remember', 'auth_captcha', 'format', 'p');
		$tpl_view = 'auth_login';
		if ($format) \SiteManager::addOutputFormat(array('json', 'jsonp'));
		if ($p) {
			// Logout User, if already logged in
			\Auth\Session::logout();
			
			// Check for brute force block
			// $user_locked = \Cache::getInstance()->get("locked_$auth_email", 'user') && !ReCaptcha::check();
			$auth = UserAuth::check(UserAuth::TYPE_REGISTERED, array($auth_email, $auth_password), null, $errCode);
			
			if ($auth) {
				$userAuth = \Auth\Session::reload($auth);
				if ($auth_remember) $userAuth->memorize();
				$_SESSION['loginData'] = array('status' => 1, 'message' => 'Welcome back ' . $userAuth->name);
				if ($format && $format === 'json') {
					$tpl->addData(array('user' => $userAuth->getUserData()));
				} elseif (AJAX_REQUEST) {
					echo '<script>$(document).trigger("auth_status_change", ' . json_encode($userAuth->getUserData()) . ');</script>';
					exit;
				} else {
					ResponseManager::redirect($redirect_decoded, NULL, NULL, 302);
				}
			} elseif ($errCode === UserAuth::ERR_AUTH_BAN_TEMP) {
				$ts = UserAuth::isLocked(null, $auth_email);
				$tpl->addMessage('Account has been locked due to too many incorrect login attempts. Please wait for ' . ceil($ts / 60) . ' minutes or try <em>forgot password</em>.');
			} else {
				$tpl->addMessage('Incorrect email address / password');
			}
		}
	} elseif ($action === 'logout') {
		/**
		 * Authentication :: Logout User
		 */
		\Auth\Session::logout();
		if (AJAX_REQUEST) {
			$tpl->addMessage('You have successfully logged out.', 'success');
		} else {
			$tpl->addMessage('You have successfully logged out. <a href="' . $redirect . '">Go Back</a>', 'success');
		}
		$tpl_view = 'auth_login';
		$auth_email = $auth_remember = '';
	} elseif ($action === 'check-email') {
		/**
		 * Check Email Availability
		 */
		header('Content-type: 	application/json');
		$available = !UserAuth::loadByIdentifier(UserAuth::AUTH_EMAIL, getCleanVar('user_email'));
		echo json_encode(array('status' => $available));
		exit;
	} elseif ($action === 'status') {
		/**
		 * Authentication :: Check Status
		 */
		if (isset($_SESSION['loginData'])) {
			if ( isset($_SESSION['loginData']['redirect']) ) {
				$arData = array('redirect' => $_SESSION['loginData']['redirect']);
			} else {
				$arData = $_SESSION['loginData'];
				unset($_SESSION['loginData']);
			}
		} elseif ($userAuth->isLoggedIn()) {
			$arData = array('status' => true, 'name' => "Welcome back {$userAuth->name}");
		} 
		if ( empty($arData) ) {
			$arData = array('status' => false, 'message' => 'Login Failed. Please try again.');
		}
		header('Content-type: 	application/json');
		echo json_encode($arData);
		exit;
	} elseif ($action === 'add') {
		if (!$userAuth->isLoggedIn()) ResponseManager::redirect('/account/auth');
	}
	$tpl->addData(compact('user_email', 'auth_remember', 'p'));
} elseif ($mode === 'account') {
	$p = getCleanPostVar('p');
	if ($action === 'register') {
		/**
		 * Account :: Register
		 */
		extractCleanPostVars('user_email', 'user_name', 'auth_password', 'auth_confirm');
		if ($p) {
			// Logout User, if already logged in
			if ($userAuth->isRegistered()) \Auth\Session::logout();

			$arError = array();
			if (empty($auth_password)) $arError['auth_password'] = 'Password cannot be empty';
			elseif (strlen($auth_password) < 6) $arError['auth_password'] = 'Password should have a minimum of 6 characters';
			elseif ($auth_password !== $auth_confirm) $arError['auth_password'] = 'Passwords do not match';
			
			if (UserAuth::loadByIdentifier(UserAuth::AUTH_EMAIL, $user_email)) {
				$arError['user_email'] = 'Email address already taken';
			}
			$arUserData = array('user_email' => $user_email, 'user_name' => $user_name, 'group_id' => 4, 'user_is_registered' => 1);
			if ($userAuth->isLoggedIn() && ($userAuth->email === $user_email)) {
				// Connect with guest account
				$user = \Auth\UserManager::loadById($userAuth->id);
			} else {
				// Create new account
				$user = new \Auth\UserManager();
			}
			if ($user->update($arUserData, $arError)) {
				$auth = $user->addAuth(UserAuth::AUTH_EMAIL, $user_email);
				$user->setPassword($auth_password);
				
				// Send Welcome email
				$status = \Account\Mailer::sendWelcomeEmail($user, $msg);
				// $tpl->addMessage($msg, $status ? 'info' : 'warning');
				
				if ($auth) {
					$userAuth = \Auth\Session::reload($auth);
					$userAuth->memorize();
					if (AJAX_REQUEST) {
						echo '<script>$(document).trigger("auth_status_change", ' . json_encode($userAuth->getUserData()) . ');</script>';
						exit;
					} else {
						ResponseManager::redirect($redirect_decoded, NULL, NULL, 302);
					}
				} else {
					$tpl->addMessage($arError[0]);
				}
			} else {
				$tpl->addMessage('Please correct the errors below to continue');
				foreach ($arError as $k => $v) {
					if (is_string($k)) $tpl->addNamedMessage($k, $v);
					else $tpl->addMessage($v);
				}
			}
		} elseif ($userAuth->isLoggedIn()) {
			$user_email = $userAuth->email;
			$user_name = $userAuth->name;
		}
		$tpl->addData(compact('user_email', 'user_name', 'verify', 'verified'));
	} elseif ($action === 'retrieve-password') {
		/**
		 * Account :: Password Recovery - Send verification email
		 */
		if ($p) {
			$user_email = getCleanVar('email');
			if (empty($user_email)) $tpl->addMessage('Enter your email address');
			elseif (!isValidEmail($user_email)) $tpl->addMessage('Enter a valid email address');
			else {
				$user = \Auth\UserManager::loadByEmail($user_email);
				if ($user) {
					$status = \Account\Mailer::sendPasswordResetEmail($user, $msg);
					$tpl->addMessage($msg, $status ? 'info' : 'error');
				} else {
					$tpl->addMessage('The specified user account does not exist.');
				}
			}
		}
		$tpl->data['email'] = '';
	} elseif ($action === 'reset-password') {
		/**
		 * Account :: Password Recovery - Verify email account and reset password
		 */
		extractCleanVars('token');
		$data = \Verification\Token::load($token);
		if ($data) {
			$user = \Auth\UserManager::loadById($data['user_id']);
			if ($user) {
				if (!$user->verified) $user->update(array('user_verified' => 1));
				if ($p) {
					extractCleanPostVars('auth_password', 'auth_confirm');
					if ($auth_password !== $auth_confirm) $tpl->addMessage('Passwords do not match');
					else {
						if ($user->setPassword($auth_password, $errorInfo)) {
							// $redirect = empty($redirect) ? '/' : $redirect;
							$tpl->addMessage("Password changed successfully", 'success');
							UserAuth::unlock(null, $user->email);
							\Verification\Token::delete($token);
							$tpl->data['auth_email'] = $user->email;
							$tpl_view = 'auth_login';
						} else {
							$tpl->addMessage($errorInfo ? $errorInfo : 'Failed to update password. Please try again later.');
						}
					}
				}
				$tpl->addData(compact('redirect', 'token'))
					->addData(['user_name' => $user->name, 'user_email' => $user->email]);
			} else {
				$tpl->addMessage('Invalid user');
			}
		} else {
			$tpl->addMessage('Invalid link or token has expired. Try again.');
			$tpl_view = 'account_retrieve_password';
		}
	}
} elseif ($mode === 'connect') {
	extractCleanVars('provider');
	if (empty($provider)) {
		require INCLUDE_PATH . '/Hybrid/Auth.php';
		require INCLUDE_PATH . '/Hybrid/Endpoint.php';
		Hybrid_Endpoint::process();
	} else {
		include __DIR__ . '/config.php';
		// Logout User, if already logged in
		// 
		$popup = getCleanVar('popup');
		try {
			require INCLUDE_PATH . '/Hybrid/Auth.php';
			if (!empty($arProviderAuthType[$provider])) {
				$hybridauth = new Hybrid_Auth($arAuthProvider);
				$adapter = $hybridauth->authenticate($provider);
				$profile = $adapter->getUserProfile();
				$authType = $arProviderAuthType[$provider];
				$auth = UserAuth::check(UserAuth::TYPE_CONNECTED, $profile->identifier, $authType);
				if (!$auth) {
					$tpl_view = 'connect_accounts';
					$connect = getCleanVar('connect');

					$user_email = $profile->emailVerified;
					$user_name = !empty($profile->displayName) ? $profile->displayName : "{$profile->firstName} {$profile->lastName}";
					
					// Check if verified email address is available
					if (!empty($user_email)) {
						// Email address is verified, check if another account exists with the same email address
						$existingUser = \Auth\UserManager::loadByEmail($user_email);
						if ($existingUser) {
							$tpl->data['existing_user'] = $existingUser->getData();
							if (($connect === 'current') && ($existingUser->id === $userAuth->id)) {
								// Detected user is the currently logged in user
								$connect = 'existing';
							}
						} elseif (!$userAuth->isLoggedIn()) {
							// Guest user. No existing email address to connect.
							// Only available option is to create a new account
							$connect = 'new';
						} elseif (!$userAuth->isRegistered() && ($userAuth->email === $user_email)) {
							// Logged in as Unregistered user with same email address
							// There is no existing user with same email address. Connect with current account
							$connect = 'current';
						}
						
						$tpl->data['connected_user'] = compact('user_name', 'user_email');

						if ($connect === 'current' && $userAuth->isLoggedIn()) {
							// Set user as current user
							$user = \Auth\UserManager::loadById($userAuth->id);
							$arUserData = array();
							if (!$user->is_registered) {
								if (!empty($profile->birthYear) && !empty($profile->birthMonth) && !empty($profile->birthDay)) {
									$arUserData['user_birthday'] = $profile->birthYear . '-' . str_pad($profile->birthMonth, 2, '0', STR_PAD_LEFT) . '-' . str_pad($profile->birthDay, 2, '0', STR_PAD_LEFT);
								}
								$arGender = array('male' => 1, 'female' => 2);
								if (isset($arGender[$profile->gender])) {
									$arUserData['user_gender'] = $arGender[$profile->gender];
								}
								$arUserData['user_is_registered'] = 1;
								$arUserData['group_id'] = 4;
							}
							if ($user->email === $user_email) $arUserData['user_verified'] = 1;
							if (!empty($arUserData)) $user->update($arUserData);
							
							// if ($curUser->verified) {
							// 	$user = $curUser;
								if (!$existingUser) $user->addAuth(UserAuth::AUTH_EMAIL, $user_email);
							// } else {
							// 	$tpl->addMessage("Cannot add the new login to your current account since the account is not verified. [ <a target=\"_blank\" href=\"/account/auth/verify\">Verify Email</a> ]");
							// }	
						} elseif ($connect === 'existing' && $existingUser) {
							// Set user as the detected user with same email address
							$user = $existingUser;
							if (!$user->verified) $user->update(array('user_verified' => 1), $errorInfo);
						} elseif ($connect === 'new') {
							// Create new user and add login to it
							$arUserData = array('user_name' => $user_name, 'user_email'	=> $user_email, 'user_verified'	=> 1, 'user_is_registered' => 1, 'group_id' => 4);
							if (!empty($profile->birthYear) && !empty($profile->birthMonth) && !empty($profile->birthDay)) {
								$arUserData['user_birthday'] = $profile->birthYear . '-' . str_pad($profile->birthMonth, 2, '0', STR_PAD_LEFT) . '-' . str_pad($profile->birthDay, 2, '0', STR_PAD_LEFT);
							}
							$arGender = array('male' => 1, 'female' => 2);
							if (isset($arGender[$profile->gender])) {
								$arUserData['user_gender'] = $arGender[$profile->gender];
							}
							// Create user
							$newUser = new \Auth\UserManager($arUserData);
							if ($newUser->save($errorInfo)) {
								// If there is no existing user with same email address, add the email address as verified
								if (!$existingUser) $newUser->addAuth(UserAuth::AUTH_EMAIL, $user_email);
								// Set the user to newly created user
								$user = $newUser;
							} else {
								$tpl->addMessage('Failed to create account');
							}
						}

						if (!empty($connect) && !empty($user) && !$tpl->hasError()) {
							$auth = $user->addAuth($authType, $profile->identifier, (($authType == UserAuth::AUTH_FACEBOOK) ? $profile->displayName : $profile->emailVerified), $arError);
							if ($auth) {
								$userAuth = \Auth\Session::reload($auth);
								$userAuth->memorize();
								ResponseManager::redirect($redirect_decoded, NULL, NULL, 302);
							} else {
								$tpl->addMessage($arError[0]);
							}
						}
					} else {
						$tpl->addMessage("Failed to login. Please make sure that your $provider account is verified.");
					}
				}
				if ($auth) {
					$userAuth = \Auth\Session::reload($auth);
					$userAuth->memorize();
					$_SESSION['loginData'] = array('status' => 1, 'message' => 'Welcome back ' . $userAuth->name);
					if ($popup) {
						echo '<script>var p = opener||parent;p.$(p.document).trigger("auth_status_change", ' . json_encode($userAuth->getUserData()) . ');window.close();</script>';
						exit;
					} else {
						ResponseManager::redirect($redirect_decoded, NULL, NULL, 302);
					}
				} else {

				}
			} else {
				$tpl->addMessage('Unsupported identity provider');
			}
		} catch (Exception $e) {
			$tpl->addMessage('Failed to authenticate with identity provider');
			trigger_error($e->getMessage(), E_USER_WARNING);
		}
	}
} elseif ($mode === 'oauth2') {
	
	$storage = new Custom\OAuth2\Storage\Pdo(\SiteManager::getDatabase());
	$server = new OAuth2\Server($storage, [
		'enforce_state' => true,
		'allow_implicit' => false
	]);
		
	$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
	$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
	$server->addGrantType(new OAuth2\GrantType\UserCredentials($storage));
	
	$arAvailableScope = [
		'user_info' => ['Basic User Information (name, email)'],
		'astrology' => ['Access astrology api'],
	];
	
	$server->setScopeUtil(new OAuth2\Scope([
		'default_scope' => 'user_info',
		'supported_scopes' => ['user_info', 'astrology']
	]));
	
	if ($action === 'auth') {

		$client_id = getCleanVar('client_id');
		
		$request = OAuth2\Request::createFromGlobals();
		$response = new OAuth2\Response();
		
		$arRequestedScope = explode(' ', $request->query('scope', 'user_info'));
		
		foreach ($arRequestedScope as $v) {
			if (!isset($arAvailableScope[$v])) {
				$tpl->addMessage("Invalid scope - $v");
				break;
			}
			$arRequestedScopeInfo[$v] = $arAvailableScope[$v][0];
		}
		
		// validate the authorize request
		if (!$server->validateAuthorizeRequest($request, $response)) {
		    $response->send();
		    die;
		}
		\SiteManager::assert($userAuth->isLoggedIn(), ROOT . '/account/templates/auth.tpl.php');
		
		if (empty($_POST)) {
			
			$tpl->setTemplate('auth');
			$client = Custom\OAuth2\Client::loadById($client_id);
			if ($client) $tpl->addData($client->getData());
			
		} else {
			// print the authorization code if the user has authorized your client
			$is_authorized = ($_POST['authorized'] === 'yes');
			$server->handleAuthorizeRequest($request, $response, $is_authorized, $userAuth->id);
			if ($is_authorized) {
			  // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
			  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
			  exit("SUCCESS! Authorization Code: $code");
			}
			$response->send();
			exit;
		}
	} elseif ($action === 'token') {
		// Handle a request for an OAuth2.0 Access Token and send the response to the client
		$server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
		exit;
	}
}
$response_nonce = \Verification\Nonce::generate($tpl_view);

$tpl->data['generator'] = 'auth';
$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'response_nonce'));

$tpl->generate();
