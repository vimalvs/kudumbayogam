<?php
include '../../include/prepend.inc.php';
set_include_path(INCLUDE_PATH . PATH_SEPARATOR . get_include_path());

$state = getCleanVar('state');
$provider = getCleanVar('provider');
$baseUri = SITE_HOME . "/account/external/$provider";

if (!empty($_SESSION[$state])) {
	$redirectUri =  $baseUri;//"$baseUri?action=authorize";
	$request = $_SESSION[$state];
	
	$client_id = $request['client_id'];//'833623267104.apps.googleusercontent.com';
	$client_secret = $request['client_secret'];//'-OaAUkEMznKrw41qVfFslATh';
	$scopes = $request['scopes'];
	$redirect = $request['redirect'];
	
	if (!empty($scopes)) {
		$client = new Google_Client();

		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirectUri);
		$client->setScopes($scopes);
		$client->setState($state);
	
		$client->setAccessType('offline');
		$client->setApprovalPrompt('force');
	
		if (isset($_GET['code'])) {
			$client->authenticate($_GET['code']);
			$token = $client->getAccessToken();
			$_SESSION[$state]['access_token'] = $token;
			ResponseManager::redirect($redirect, compact('state'), null, null, 'POST');
		} else {
			header('Location: ' . $client->createAuthUrl());
		}
	
	} else {
		die('No scope defined');
		unset($_SESSION[$state]);
	}
	
} else {
	die('Invalid request key');
}

