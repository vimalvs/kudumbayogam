<?php
$redirect = isset($redirect) ? $redirect : '';
$redirect_encoded = empty($redirect) ? '' : ('?redirect=' . urlencode($redirect));
if (empty($mode)) {
	$mode = 'login';
	$tpl_view = 'auth_login';
	$generator = '';
}
$params = $redirect_encoded;
if (AJAX_REQUEST) {
	$params .= (empty($params) ? '?' : '&') . 'popup=1';
}
if (empty($response_nonce)) $response_nonce = \Verification\Nonce::generate($tpl_view);
?>
<link type="text/css" rel="stylesheet" href="<?=SITE_HOME?>/account/assets/css/style.css">
<link type="text/css" rel="stylesheet" href="<?=SITE_HOME?>/account/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
<div id="account-action" class="section <?=$tpl_view?>">
<div id="account-action-inner">
<script>
if ('placeholder' in document.createElement('input')) {
	document.getElementById('account-action').className += ' placeholder-supported';
	
}
</script>
<?php if ($tpl_view === 'account_register'):?>
	<div class="content-group">
		<h1>User Registration<i class="fa fa-edit fa-fw"></i></h1>
	</div>
	<form id="frmRegister" class="form-horizontal" action="<?=SITE_HOME?>/account/auth/register" method="post">
		<div class="content-group">
			<?php if ($generator !== 'auth'):?>
				<div class="alert alert-info">Please create a user account to continue. Once you are registered / logged in, you will be redirected back to the previous page to complete the current action.</div>
			<?php endif;?>
			<p class="account-return-info">Register for a new account. <small>Already registered? <a href="<?=SITE_HOME?>/account/auth">Login</a></small></p>
		
			<?php $this->printMessage();?>
			<?php TemplateRender::formField('Display Name', 'user_name', array('class' => 'account-input', 'attr' => ['required' => true, 'minlength' => 3, 'maxlength' => 20]), $this, 'Display Name');?>
			<?php TemplateRender::formField('Email Address', 'user_email', array('class' => 'account-input', 'type' => 'email', 'attr' => ['required' => true, 'maxlength' => 60]), $this, 'Email Address');?>
			<?php TemplateRender::formField('Password', 'auth_password', array('class' => 'account-input', 'type' => 'masked', 'attr' => ['required' => true]), $this, 'Password');?>
			<?php TemplateRender::formField('Confirm Password', 'auth_confirm', array('class' => 'account-input', 'type' => 'masked', 'attr' => ['required' => true]), $this, 'Confirm Password');?>
			<p><small>By creating an account, I accept the site's <a class="b" target="_blank" href="<?=SITE_HOME?>/terms.htm">Terms of Service</a> and <a class="b" target="_blank" href="<?=SITE_HOME?>/privacy.htm">Privacy Policy</a>. </small></p>
		</div>
		<div id="account-action-footer">
			<input type="hidden" name="p" value="1">
			<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
			<input type="hidden" name="nonce" value="<?=$response_nonce?>">
			<a id="btn-account-return" href="<?=SITE_HOME?>/account/auth" class="account-btn account-btn-link"><span>Already Registered?</span> Login</a>
			<button class="account-btn" type="submit">Create Account</button>
		</div>
	</form>
	<script>
	$(function(){
		var reEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		function showStatus($inp, msg, status) {
			var $cg = $inp.closest('.control-group').removeClass('success error');
			if (status === 2) {
				$cg.find('.help-inline').text('Checking...');
			}
			else $cg.addClass(status ? 'success' : 'error').find('.help-inline').html((status ? '&#x2714; ' : '&#x2718; ') + msg);
			// else $cg.find('.help-inline').html('');
		}
		var ajaxData = {email : ''}, nonce = $('#frmRegister input[name="nonce"]').val();
		function checkInput(e) {
			var $inp = this.id === 'frmRegister' ? $('.account-input', this) : $(this);
			var frmStatus = true;
			$inp.each(function() {
				var status = '', msg = '', $t = $(this), val = this.value, len = this.value.length;
				if (this.name === 'user_email') {
					console.log(/^.{8,60}$/.test(val));
					if (!val) msg = required;
					else if (!reEmail.test(val)) msg = 'Invalid';
					else if (!/^.{7,60}$/.test(val)) msg = len < 7 ? 'Too short' : 'Too long';
					else if (val !== ajaxData.email) {
						if ($t.xhr) $t.xhr.stop();
						$t.xhr = $.post('/account/auth/check-email', {user_email : val, nonce: nonce}, function (data) {
							ajaxData = {email : val, status : data.status, msg : (data.status ? 'Available' : 'Email taken')};
							showStatus($t, ajaxData.msg, ajaxData.status);
						}, 'json');
						status = 2;
					} else {
						msg = ajaxData.msg;
						status = ajaxData.status;
					}
				} else if (this.name === 'user_name') {
					if (!len) msg = 'Required';
					else if (len < 3) msg = 'Too short';
					else if (len > 20) msg = 'Too long';
				} else if (this.name === 'auth_password') {
					if (len < 6) msg = 'Too short';
				} else if (this.name === 'auth_confirm') {
					status = ((len >= 6) && (val === $('#fin_auth_password', '#frmRegister').val()));
					if (!status) msg = (len >= 6) ? 'Mismatch' : '';
				}
				if (status === '') status = !msg.length;
				frmStatus = frmStatus && status;
				showStatus($t, msg, status);
			});
			if (!frmStatus) e.preventDefault();
		}
		$('#frmRegister').submit(checkInput).find('.account-input').blur(checkInput).each(function(){
			var $t = $(this);
			if ($t.siblings('.help-inline').length == 0) $t.after('<span class="help-inline"></span>');
		})
	})
	</script>
<?php elseif ($tpl_view === 'account_retrieve_password'):?>
	<div class="content-group">
		<h1>Reset your password</h1>
	</div>
	<form class="form-horizontal" action="<?=SITE_HOME?>/account/auth/retrieve-password" method="post">
		<div class="content-group">
			<p>Enter your email address. <small>No account? <a href="<?=SITE_HOME?>/account/auth/register<?=$redirect_encoded?>">Register</a></small></legend>
			<?php $this->printMessage();?>
			<?php TemplateRender::formField('Email Address', 'email', array('class' => 'account-input', 'type' => 'email', 'attr' => ['required' => true]), $this, 'Email Address');?>
			<p>If you have forgotten your password, we will send you an email to reset your password.</p>
		</div>
		<div id="account-action-footer">
			<input type="hidden" name="p" value="1">
			<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
			<input type="hidden" name="nonce" value="<?=$response_nonce?>">
			<a id="btn-account-return" href="<?=SITE_HOME?>/account/auth" class="account-btn account-btn-link">Login</a>
			<button class="account-btn" type="submit">Continue &raquo;</button>
		</div>
	</form>
<?php elseif ($tpl_view === 'account_reset_password'):?>
	<div class="content-group">
		<h1>Reset your password</h1>
	</div>
	<form class="form-horizontal" action="<?=SITE_HOME?>/account/auth/reset-password" method="post">
		<div class="content-group">
			<p>Welcome <strong><?=$user_name?></strong>, your login id is <strong><?=$user_email?></strong>.</p>
			<p>Choose your new password below. <small><a href="<?=SITE_HOME?>/account/auth<?=$redirect_encoded?>">Login</a></small></p>
			<?php $this->printMessage();?>
			<?php TemplateRender::formField('Password', 'auth_password', array('value' => '', 'class' => 'account-input', 'type' => 'masked', 'attr' => ['required' => true]), $this, 'Password');?>
			<?php TemplateRender::formField('Verify password', 'auth_confirm', array('value' => '', 'class' => 'account-input', 'type' => 'masked', 'attr' => ['required' => true]), $this, 'Verify Password');?>
		</div>
		<div id="account-action-footer">
			<input type="hidden" name="p" value="1">
			<input type="hidden" name="token" value="<?=htmlspecialchars($token)?>">
			<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
			<input type="hidden" name="nonce" value="<?=$response_nonce?>">
			<input type="hidden" name="user_email" value="<?=htmlspecialchars($user_email)?>">
			<a id="btn-account-return" href="<?=SITE_HOME?>/account/auth" class="account-btn account-btn-link">Login</a>
			<button class="account-btn" type="submit">Update Password</button>
		</div>
	</form>
<?php elseif ($tpl_view === 'connect_accounts'):?>
	<div class="content-group">
		<h1>Connect Account</h1>
	</div>
	<form class="form-horizontal" action="<?=SITE_HOME?>/account/auth/add" method="post">
		<div class="content-group">
			<p>This login is new to the site. What would you like to do?</p>
			<?php $this->printMessage();?>
			<ul id="account-connect-list" class="list-accounts thumbnails">
				<?php if ($this->user->isLoggedIn()):?>
					<li>
						<a class="thumbnail" href="<?=SITE_HOME?><?=$_SERVER['REQUEST_URI']?>&amp;connect=current">
							Connect with the account I am currently logged in to<br/>
							<h6><?=$this->user->name?> [<?=$this->user->email?>]</h6>
						</a>
					</li>
				<?php endif;?>
				<?php if (!empty($existing_user) && ($existing_user['user_id'] != $this->user->id)):?>
					<li>
						<a class="thumbnail" href="<?=SITE_HOME?><?=$_SERVER['REQUEST_URI']?>&amp;connect=existing">
							Connect with my existing account:<br/>
							<h6><?=$existing_user['user_name']?> [<?=$existing_user['user_email']?>]</h6>
						</a>
					</li>
				<?php endif;?>
				<li>
					<a class="thumbnail" href="<?=SITE_HOME?><?=$_SERVER['REQUEST_URI']?>&amp;connect=new">I would like to create a new account
						<h6><?=$connected_user['user_name']?> [<?=$connected_user['user_email']?>]</h6>
					</a>
				</li>
			</ul>
		</div>
		<div id="account-action-footer" style="text-align:left;">
			<a id="btn-account-return" style="float:none;" href="<?=SITE_HOME?>/account/auth<?=$redirect_encoded?>" class="account-btn account-btn-link">Try another login?</a>
		</div>
	</form>
<?php elseif ($tpl_view === 'auth_add'):?>
	<div class="content-group">
		<h1>Add Login</h1>
	</div>
	<div class="content-group">
		<?php if (!empty($message)):?>
			<div class="alert block-alert alert-info">
				<p><?=$message?></p>
			</div>
		<?php elseif ($generator === 'auth'):?>
			<?php if ($this->user->isLoggedIn()):?>
				<div class="alert alert-block alert-info">
					<h4 class="alert-heading">Hi <?=$this->user->name?>!</h4>
					<p>You are currently signed in as <?=$this->user->name?>. Login with one of the accounts below to connect it with your current account.</p>
				</div>
			<?php endif;?>
		<?php endif;?>
		<p>Connect using</p>
		<?php if (!empty($provider)) $this->printMessage();?>
		<?php $target = AJAX_REQUEST ? 'target="_connect"' : '';?>
		<ul id="federated-login">
			<li><a class="button-facebook" <?=$target?> href="<?=SITE_HOME?>/account/auth/facebook<?=($tmp = qsAddParam($params, 'connect=current'))?>">Connect with Facebook</a></li>
			<li><a class="button-googleplus" <?=$target?> href="<?=SITE_HOME?>/account/auth/google<?=$tmp?>">Connect with Google</a></li>
		</ul>
	</div>
	<div id="account-action-footer" style="text-align:left;">
		<a id="btn-account-return" style="float:none;" href="<?=$redirect?>" class="account-btn account-btn-link">&larr; Back</a>
	</div>
	<?php if (AJAX_REQUEST):?>
	<script>
	$(function(){
		var win = null;
		$('#federated-login a').attr('data-skip', 'skip').click(function(e){
			if (win) win.close();
			win = window.open(this.href, '_current', 'location=0, status=0, width=800, height=500');
			if (!win.opener) win.opener = self;
			win.focus();
			return false;
		});
	});
	</script>
	<?php endif;?>
	
<?php elseif ($tpl_view === 'oauth2_auth'):?>
	<div class="content-group">
		<h1>Authorize Request</h1>
	</div>
	<form class="form-horizontal" action="<?=SITE_HOME?><?=$_SERVER['REQUEST_URI']?>" method="post">
		<div class="content-group">
			<?=$client_name?>
			<ul>
			</ul>
		</div>
		<div id="account-action-footer">
			<button class="account-btn" type="submit" name="authorized" value="no">Cancel</button>
			<button class="account-btn" type="submit" name="authorized" value="yes">Authorize</button>
		</div>
	</form>
	
<?php else:?>
	
	<div class="content-group">
		<h1>User Login <i class="fa fa-fw fa-user"></i></h1>
	</div>

	<div class="content-group">
		<?php if (!empty($message)):?>
			<div class="alert block-alert alert-info">
				<p><?=$message?></p>
			</div>
		<?php elseif ($generator === 'auth'):?>
			<?php if ($this->user->isRegistered()):?>
				<div class="alert alert-block alert-info">
					<h4 class="alert-heading">Hi <?=$this->user->name?>!</h4>
					<p>You are currently signed in as <?=$this->user->name?> and will be automatically <a href="<?=SITE_HOME?>/account/auth/logout<?=$redirect_encoded?>">signed out</a> of your current session, if you continue.</p>
				</div>
			<?php endif;?>
		<?php elseif ($this->user->isLoggedIn()):?>
			<div class="alert alert-block alert-error">
				<button data-dismiss="alert" class="close">×</button>
				<h4 class="alert-heading">Apologies <?=$this->user->name?>!</h4>
				<p class="help-block">You don't have the necessary privileges to perform this action.</p>
			</div>
		<?php else:?>
			<div class="pad" style="background-color: #25ACD9;padding: 10px;margin-bottom: 5px;">
				<img width="100%" src="/theme/classic/assets/images/logo.png" alt="logo">
			</div>
		<?php endif;?>

		<?php if (!empty($provider)) $this->printMessage();?>
		<?php $target = AJAX_REQUEST ? 'target="_tab"' : '';?>
	</div>

	<form class="form-horizontal" action="<?=SITE_HOME?>/account/auth" method="post">

		<div class="content-group">
			<?php if (empty($provider)) $this->printMessage();?>
			<div class="control-group ">
				<label class="control-label" for="fin_auth_email">Email Address</label>
				<div class="controls">
					<div class="input-group mb-2 mb-sm-0">
				        <div class="input-group-addon"><i class="fa fa-fw fa-envelope"></i></div>
				        <input name="auth_email" type="email" id="fin_auth_email" class="account-input" required="1" placeholder="Email Address" value="">
			     	</div>
				</div>
			</div>
			<div class="control-group ">
				<label class="control-label" for="fin_auth_password">Password</label>
				<div class="controls">
					<div class="input-group mb-2 mb-sm-0">
				        <div class="input-group-addon"><i class="fa fa-fw fa-lock"></i></div>
				        <input name="auth_password" type="password" id="fin_auth_password" class="account-input" required="1" placeholder="Password" value="">
			     	</div>
				</div>
			</div>
			<p style="text-align:left;font-size:0.8em;"><a class="b" href="<?=SITE_HOME?>/account/auth/retrieve-password<?=$redirect_encoded?>">Forgot Password</a> | <a class="b" href="<?=SITE_HOME?>/account/auth/register<?=$redirect_encoded?>">Quick Signup</a></p>
			<label style="margin:10px 0 0 10px;" class="checkbox"><input type="checkbox" name="auth_remember" value="1" <?=(empty($p) || !empty($auth_remember)) ? 'checked' : ''?>> Remember me</label>
		</div>
		<div id="account-action-footer">
			<button class="account-btn" type="submit">Sign in</button>
			<input type="hidden" name="p" value="1">
			<input type="hidden" name="nonce" value="<?=$response_nonce?>">
			<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
		</div>
	</form>
	<?php if (AJAX_REQUEST):?>
		<script>
		<?php if ($action === 'logout'):?>$(document).trigger("auth_status_change", <?=json_encode($this->user->getUserData())?>);<?php endif;?>
		$(function(){
			var win = null;
			$('#federated-login a').click(function(e){
				if (win) win.close();
				win = window.open(this.href, 'current', 'location=0, status=0, width=800, height=500');
				if (!win.opener) win.opener = self;
				win.focus();
				return false;
			}).attr({target : 'current', 'data-skip' : 'skip'});
		});
		</script>
	<?php endif;?>
<?php endif;?>
</div>
</div>
