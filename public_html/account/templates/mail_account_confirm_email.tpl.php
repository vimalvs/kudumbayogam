Hi <?=$user_name?>,

You have recently let us know that you need to reset your password.  Please follow this link: <?=SITE_HOME?>/account/confirm-email/<?=$user_id?>-<?=$token_salt?>-<?=$token_hash?>?email=<?=urlencode($user_email)?> 
(If clicking the link did not work, try copying and pasting it into your browser.)

If you did not request to reset your password, please disregard this message.  This link will automatically expire within 48 hours.

Thanks,
The <?=SITE_NAME?> Team
