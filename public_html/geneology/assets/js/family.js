/*var config = {
        container: "#basic-example",
        
        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    },
    ceo = {
        text: {
            name: "Padiyara Vallikattu",
        },
        image: "/assets/imgs/family-1.jpg"
    },

    cto = {
        parent: ceo,
        text:{
            name: "Padiyara",
            title: "Main Stream",
        },
        // stackChildren: true,
        image: "/assets/imgs/family-1.jpg"
    },
    cbo = {
        parent: ceo,
        // stackChildren: true,
        text:{
            name: "Vallikattu",
            title: "Main Stream",
        },
        image: "/assets/imgs/family-1.jpg"
    },
    cdo = {
        parent: cto,
        text:{
            name: "Padiyara",
        },
        image: "/assets/imgs/family-1.jpg"
    },
    cio = {
        parent: cto,
        text:{
            name: "Karumankal",
        },
        image: "/assets/imgs/family-1.jpg"
    },
    ciso = {
        parent: cto,
        text:{
            name: "Thekkekara",
        },
        image: "/assets/imgs/family-1.jpg"
    }

    chart_config = [
        config,
        ceo,
        cto,
        cbo,
        cdo,
        cio,
        ciso,
    ];

*/


    // Antoher approach, same result
    // JSON approach


    var chart_config = {
        chart: {
            container: "#basic-example",
            
            connectors: {
                type: 'step'
            },
            node: {
                HTMLclass: 'nodeExample1'
            }
        },
        nodeStructure: {
            text: {
                name: "Mark Hill",
                title: "Chief executive officer",
                contact: "Tel: 01 213 123 134",
            },
            image: "/assets/imgs/family-1.jpg",
            children: [
                {
                    text:{
                        name: "Joe Linux",
                        title: "Chief Technology Officer",
                    },
                    stackChildren: true,
                    image: "/assets/imgs/family-1.jpg",
                    children: [
                        {
                            text:{
                                name: "Ron Blomquist",
                                title: "Chief Information Security Officer"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        },
                        {
                            text:{
                                name: "Michael Rubin",
                                title: "Chief Innovation Officer",
                                contact: "we@aregreat.com"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        }
                    ]
                },
                {
                    stackChildren: true,
                    text:{
                        name: "Linda May",
                        title: "Chief Business Officer",
                    },
                    image: "/assets/imgs/family-1.jpg",
                    children: [
                        {
                            text:{
                                name: "Alice Lopez",
                                title: "Chief Communications Officer"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        },
                        {
                            text:{
                                name: "Mary Johnson",
                                title: "Chief Brand Officer"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        },
                        {
                            text:{
                                name: "Kirk Douglas",
                                title: "Chief Business Development Officer"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        }
                    ]
                },
                {
                    text:{
                        name: "John Green",
                        title: "Chief accounting officer",
                        contact: "Tel: 01 213 123 134",
                    },
                    image: "/assets/imgs/family-1.jpg",
                    children: [
                        {
                            text:{
                                name: "Erica Reel",
                                title: "Chief Customer Officer"
                            },
                            link: {
                                href: "http://www.google.com"
                            },
                            image: "/assets/imgs/family-1.jpg"
                        }
                    ]
                }
            ]
        }
    };

