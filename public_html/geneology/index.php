<?php
include '../include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$mode = getCleanVar('mode', 'home');

// function getTree($arFamily) {
// 	$arFamilyTree = [];
// 	foreach ($arFamily as $fkey => $fvalue) {
// 		$arTreeId = explode(".", $fvalue['tid']);
// 		foreach ($arTreeId as $tkey => $tvalue) {
// 			if (isset($arFamilyTree[$tvalue])) {
// 				return getTree($arFamily);
// 			} else {
// 				$arFamilyTree[$tvalue] = $fvalue;
// 			}
// 		}
// 	}
// 	return $arFamilyTree;
// }

function getTree($id, &$ar1, &$ar2){
	$arM = array();
	$arM['text'] = (object)[
		'Family Name' => $ar1[$id]['family_name'],
	];
	$arM['image'] = "/assets/imgs/family-1.jpg";
	$arM['link'] = (object)[
		"href" => "/geneology/{$ar1[$id]['family_pathname']}-{$ar1[$id]['id']}.html"
	];
	if(isset($ar2[$id]) && is_array($ar2[$id])){
		foreach($ar2[$id] as $a){
			$arM['children'][] = getTree($a['id'], $ar1, $ar2);
		}
	}else{
		// $arM[1] = array();
	}
	return $arM;
}

function getMemberTree($id, &$ar1, &$ar2){
	// printr($ar1, $ar2);
	// exit;
	$arM = array();
	$arM['text'] = (object)[
		'Member Name' => $ar1[$id]['member_name'],
	];
	$arM['image'] = "/assets/imgs/member.png";
	$arM['link'] = (object)[
		"href" => "/members/{$ar1[$id]['member_pathname']}-{$ar1[$id]['id']}.html"
	];
	if(isset($ar2[$id]) && is_array($ar2[$id])){
		foreach($ar2[$id] as $a){
			$arM['children'][] = getMemberTree($a['member_uid'], $ar1, $ar2);
		}
	}else{
		// $arM[1] = array();
	}
	return $arM;
}

if ($mode === 'home') {
	
	$tpl->setTemplate('home');

	$stmt = $pdbo->search('family', NULL, array('id', 'ASC'), null, ['id', 'parent_id', 'family_name', 'family_pathname']);	

	$arFamily = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($arFamily as $fkey => $fvalue) {
		$arFamily1[$fvalue['id']] = $fvalue;
		$arFamily2[$fvalue['parent_id']][] = $fvalue;
	}

	$arFamilyTree['chart'] = (object)[
		"container" => "#basic-example",
		"connectors" => (object)[
			"type" => "step",
		],
		"node" => (object)[
			"HTMLclass" => "nodeExample1",
		],
	];
	$arFamilyTree['nodeStructure'] = (object)getTree(1, $arFamily1, $arFamily2);
} elseif($mode === 'family') {
	
	$tpl->setTemplate('geneology');
	
	$family_id = getCleanVar('family_id', 10);
	
	$stmt = $pdbo->search('members', ['family_id' => $family_id], array('id', 'ASC'), null, ['id', 'member_uid', 'member_pathname', 'parent_id', 'member_name']);	
	$arFamily = $stmt->fetchAll();

	foreach ($arFamily as $fkey => $fvalue) {
		$arFamily1[$fvalue['member_uid']] = $fvalue;
		$arFamily2[$fvalue['parent_id']][] = $fvalue;
	}
	// printr($arFamily);
	// printr($arFamily1);
	// printr($arFamily2);

	$arFamilyTree['chart'] = (object)[
		"container" => "#basic-example",
		"connectors" => (object)[
			"type" => "step",
		],
		"node" => (object)[
			"HTMLclass" => "nodeExample1",
		],
	];

// printr($stmt,$arFamily,$arFamily1,$arFamily2);
	
	// exit;
	$arFamilyTree['nodeStructure'] = (object)getMemberTree(1, $arFamily1, $arFamily2);

}
// printr($arFamilyTree);

// exit;

function getShortName($person){
	$shortName = '';
	$i = 0;
	$arName = explode(' ', strtoupper($person));
	$len = count($arName);
	do{
		$sLen = strlen($arName[$i]);
		if($sLen > 3){
			$shortName .= $arName[$i] . ' ';
			break;
		}elseif(in_array($arName[$i], array('DR.', 'SR.', 'JR.', 'SIR')) || substr($arName[$i], 1, 1) == '.' || $sLen == 1){
			$shortName .= '';
		}else{
			$shortName .= $arName[$i] . ' ';
		}
	}while(++$i < $len);
	return $shortName;
}


$tpl->addData(compact('arFamilyTree'));

$tpl->generate();
