<?php
// printr($arFamilyTree);
// printr(json_encode($arFamilyTree));
// exit;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Padiyara Vallikattu Family Genealogy</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Padiyara Vallikattu Family Genealogy..." />
<?php $this->render('theme::headContent');?>
<link rel="stylesheet" href="/geneology/assets/js/treant-js-master/Treant.css">

<style>
.Treant > .node {  }
.Treant > p { font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-weight: bold; font-size: 12px; }
.node-name { font-weight: bold;}

.nodeExample1 {
    padding: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background-color: #ffffff;
    border: 1px solid #000;
    width: 200px;
    font-family: Tahoma;
    font-size: 15px;
}
.nodeExample1 p {
	margin: 2px;
	text-align: left;
}

.nodeExample1 img {
    margin:  10px;
}
.node img {
	width: 30px;
	height: 30px;	
}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
    		<div class="text-center">
				<span class="t-xlarge">Welcome To</span>	
				<h1>PADIYARA VALLIKATTU FAMILY GENEALOGY</h1>
				<hr class="h-underline">
			</div>

    		<div class="chart" id="basic-example"></div>

		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>

<script src="/geneology/assets/js/treant-js-master/vendor/raphael.js"></script>
<script src="/geneology/assets/js/treant-js-master/Treant.js"></script>

<!-- <script src="/geneology/assets/js/family.js"></script> -->
<script>

var chart_config = <?php echo json_encode($arFamilyTree)?>;
new Treant( chart_config );
</script>

</body>
</html>