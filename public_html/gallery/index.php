<?php
include '../include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$mode = getCleanVar('mode', 'list');

if ($mode === 'list') {
	$tpl->setTemplate('home');
	$stmt = $pdbo->search("gallery_categories", null, ['position', 'ASC']);
	$arCategories = $stmt ? $stmt->fetchAll() : [];

	$tpl->addData(compact('arCategories'));
} else if ($mode === 'gallery-category') {
	extractCleanVars('id');
	$tpl->setTemplate('gallery');
	$stmt = $pdbo->search("gallery_categories", ['id' => $id]);
	$category = $stmt ? $stmt->fetch() : [];
	$path_name = "https://padiyaravallikattu-kudumbayogam.com/gallery/".$category['category_pathname']."-".$category['id'].".html";
	$stmt = $pdbo->search("gallery_images", ['gid' => $id], ['image_order', 'ASC']);
	$tpl->data['arImage'] = $stmt ? $stmt->fetchAll() : [];

	$tpl->addData(compact('category', 'path_name'));
}

$tpl->addData(compact('arCategories'));

$tpl->generate();
