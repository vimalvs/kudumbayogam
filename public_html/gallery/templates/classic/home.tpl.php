<!DOCTYPE html>
<html lang="en">
<head>
<title>Padiyara Vallikattu Family Gallery</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Padiyara Vallikattu Family Gallery..." />
<?php $this->render('theme::headContent');?>
<style>
	.gallery-image-wrapper {
		border: 1px solid #ddd;
		text-align: center;
		position: relative;	
	}
	.image-title {
		text-align: center;
		background: #ddd;
		padding: 10px;
	}
	.gallery-overlay {
	  position: absolute;
	  bottom: 0;
	  left: 0;
	  right: 0;
	  background-color: #008CBA;
	  overflow: hidden;
	  width: 100%;
	  height: 0;
	  transition: .5s ease;
	}

	.gallery-image-wrapper:hover .gallery-overlay {
	  height: 100%;
	}
	.image-description {
		color: white;
		font-size: 20px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		text-align: center;
	}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div class="container">
		      	<div class="row p-3">
		      		<?php foreach($arCategories as $key => $category):?>
		      			<div class="col-xs-12 col-sm-6 col-md-4 p-3">
		      				<div class="gallery-image-wrapper">
								<img src="/assets/imgs/gallery_2.png" alt="gallery" class="gallery-icon" height="250" width="250">
								<div class="gallery-overlay">
									<div class="image-description">
										<?php if(!empty($category['description'])):?>
											<?=$category['description']?>
										<?php else:?>
											<?=$category['category']?>
		      								<br>(<?=$category['image_count']?> Images)
										<?php endif;?>
									</div>
								</div>
							</div>
							<div class="image-title">
		      					<a href="<?=$category['category_pathname']?>-<?=$category['id']?>.html"> 
		      						<?=$category['category']?>
		      						<br>(<?=$category['image_count']?> Images)		
	      						</a>
	      					</div>
		      			</div>
		      		<?php endforeach;?>
		      	</div>
	      	</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>