<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Verification;

class Nonce {
	
	private static $key = null;
	private static $requestId = null;
	
	public static function initialize() {
		self::$key = CRYPT_KEY;
	}
	
	private static function getRequestID() {
		if (!self::$requestId) {
			$userAuth = \Auth\Session::getInstance();
			$uid = $userAuth->uid;
			if (!$uid) {
				$uid = session_id() . '_' . $_SERVER['REMOTE_ADDR'];
			}
			self::$requestId = $uid;
		}
		return self::$requestId;
	}
	
	private static function getHash($data) {
		return substr(hash_hmac('md5', $data, self::$key), 15, 12);
	}
	
	public static function generate($action = null, $time = 3600) {
		$requestId = self::getRequestID();
		$tick = ceil(TIME_NOW / $time * 2);
		return self::getHash($action . $requestId . $tick);
	}
	
	public static function verify($nonce, $action = null, $time = 3600) {
		$requestId = self::getRequestID();
		$tick = ceil(TIME_NOW / $time * 2);
		return (self::getHash($action . $requestId . $tick) === $nonce) || (self::getHash($action . $requestId . ($tick - 1)) === $nonce);
	}
	
}

Nonce::initialize();