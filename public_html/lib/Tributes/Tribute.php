<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com    					      # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Page Class
 * 
 * Class for cms page related actions
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2017-11-23 12:10:00 +0530
 */

namespace Tributes;

class Tribute {

	protected $data = array('page_indexed' => 0, 'page_description' => '', 'page_pathname' => '',  'page_title' => '', 'page_content' => '', 'page_content_title' => '');
	
	private static function load($page_key) {
		$page = \SiteManager::getDatabase()->getRecord('tribute', compact('page_key'));
		if ($page) {
			return $page ? new static($page, false) : false;
		}
		return false;
	}
	
	public static function loadById($id) {
		$page = \SiteManager::getDatabase()->getRecord('tribute', compact('id'));
		if ($page) {
			return $page ? new static($page, false) : false;
		}
		return false;
	}
	
	public function __construct($data) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	public function __get($key){
		return isset($this->data['page_' . $key]) ? $this->data['page_' . $key] : null;
	}
	
	public function getData($key = null, $prefix = 'page_') {
		return is_null($key) ? $this->data : $this->data[$prefix . $key];
	}
	
	public static function search($condition = null, $order = null, $limit = null, &$count = false) {
		$arItem = array();
		$pdbo = \SiteManager::getDatabase();
		if ($order && !is_array($order)) $order = array($order, 'ASC');
		if ($limit && !is_array($limit)) $limit = array(0, $limit);

		$whereSql = empty($condition) ? 1 : $pdbo->genQuery($condition);
		if ($count !== false) {
			$count = $pdbo->getCount('tribute', $whereSql);
		}
		$stmt = $pdbo->search('tribute', $whereSql, $order, $limit);
		$arItem = $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : false;
		return $arItem;
	}
}
