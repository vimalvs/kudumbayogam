<?php 
class Template{ 
    var $path = array(); 
    var $origCode = array(); 
    var $finalCode = array(); 
    var $varCode = array(); 

    public function __construct($root = '') {
        //if(empty($root))die('<b>Template:</b> No root directory set.'); 
        //else
		if(!(empty($root) || is_dir($root))) die("<b>Template:</b> $root Directory doesn't exist."); 
        $this->root = $root; 
     
    } 
    function getFile($arFile){ 
        if(!is_array($arFile)) die("<b>getFile:</b> $arFile not an array."); 
        foreach($arFile as $filename ){ 
            $tplFile = $this->root . $filename; 
			if(!file_exists($tplFile)) die("<b>getFile:</b> $tplFile doesnot exist.");
			else $this->origCode = implode( '', @file($tplFile) ); 
        } 
    } 
    function varRef($arRef){ 
        if(!is_array($arRef))die("<b>varRef:</b> $arRef is not an array."); 
        if(!empty($this->origCode)){ 
            foreach($arRef as $ref=>$replace){ 
                $this->varCode['{' . strtoupper($ref) . '}'] = $replace; 
            } 
        }
    } 
    function compile(){ 
        if(empty($this->origCode))die("<b>compile:</b> No code exist."); 
        $code = $this->origCode; 
        foreach($this->varCode as $varref => $replace){
			$code = str_replace($varref, $replace, $code);
        } 
        return $code; 
    } 
} 
?>