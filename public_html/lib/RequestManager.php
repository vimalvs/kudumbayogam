<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * RequestManager Class
 * 
 * HTTP Request Handler
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-05-18 14:43:00 +0530 (Wed, 18 May 2011	) $
 */

class RequestManager {
	const IP_DETAILS_COUNTRY = 1;
	const IP_DETAILS_CITY = 2;
	private static $device = null;
	private static $userAgent = null;
	private static $initialized = false;
	public static $isMobileEdition = false;
	private static $arScreenSize = array('240x320' => 'VGA 240x320', '480x640' => 'VGA 480x640', '320x240' => 'VGA 320x240', '640x480' => 'VGA 640x480', '240x400' => 'WVGA 240x400', '480x800' => 'WVGA 480x800', '400x240' => 'WVGA 400x240', '800x480' => 'WVGA 800x480', '320x480' => 'iPhone 320x480', '480x320' => 'iPhone 480x320', '640x960' => 'iPhone 640x960', '960x640' => 'iPhone 960x640', '480x272' => 'PSP 480x272', '272x480' => 'PSP 272x480', '176x220' => 'Phone 176x220', '220x176' => 'Phone 220x176');
	private static $arConfig = [];
	
	private static $baseUrl = null;
	private static $mobileBaseUrl = null;
	private static $uaType = 'PA';
	
	public static function initialize($baseUrl, $mobileBaseUrl = null, $isMobileEdition = false) {
		static::$baseUrl = $baseUrl;
		static::$mobileBaseUrl = $mobileBaseUrl ?: $baseUrl;
		static::$isMobileEdition = $isMobileEdition;
		
		self::$userAgent = strtolower(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
		
		if (isset($_GET['skip_ua_check'])) {
			$ua_type = 'PA';
		} else if (isset($_GET['set_ua_type']) && preg_match('/[PM][AB]0?/', $_GET['set_ua_type'])) {
			$ua_type = $_GET['set_ua_type'];
		} else if (!empty($_COOKIE['ua_type'])) {
			$ua_type = $_COOKIE['ua_type'];
		} else {
			$arDeviceInfo = self::getDeviceInfo();
			$ua_type = ($arDeviceInfo['is_pc'] ? 'P' : 'M') . $arDeviceInfo['grade'];
		}
		
		if (empty($_COOKIE['ua_type']) || $_COOKIE['ua_type'] !== $ua_type) {
			setcookie('ua_type', $ua_type, TIME_NOW + 2592000, '/', COOKIE_DOMAIN, false, true);
			$_COOKIE['ua_type'] = $ua_type;
		}

		self::$uaType = $ua_type;
	}
	
	public static function getUserAgent() {
		return self::$userAgent;
	}
	
	public static function getUserAgentType() {
		return self::$uaType;
	}
	
	public static function getBaseUrl($mobile = false) {
		return $mobile ? static::$mobileBaseUrl : static::$baseUrl;
	}
	
	public static function isAjax() {
		static $isAjaxRequest = null;
		if (is_null($isAjaxRequest)) {
			$isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') || isset($_GET['jsonp_callback']) || !empty($_GET['ajax']);
		}
		return $isAjaxRequest;
	}
	
	public static function isBot() {
		static $isBot = null;
		if (is_null($isBot)) {
			$isBot = preg_match('/bot|crawler|slurp/i', self::$userAgent);
		}
		return $isBot;
	}
	
	public static function isDesktop() {
		$ua = self::$userAgent;
		
		if (!is_null(self::$device)) {
			return (self::$device === 'desktop');
		}

		if (!(self::isMobilePhone() || preg_match('/ipad|iphone|android|webos|maemo|blackberry|hpwos/', $ua))) {
			self::$device = 'desktop';
			return true;
		}
		return false;
	}
	
	public static function isTablet() {
		$ua = self::$userAgent;

		if (!is_null(self::$device)) {
			return (self::$device === 'tablet');
		}

		if ((stripos($ua, 'tablet') !== false) // All Tablets
		  || (stripos($ua, 'ipad') !== false && stripos($ua, 'webkit') !== false) // iPad
		  || ((stripos($ua, 'android') !== false) && (stripos($ua, 'mobile') === false)) || (stripos($ua, 'googletv') !== false) // Android Tablet
		  || (stripos($ua, 'playbook') !== false)  // BlackBerry Tablet
		  || ((stripos($ua, 'hpwos') !== false) && (stripos($ua, 'tablet') !== false)) // HP WebOS Tablet
		) {
			self::$device = 'tablet';
			return true;
		}
		return false;
	}
	
	public static function isSmartPhone() {
		$ua = self::$userAgent;
		
		if (!is_null(self::$device)) {
			return (self::$device === 'smartphone');
		}
		
		if (preg_match('/iphone|ipod/i', $ua) // iPhone / iPod
		  || ((stripos('android', $ua) !== false) && (stripos('mobile', $ua) !== false)) // Android Phone
		  || preg_match('/blackberry95|blackberry 98/i', $ua) // BlackBerry
		  || (stripos('webos', $ua) !== false) // Palm WebOS
		  || (stripos('maemo', $ua) !== false) || ((stripos('linux', $ua) !== false) && (stripos('tablet', $ua) !== false)) // Maemo
		) {
			self::$device = 'smartphone';
			return true;
		}
		return false;
	}
	
	public static function isMobilePhone() {
		
		self::isSmartPhone();
		
		if (!is_null(self::$device)) {
			return (self::$device === 'mobilephone');
		}

		$httpAccept = strtolower(isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : '');
		$arMobileUserAgent = array(
			'acs-','alav','alca','amoi','audi','aste','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco',
			'eric','hipt','htc','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp',
			'mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','opwv','palm','pana','pant','pdxg','phil','play','pluc',
			'port','prox','qtek','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar',
			'sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr',
			'webc','winw','winw','xda-'
		);
		if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])
			|| (strpos($httpAccept, 'text/vnd.wap.wml') !== false) || (strpos($httpAccept, 'application/vnd.wap.xhtml+xml') !== false) 
			|| preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|ipod|phone|vodafone|o2|pocket|mobi|pda|psp)/i', self::$userAgent)
			|| in_array(substr(self::$userAgent, 0, 4), $arMobileUserAgent)){
			self::$device = 'mobilephone';
			return true;
		}
		return false;
	}
	
	public static function isDebugMode() {
		return self::$debugMode;
	}
	
	public static function getIp() {
		// return isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
		return $_SERVER['REMOTE_ADDR'];
	}
	
	public static function getIpDetails($detailed = self::IP_DETAILS_CITY, $ip = null) {
		static $reader = null;
		$ip = $ip ?: self::getIp();
		if (!is_null($ip)) {
			if (!$reader) {
				try {
					$reader = new \GeoIp2\Database\Reader('/usr/local/share/GeoIP/GeoLite2-City.mmdb');		
				} catch (\MaxMind\Db\Reader\InvalidDatabaseException $e) {
					\Feedback\Alert::alert('[GeoIP2] Invalid Database', "MaxMind DB Reader exited with message\m{$e->getMessage()}")->limitRate()->send();
				} catch (\Exception $e) {
					// Do Nothing
				}
			}
			if ($reader) {
				try {
					if ($detailed === 2) {
						$ipRec = $reader->city($ip);
						return [
							'ip' => $ip,
							'latitude' => $ipRec->location->latitude,
							'longitude' => $ipRec->location->longitude,
							'time_zone' => $ipRec->location->timeZone,
							'geonameid' => $ipRec->city->geonameId,
							'country_code' => $ipRec->country->isoCode
						];
					} else {
						$ipRec = $reader->country($ip);
						return [
							'ip' => $ip,
							'country_code' => $ipRec->country->isoCode
						];
					}
				} catch (\Exception $e) {
					// Do Nothing
				}
			}
		}
		return false;
	}
	
	public static function getDeviceInfo() {
		$ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : (isset($_SERVER['HTTP_X_DEVICE_USER_AGENT']) ? $_SERVER['HTTP_X_DEVICE_USER_AGENT'] : '');
		if (isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])) {
			$ua .= $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'];
		}
		$key = md5($ua);
		$arDeviceInfo = \Cache::load($key, 'device_info', 300, function () use ($ua) {
			$detect = new \Detection\MobileDetect;
			$isMobile = $detect->isMobile() || preg_match('/Mobile|Touch/', $ua);
			$isTablet = $detect->isTablet() || preg_match('/Tablet/', $ua);
			
			// MIDP, UCWeb, Opera, WAP, Google Wireless Transcoder
			$isHighEndPortable = !preg_match('#MIDP|UCWeb|Opera|WAP|Google Wireless Transcoder#', $ua) 
									&& !preg_match('#\b(?:midp|wap|cldc|maui)\b#i', $ua) 
									&& preg_match('#Android|iPhone|iPad|iPod|IEMobile/1\d#', $ua);
			$grade = ($isMobile && !$isHighEndPortable) ? 'B' : 'A';
			
			return array('is_tablet' => $isTablet, 'is_mobile' => $isMobile, 'is_pc' => !$isTablet && !$isMobile, 'grade' => $grade);
			
		}, false);
		return $arDeviceInfo;
	}
	
	public static function isSlowConnection() {
		if (
			// New connection with long connection time
			(isset($_SERVER['HTTP_X_REQUEST_COUNT']) && $_SERVER['HTTP_X_REQUEST_COUNT'] == 1 && ((microtime(true) * 1000000 - $_SERVER['HTTP_X_REQUEST_START']) > 100000)) || 
			// Cookie already set
			!empty($_COOKIE['ua_lite'])
			) {
				return true;
		}
		return false;
	}
	
	public static function detectSlowConnection() {
		if (self::isSlowConnection() && empty($_COOKIE['ua_lite'])) {
			setcookie('ua_lite', 1);
		}
	}
	
	public static function enableMobileRedirection() {
		$ua_type = self::$uaType;
		if (empty($_COOKIE['ua_type']) || $_COOKIE['ua_type'] !== $ua_type) {
			setcookie('ua_type', $ua_type, TIME_NOW + 2592000, '/', COOKIE_DOMAIN, false, true);
			$_COOKIE['ua_type'] = $ua_type;
		}

		$data = [
			'ua_type' => self::$uaType,
			'uri' => static::getUri(),
			'request_method' => (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST' ? 'POST' : 'GET'),
			'redirect_base' => null,
		];
		
		if (self::$isMobileEdition) {
			if ($data['ua_type'][0] === 'P') {
				$data['redirect_base'] = static::$baseUrl;
			}
		} else {
			if ($data['ua_type'][0] === 'M') {
				$data['redirect_base'] = static::$mobileBaseUrl;
			}
		}
		
		$data = \SiteManager::invokeFilter('request_before_mobile_redirection', $data);

		if ($data['redirect_base']) {
			ResponseManager::redirect($data['redirect_base'] . $data['uri'], ($data['request_method'] === 'POST' ? $_POST : null), false, 302, $data['request_method']);
		}
	}
	
	public static function getUri($removeControlParams = null) {
		static $pageUri = null, $pageUriClean = null;
		if (is_null($pageUri)) {
			$pageUri = preg_replace('#^/m(?=/)#', '', $_SERVER['REQUEST_URI']);
		}
		if (is_null($removeControlParams)) {
			$removeControlParams = 'set_ua_type|mob|skip_ua_check|ua_redir|_part|utm_medium|utm_source|utm_campaign|PageSpeed';
		}
		if ($removeControlParams) {
			if (is_null($pageUriClean)) {
				$pageUriClean = rtrim(preg_replace("/(?<=[?&;])(?:$removeControlParams)=.*?(?:$|[&;])/", '', $pageUri), '?&');
			}
			return $pageUriClean;
		} else {
			return $pageUri;
		}
	}
	
	public static function getShareUri() {
		$removeParams = 'set_ua_type|mob|skip_ua_check|_part|utm_medium|utm_source|utm_campaign|PageSpeed|page';
		$uri = defined('CANONICAL_PATH') ? CANONICAL_PATH : static::getUri($removeParams);
		$uri = preg_replace('#(?<=/)page-\\d+\\.html#i', '', $uri);
		return $uri;
	}
	
	public static function getUrl($mobile = null, $removeControlParams = null) {
		if (is_null($mobile)) {
			$mobile = static::$isMobileEdition;
		}
		return static::getBaseUrl($mobile) . static::getUri($removeControlParams);
		// return $removeControlParams
		// static $pageUrl = null;
		// if (is_null($pageUrl)) {
		// 	$pageUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https://' : 'http://';
		// 	$pageUrl .= $_SERVER[($useCanonical ? 'SERVER_NAME' : 'HTTP_HOST')] . (($_SERVER['SERVER_PORT'] == 80) ? '' : $_SERVER['SERVER_PORT']) . $_SERVER['REQUEST_URI'];
		// }
		// return $pageUrl;
	}
	
	public static function getScreenSize() {
		if (!defined('SCREEN_RESOLUTION')) {
			$screen_res = !empty($_POST['screen_res']) ? $_POST['screen_res'] : (!empty($_GET['screen_res']) ? $_GET['screen_res'] : (!empty($_COOKIE['screen_res']) ? $_COOKIE['screen_res'] : ''));	
			$arScreenResolution = array('240x320', '480x640', '320x240', '640x480', '240x400', '480x800', '400x240', '800x480', '320x480', '480x320', '640x960', '960x640', '480x272', '272x480', '176x220', '220x176');
			if (!isset(self::$arScreenSize[$screen_res])) {
				$screen_res = '240x320';
				define('SCREEN_RESOLUTION_SET', false);
			} else {
				define('SCREEN_RESOLUTION_SET', true);
			}
			define('SCREEN_RESOLUTION', $screen_res);
			$tmp = explode('x', $screen_res);
			define('SCREEN_WIDTH', (int)$tmp[0]);
			define('SCREEN_HEIGHT', (int)$tmp[1]);
		} else {
			$tmp = explode('x', SCREEN_RESOLUTION);
		}
		return $tmp;
	}
	
	public static function getAvailableScreenSizes() {
		return self::$arScreenSize;//array('240x320', '480x640', '320x240', '640x480', '240x400', '480x800', '400x240', '800x480', '320x480', '480x320', '640x960', '960x640', '480x272', '272x480', '176x220', '220x176');
	}
	
	public static function getStateUrl($type = 's', $ttl = null, $auto_delete = 0) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$data = array('auto_delete' => $auto_delete, 'data' => array('action' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'values' => $_POST));
			if ($type === 's') {
				if (session_status() == PHP_SESSION_NONE) {
					session_start();
				}
				do {
					$state = UniqueID(8, 0);
				} while (isset($_SESSION['saved_state_info'][$state]));
				$_SESSION['saved_state_info'][$state] = $data;
			} elseif ($type === 'm') {
				$cache = \Cache::getInstance();
				do {
					$state = UniqueID(8, 0);
				} while ($cache->get($state));
				$cache->set($state, $data, is_null($ttl) ? 3600 : $ttl, 'saved_state_info');
			} elseif ($type === 'p') {
				$pdbo = \SiteManager::getDatabase();
				do {
					$state = UniqueID(10);
				} while ($pdbo->getRecord('http_state_redirect', array('state' => $state)));
				$data['expiry'] = TIME_NOW + (is_null($ttl) ? 2592000 : $ttl);
				$data['data'] = serialize($data['data']);
				$data['state'] = $state;
				$pdbo->insertRecord('http_state_redirect', $data);
			}
			return SITE_HOME . "/res/redirect/$type/$state";
		} else {
			return $_SERVER['REQUEST_URI'];
		}
	}
	
	public static function setConfig($key, $value) {
		self::$arConfig[$key] = $value;
	}
	
	public static function getConfig($key, $default = null) {
		return isset(self::$arConfig[$key]) ? self::$arConfig[$key] : $default;
	}
	
	public static function getSessionData($key, $default = null) {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
	}
	
	public static function setSessionData($key, $val = null) {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		$_SESSION[$key] = $val;
	}
}

// RequestManager::initialize();