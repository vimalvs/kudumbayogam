<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Review;

class Queue_Entry {
	
	protected $data = [];
	protected $updateData = [];
	
	public function __construct($data) {
		if (isset($data['template_id'])) {
			$this->data = $data;
		} else {
			$this->updateData = $data;
		}
	}

	public function update($data, &$errorInfo = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($errorInfo);
	}
	
	public function save(&$errorInfo = null) {
		$isNew = empty($this->data['template_id']);
		$pdbo = \SiteManager::getDatabase();
		
		// ID is not editable
		unset($this->updateData['template_id']);
		
		if (empty($this->updateData)) return null;
		
		extract($this->data);
		extract($this->updateData, EXTR_REFS);
		
		if (is_null($errorInfo)) $errorInfo = array();
		if (empty($template_name)) $errroInfo['template_name'] = 'Template name is required';
		if (empty($template_content)) $errroInfo['template_content'] = 'Template cannot be empty';
		
		$status = empty($errorInfo);
		if ($status) {
			if ($isNew) {
				if (empty($this->updateData['template_time_created'])) {
					$this->updateData['template_time_created'] = TIME_NOW;
				}
				$status = ($pdbo->insertRecord('review_queue_template', $this->updateData) == 1);
				if ($status) {
					$template_id = $pdbo->lastInsertId();
					$this->data['template_id'] = $template_id;
				}
			} else {
				$status = $pdbo->updateRecord('review_queue_template', array('template_id' => $template_id), $this->updateData);
				// Return success if item is not modified / there is no error
				$status = ($status !== false);
			}
			if ($status) {
				$this->data = array_merge($this->data, $this->updateData);
				$this->updateData = array();
			}
		}
		return $status;
	}
}