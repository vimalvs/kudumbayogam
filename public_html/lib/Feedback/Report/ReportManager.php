<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Error Report Class
 * 
 * Class for actions related to error reporting
 * @package		Report
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Feedback\Report;

class ReportManager extends Report {

	protected $updateData = array();
	protected $sectionInfo = null;

	public static function getSectionInfo($report_section) {
		return \SiteManager::getConfig("feedback_report_section_{$report_section}");
	}

	public static function checkItem($report_section, $report_item_id, &$errorInfo = null) {
		$sectionInfo = \SiteManager::getConfig("feedback_report_section_{$report_section}");
		if ($sectionInfo) {
			if ($sectionInfo['class_name']) {
				try {
					$item = call_user_func("{$sectionInfo['class_name']}::getItem", $report_item_id, $report_section);
					return true;
				} catch(\Exception $e) {
					$errorInfo = $e->getMessage();
				}
			}
		} else {
			$errorInfo = 'Invalid section';
		}
		return false;
	}
	
	public function __construct($data = null) {
		if ($data) {
			if (isset($data['report_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}
	public function getAdditionalTemplate() {
		extract($this->data);
		$sectionInfo = self::getSectionInfo($report_section);
		$additional_template = null;
		$parseData = ['site_home' => SITE_HOME, 'report_id' => $report_id, 'report_section' => $report_section] + $this->data['report_item_details'];
		if ($sectionInfo['template']) {
			$additional_template = $sectionInfo['template'];
			if (preg_match_all('/\{([a-z_]+)\}/i', $additional_template, $matches, PREG_SET_ORDER)){
				foreach ($matches as $match) {
					$key = strtolower($match[1]);
					if (isset($parseData[$key])) {
						$additional_template = str_replace($match[0], $parseData[$key], $additional_template);
					}
				}
			}
		}
		return $additional_template;
	}
			
	public function send() {
		extract($this->data);
		$sectionInfo = self::getSectionInfo($report_section);
		$additional_template = null;
		$parseData = ['site_home' => SITE_HOME, 'report_id' => $report_id, 'report_section' => $report_section] + $this->data['report_item_details'];
		if ($sectionInfo['template']) {
			$additional_template = $sectionInfo['template'];
			if (preg_match_all('/\{([a-z_]+)\}/i', $additional_template, $matches, PREG_SET_ORDER)){
				foreach ($matches as $match) {
					$key = strtolower($match[1]);
					if (isset($parseData[$key])) {
						$additional_template = str_replace($match[0], $parseData[$key], $additional_template);
					}
				}
			}
		}
		$report_description = 'User reported incorrect data';
		$email = new \EmailTemplate(array('to' => array('Developer-admin', 'alert@ennexa.com'), 'subject' => "User reported incorrect data", 'isHTML' => true));
		$email->setTemplate(ROOT . '/res/report/templates/mail_report.tpl.php')
			  ->addData(compact('report_user_name', 'report_user_email', 'report_mistake', 'report_correction', 'report_field', 'report_url', 'report_section', 'additional_template', 'report_description'));
		return $email->send();
	}

	public function delete(&$errorInfo = null) {
	
		$pdbo = \SiteManager::getDatabase();
		$status = $pdbo->deleteRecord('error_report', array('report_id' => $this->data['report_id']));
		if ($status) {
			\Logger::log("Deleted error report : " . $this->data['report_url'], 'delete', 'error_report', $this->data['report_id']);
			return true;
		} else {
			$errorInfo = 'Failed to delete report from database';
		}		
	}
	
	public function update($data, &$errorInfo = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($errorInfo);
	}
	
	public function save(&$errorInfo = null) {
		$isNew = empty($this->data['report_id']);

		unset($this->updateData['report_id']);

		if (empty($this->updateData)) return true;

		extract($this->data); 
		extract($this->updateData, EXTR_REFS);

		if (empty($report_user_name)) $errorInfo['report_user_name'] = 'Enter your name';
		if (empty($report_user_email)) $errorInfo['report_user_email'] = 'Enter your email id';
		if (empty($report_correction) && empty($mistake)) $errorInfo['report_correction'] = 'Mention the detail';

		$sectionInfo = self::getSectionInfo($report_section);
		if ($sectionInfo) {
			if ($sectionInfo['class_name']) {
				try {
					$item = call_user_func("{$sectionInfo['class_name']}::getItem", $report_item_id, $report_section);
					if($item) {						
						$this->updateData['report_item_details'] = $item->getItemData($report_item_id, $report_section);
					}
				} catch(\Exception $e) {
					$errorInfo = $e->getMessage();
				}
			} else {
				$this->updateData['report_item_details'] = [];
			}
		} else {
			$errorInfo = 'Invalid section';
		}
		$status = empty($errorInfo);
		if ($status) {
			$pdbo = \SiteManager::getDatabase();
			$updateData = $this->updateData;
			$updateData['report_item_details'] = serialize($this->updateData['report_item_details']);
			if ($isNew) {
				$status = $pdbo->insertRecord('error_report', $updateData);
				if ($status) {
					$report_id = $pdbo->lastInsertId();
					$this->data['report_id'] = $report_id;
					\Logger::log("Error report Added : $report_url", 'add' , 'error_report', $report_id);
				}
			} else {
				$status = $pdbo->updateRecord('error_report', compact('report_id'), $updateData);
				if ($status) {
					\Logger::log("Error report status updated : $report_url", 'update' , 'error_report', $report_id);
				}
			}
			if ($status) {
				$this->data = array_merge($this->data, $this->updateData);
				$this->updateData = [];
			}
		}

		return $status;
	}
}