<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Error Report Class
 * 
 * Class for actions related to error reporting
 * @package		Report
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Feedback\Report;
class Report {
	protected $data = array();

	public function __construct($data = null) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	protected static function load($condition) {
		$report = \SiteManager::getDatabase()->getRecord('error_report', $condition);
		if ($report) {
			// Any updations here should be performed in ReportManager::save too
			$report['report_item_details'] = unserialize($report['report_item_details']);
			return $report;
		}
		return false;	
	}
	
	public static function loadById($report_id) {
		$data = static::load(array('report_id' => $report_id));
		return $data ? new static($data) : false;
	}

	
	public static function search($condition, $order = null, $limit = null, &$count = null) {
		$pdbo = \SiteManager::getDatabase();
		if ($limit !== 0) {
			$stmt = $pdbo->search('error_report', $condition, $order, $limit);
			$reports = $stmt ? $stmt->fetchAll() : array();
			$reports = array_map(function ($report) {
				$report['report_item_details'] = unserialize($report['report_item_details']);
				return $report;
			}, $reports);
		} else {
			$reports = array();
		}
		if ($count !== false) {
			$count = $pdbo->getCount('error_report', $condition);
		}
		return $reports;
	}

	public function __get($key) {
		return isset($this->data['report_' . $key]) ? $this->data['report_' . $key] : null;
	}
	
	public function getData($key = NULL) {
		return is_null($key) ? $this->data : $this->data[$key];
	}
}