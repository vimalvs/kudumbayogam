<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Logger Class
 * 
 * Logging of moderation actions
 * @package		Utilities
 * @author 		Joyce Babu <joyce@ennexa.com>
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

class Logger {
	
	public static function log($log_message, $log_action , $log_section, $log_item = NULL) {
		global $userAuth;
		$pdbo = SiteManager::getDatabase();
		if ($pdbo) {
			$log_time = TIME_NOW;
			$user_id = isset($userAuth) ? $userAuth->id : 0;
			$log_hash = md5($log_message . '|' . $log_action . '|' . $log_section . '|' . $log_item . '|' . $user_id);
			$lastLog = \Cache::load('last_entry', 'logger', 3600, function () use ($pdbo) {
				$stmt = $pdbo->search('moderation_log', NULL, array('log_id', 'DESC'), 1);
				return $stmt ? $stmt->fetch(PDO::FETCH_ASSOC) : NULL;
			}, FALSE);
			if ($lastLog && ($lastLog['log_hash'] === $log_hash) && ($lastLog['log_time'] - $log_time) < 86400) {
				$lastLog['log_time'] = $log_time;
				$status = $pdbo->updateRecord('moderation_log', array('log_id' => $lastLog['log_id']), array('log_time' => $log_time));
				if ($status) {
					\Cache::getInstance()->set('last_entry', $lastLog, 3600, 'logger');
				}
			} else {
				$data = compact('log_message', 'log_action', 'log_section', 'log_item', 'log_time', 'user_id', 'log_hash');
				$status = $pdbo->insertRecord('moderation_log', $data);
				if ($status) {
					$data['log_id'] = $pdbo->getLastInsertId();
					\Cache::getInstance()->set('last_entry', $data, 3600, 'logger');
				}
			}
			return $status;
		}
		return FALSE;
	}
	
	public static function search($condition, $order = NULL, $limit = NULL, &$count = FALSE) {
		$pdbo = SiteManager::getDatabase();
		if (is_null($order)) $order = array('log_id', 'DESC');
		if (is_null($limit)) $limit = 25;
		$stmt = $pdbo->search('moderation_log log LEFT OUTER JOIN user_list user USING(user_id)', $condition, $order, $limit, 'log.*, IFNULL(user.user_name, "Guest") as log_user');
		if ($count !== FALSE) {
			$count = $pdbo->getCount('moderation_log', $condition);
		}
		return $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : array();
	}
	
	public static function history($filter, $nPerPage = 15) {
		$tpl = \SiteManager::getTemplate();
		$page = getCleanVar('page', 1);
		$nPerPage = (int)getCleanVar('items_per_page', 25);
		
		// $sort_field = getCleanVar('sort_field', '#');
		$sort_direction = getCleanVar('sort_direction', 'asc');
		$query_filter = getCleanVar('query_filter', array());
		$qf = array_filter($query_filter);
		if (!empty($qf['log_time']['RANGE_START'])) $qf['log_time']['RANGE_START'] = strtotime($qf['log_time']['RANGE_START']);
		if (!empty($qf['log_time']['RANGE_END'])) $qf['log_time']['RANGE_END'] = strtotime($qf['log_time']['RANGE_END']);
		
		$filters = \Query\Filter::parse(array_filter($qf));
		$condition = \Query\Filter::generateCondition($filters);
		
		if ($filter instanceof \Query\Condition) {
			$condition->add($filter);
		} elseif (is_array($filter)) {
			$condition->add(new \Query\Condition($filter));
		}
		// $tmp = ($sort_field === '#') ? array('log_id', $sort_direction === 'asc' ? 'desc' : 'asc') : array(substr($sort_field, 6), $sort_direction);
		
		$start = ((int)$page - 1) * $nPerPage;
		$history = self::search($condition, NULL, array($start, $nPerPage), $count);
		$tpl->addData(array('pages' => ceil($count / $nPerPage), 'page' => $page, 'per_page' => $nPerPage, 'query_filter' => $query_filter));
		return $tpl->data['history'] = $history;
	}
	
	public static function recent($section, $table, $field_id) {
		$pdbo = \SiteManager::getDatabase();
		$tpl = \SiteManager::getTemplate();
		$stmt = $pdbo->query("SELECT f.*, l1.*, user.user_name FROM moderation_log l1 LEFT OUTER JOIN user_list user USING(user_id) LEFT JOIN moderation_log l2 ON (l1.log_item = l2.log_item AND l1.log_section = {$pdbo->quote($section)} AND l2.log_section = {$pdbo->quote($section)} AND l1.log_id < l2.log_id) JOIN $table f ON (f.$field_id = l1.log_item AND l1.log_section = {$pdbo->quote($section)}) WHERE l1.log_section = {$pdbo->quote($section)} AND l2.log_id IS NULL ORDER BY l1.log_id DESC LIMIT 10");
		return $stmt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : array();
	}
	
	public static function itemActivity($item_id, $log_section) {
		
	}

	public static function userActivity($user_id, $log_section = NULL, $ignoredAction = NULL, $limit = 15, $last_seen = NULL) {
		$pdbo = \SiteManager::getDatabase();
		$user_id = $pdbo->quote($user_id);
		$log_section = $pdbo->quote($log_section);
		if ($last_seen) $last_seen_quoted = $pdbo->quote($last_seen);
		$ignoredAction =  is_null($ignoredAction) ? array('delete') : (array)$ignoredAction;
		if (!empty($ignoredAction)) {
			$ignoreSql = ' AND NOT l1.log_action IN (' . implode(array_map(array($pdbo, 'quote'), $ignoredAction)) . ')';
		} else {
			$ignoreSql = '';
		}
		$stmt = $pdbo->query("SELECT l1.* FROM moderation_log l1 LEFT JOIN moderation_log l2 ON (l1.log_item = l2.log_item AND l1.log_section = l2.log_section AND l1.log_id < l2.log_id)  WHERE l1.user_id = $user_id AND l2.log_id IS NULL " . (empty($last_seen) ? '' : "l1.log_id > $last_seen_quoted") . "$ignoreSql ORDER BY l1.log_id DESC LIMIT " . (empty($last_seen) ? $limit : "$last_seen_quoted, 10"));
		return $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : array();
	}
}
