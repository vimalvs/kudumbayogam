<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * FileCache Class
 * 
 * Class for file based data caching
 * @package		Cache
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		1.0.0
 * @date		$Date: 2010-07-02 17:47:00 +0530 (Fri, 02 Jul 2010	) $
 */
if (!defined('CACHE_PATH')) {
	define('CACHE_PATH', rtrim(str_replace('\\', '/', sys_get_temp_dir()), '/') . '/filecache/');
}
class FileCache{
	
	private $cacheGroup = null;
	private $cacheTime = 900;
	private $cache = array();
	private $tempDir = '';
	
	const FILE_READ = 0;
	const FILE_WRITE = 1;
	const FILE_UPDATE = 2;
	
	public function __construct($cacheTime = null, $cacheGroup = null){
		$ts = microtime(true);
		if (!is_null($cacheTime)) $this->cacheTime = $cacheTime;
		if (!is_null($cacheGroup)) $this->cacheGroup = SITE_CODE . '_' . $cacheGroup;
		else $this->cacheGroup = SITE_CODE . '_cache';
		$this->setCacheDirectory(CACHE_PATH);
		\Feedback\Stats::timing("timing.cache.filecache.init", (microtime(true) - $ts) * 1000000);
	}
	public function setCacheDirectory($dir) {
		if (!file_exists($dir)) {
			$umask = umask(0); 
			mkdir($dir, 0777);
			umask($umask); 
		}
		if (!is_dir($dir) || !is_writable($dir)) {
			throw new \Exception("<em>$dir</em> is not writable");
		} else {
			$this->tempDir = $dir;
		}
		
	}
	public function setCacheGroup($cacheGroup = null){
		if (is_null($cacheGroup)) $cacheGroup = 'cache';
		$this->cacheGroup = SITE_CODE . '_' . $cacheGroup;
	}
	public function setCacheTime($cacheTime = null){
		if (!is_null($cacheTime)) {
			$this->cacheTime = $cacheTime;
		}
	}
	public function __get($key){
		$this->get($key);
	}
	public function get($key, $cacheGroup = null){
		$ts = microtime(true);

		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}

		$data = self::fileReadWrite(self::FILE_READ, $key, null, null, $cacheGroup);
		\Feedback\Stats::timing("timing.cache.filecache.get", (microtime(true) - $ts) * 1000000);
		
		if (is_null($data)) {
			\Feedback\Stats::timing("timing.cache.filecache.miss", (microtime(true) - $ts) * 1000000);
		}
		
		return $data;
	}
	public function __set($key, $value){
		$this->set($key, $value);
	}
	public function set($key, $value, $cacheTime = null, $cacheGroup = null){
		$ts = microtime(true);
		$status = $this->fileReadWrite(self::FILE_WRITE, $key, $value, $cacheTime, $cacheGroup);
		\Feedback\Stats::timing("timing.cache.filecache.set", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	public function __isset($key){
		return $this->has($key);
	}
	public function has($key, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '_' . $cacheGroup);
		return !is_null($this->get($key, $cacheGroup));
	}
	public function __unset($key){
		return $this->delete($key);
	}
	public function delete($key, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '_' . $cacheGroup);
		if (file_exists($this->tempDir . '/' . $cacheGroup . '/' . strtr(urlencode($key), '.', '/') . '.fc')) {
			return unlink($this->tempDir . '/' . $cacheGroup . '/' . strtr(urlencode($key), '.', '/') . '.fc');	
		}
		return true;
	}
	public function clear($prefix = null, $cacheGroup = null){
		$ts = microtime(true);
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '_' . $cacheGroup);
		$dir = $this->tempDir . '/' . $cacheGroup . '/' . strtr(urlencode($prefix), '.', '/');
		$status = \Utils\Utils::rmdir($dir);
		\Feedback\Stats::timing("timing.cache.filecache.clear", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	public function inc($key, $value = 1,  $cacheTime = null, $cacheGroup = null) {
		return $this->fileReadWrite(self::FILE_UPDATE, $key, $value, $cacheTime, $cacheGroup);
	}
	public function dec($key, $value = 1,  $cacheTime = null, $cacheGroup = null) {
		return $this->fileReadWrite(self::FILE_UPDATE, $key, 0 - $value, $cacheTime, $cacheGroup);
	}
	
	private function fileReadWrite($action, $key, $value, $cacheTime, $cacheGroup) {
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '_' . $cacheGroup);
		$file = $this->tempDir . '/' . $cacheGroup . '/' . strtr(urlencode($key), '.', '/') . '.fc';
		$dir = substr($file, 0, strrpos($file, '/'));
		// printr($file, $cacheGroup, $key, $this->tempDir . "/" . $cacheGroup . "/" . strtr(urlencode($key), ".", "/"));
		assert('strpos($cacheGroup . $key, "#") === false', new \AssertionError("[FileCache] Invalid key $cacheGroup.$key. Cache keys should not have # character in it"));
		assert('!is_dir($this->tempDir . "/" . $cacheGroup . "/" . strtr(urlencode($key), ".", "/"))', new \AssertionError("[FileCache] A bucket exists with the name $key. Versioned keys cannot have items sharing a bucket name."));
		
		$isRead = $action === self::FILE_READ;
		$exists = file_exists($file);
		
		if (!$exists) {
			if (!$isRead) {
				$status = file_exists($dir) || mkdir($dir, 0700, true);
				if (!$status) {
					trigger_error("[FileCache] Failed to create temporary directory $dir", E_USER_ERROR);
					return false;
				}
			} else {
				return null;
			}
		}
		
		$handle = fopen($file, ($isRead ? 'r' : 'c+'));
		if ($handle) {
			$locked = flock($handle, ($isRead ? LOCK_SH : LOCK_EX));
			if ($locked) {
				// $data = $exists ? fread($handle, filesize($file)) : null;
				if ($exists) {
					$data = unserialize(file_get_contents($file));
					$data = isset($data[1]) ? $data[1] : null;
				} else {
					$data = null;
				}
				
				if ($isRead) {
					flock($handle, LOCK_UN);
					return $data;
				} else {
					if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
					if ($action === self::FILE_UPDATE) {
						$value += (int)$data;
					}
					ftruncate($handle, 0);
					rewind($handle);
					fwrite($handle, serialize([$cacheTime, $value]));
					flock($handle, LOCK_UN);
					return true;
				}
			} else {
				trigger_error("[FileCache] Failed to acquire lock for updating $file", E_USER_ERROR);
			}
		} else {
			trigger_error("[FileCache] Failed to open file $file", E_USER_ERROR);
		}
	}
	
	public function purge($path = null) {
		if (is_null($path)) {
			$path = rtrim(str_replace('\\', '/', sys_get_temp_dir()), '/') . '/filecache';
		}
		
		$directory = new \RecursiveDirectoryIterator($path);
		$filter = new \Filesystem\FilenameFilterIterator($directory, '/\.fc$/');
		$iterator = new \RecursiveIteratorIterator($filter);
		$status = true;
		
		foreach ($iterator as $file) {
			$filePath = $file->getPathname();
			
			$data = file_get_contents($filePath);
			if ($data) {
				$data = unserialize($data);
				if ($data) {
					$atime = fileatime($filePath);
					if (TIME_NOW - $atime > $data[0]) {
						unlink($filePath);
					}
				}
			}
		}
		self::removeEmptyDirectories($path);
		return $status;
	}
	
	private static function removeEmptyDirectories($path) {
		$empty = true;
		foreach (new \FilesystemIterator($path) as $fileInfo) {
			$empty &= $fileInfo->isDir() && self::removeEmptyDirectories($fileInfo->getPathname()) && rmdir($fileInfo->getPathname());
		}
		return $empty;
	}
}
