<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * Redis Class
 * 
 * This class implements an Redis module for @class Cache
 * Use Cache::getInstance to use the module
 * $cache = Cache::getInstance(instance_name, 'redis')
 * 
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @package		Cache
 * @version		1.0.0
 * @date		$Date: 2010-07-03 21:47:00 +0530 (Fri, 03 Jul 2010	) $
 */
class Redis {
	
	private $cacheGroup = null;
	private $cacheTime = 900;
	private $backend = null;
	
	private static $arBucket = [];
	
	public function __construct($cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);
		
		if (!class_exists('\\Redis')) {
			throw new \Exception("Redis extension not loaded");
		}

		if (!is_null($cacheTime)) $this->cacheTime = $cacheTime;
		if (!is_null($cacheGroup)) $this->cacheGroup = $cacheGroup;
		else $this->cacheGroup = SITE_CODE . '_cache';
		
		$this->backend = new \Redis;
		$this->backend->connect('127.0.0.1', 6379);
		$this->backend->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
		$this->backend->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
		$this->backend->setOption(\Redis::OPT_PREFIX, SITE_CODE);
		\Feedback\Stats::timing("timing.cache.redis.init", (microtime(true) - $ts) * 1000000);
	}
	public function setCacheGroup($cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup =  'cache';
		$this->cacheGroup = $cacheGroup;
	}
	public function setCacheTime($cacheTime = null) {
		if (!is_null($cacheTime)) {
			$this->cacheTime = $cacheTime;
		}
	}
	public function __get($key) {
		return $this->get($key);
	}
	public function get($key, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		$data = $this->backend->get($this->getVersionedKey($cacheGroup . '.' . $key));
		\Feedback\Stats::timing("timing.cache.redis.get", (microtime(true) - $ts) * 1000000);
		return $data !== false ? $data : null;
	}
	public function __set($key, $value) {
		$this->set($key, $value);
	}
	public function set($key, $value, $cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		
		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}

		$versionedKey = $this->getVersionedKey($cacheGroup . '.' . $key);

		if (!$cacheTime) {
			$this->backend->delete($versionedKey);
			return true;
		}
		
		$status = $this->backend->set($versionedKey, $value, (int)$cacheTime);
		\Feedback\Stats::timing("timing.cache.redis.set", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	public function __isset($key){
		return $this->has($key);
	}
	public function has($key, $cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->exists($this->getVersionedKey($cacheGroup . '.' . $key));
	}
	public function __unset($key){
		return $this->delete($key);
	}
	public function delete($key, $cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->delete($this->getVersionedKey($cacheGroup . '.' . $key));
	}
	public function clear($prefix = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		if (is_null($prefix)) $prefix = '';
		
		if ($cacheGroup) {
			list($bucket, $last) = $this->getVersionedKey($prefix ? $cacheGroup . '.' . $prefix : $cacheGroup, true);
			$this->backend->hIncrBy("$bucket.__ver", $last, 1);
			$arBucketName = explode('.', $bucket);
			$bucket =& self::$arBucket;
			foreach ($arBucketName as $bucketName) {
				$bucketName = substr($bucketName, 0, strrpos($bucketName, '#'));
				$bucket =& $bucket['buckets'][$bucketName];
				$last = '';
			}
			if (isset($bucket['buckets'][$last])) {
				$bucket['buckets'][$last]['version']++;
				$bucket['buckets'][$last]['buckets'] = [];
			}
		} else {
			$this->backend->hIncrBy('__ver', '', 1);
			self::$arBucket['buckets']['']['version']++;
			self::$arBucket['buckets']['']['buckets'] = [];
			$this->purge();
		}
		\Feedback\Stats::timing("timing.cache.redis.clear", (microtime(true) - $ts) * 1000000);
		return true;
	}
	public function inc($key, $value = 1, $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->incrBy($this->getVersionedKey($cacheGroup . '.' . $key), $value);
	}
	public function dec($key, $value = 1, $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->decrBy($this->getVersionedKey($cacheGroup . '.' . $key), $value);
	}
	
	public function getBackend() {
		return $this->backend;
	}
	
	public function getVersionedKey($key, $returnArray = false) {
		$bucket =& self::$arBucket;
		$versionedKey = '';
		$arBucket = explode('.', ".$key");
		$itemKey = array_pop($arBucket);

		foreach ($arBucket as $bucketName) {
			if (!isset($bucket['buckets'][$bucketName])) {
				// Load bucket current version from redis
				$version = $this->backend->hget($versionedKey . '__ver', $bucketName);
				if ($version === false) {
					$version = 1;
					// hset causes serialization. To prevent, use hIncrBy
					$this->backend->hIncrBy($versionedKey . '__ver', $bucketName, 1);
				}
				$bucket['buckets'][$bucketName] = ['version' => $version, 'buckets' => []];
			}
			
			$bucket =& $bucket['buckets'][$bucketName];
			$versionedKey .= "$bucketName#{$bucket['version']}.";
		}
		if ($returnArray) {
			return [rtrim($versionedKey, '.'), $itemKey];
		} else {
			assert('strpos($key, "#") === false', new \AssertionError("[Redis] Invalid key $key. Cache keys should not have # character in it"));
			assert('$this->backend->hget($versionedKey . "__ver", $itemKey) === false', new \AssertionError("[Redis] A bucket exists with the name $key. Versioned keys cannot have items sharing a bucket name."));
			return $versionedKey . $itemKey;
		}
	}
	
	private function updateAllBucketVersion($prefix = '', &$bucket = null) {
		if (is_null($bucket)) {
			$bucket =& self::$arBucket['buckets'][''];
		}
		$prefix = "$prefix#{$bucket['version']}";
		$arVersion = $this->backend->hGetAll("$prefix.__ver");

		foreach ($arVersion as $bucketName => $version) {
			$bucket['buckets'][$bucketName] = ['version' => (int)$version, 'buckets' => []];
			$this->updateAllBucketVersion($prefix . '.' .$bucketName, $bucket['buckets'][$bucketName]);
		}
	}
	
	public function purge() {
		$iterator = null;
		$this->updateAllBucketVersion();

		while ($arKey = $this->backend->scan($iterator, "*", 1000)) {
			foreach ($arKey as $key) {
				$bucket = self::$arBucket;
				$bucketName = strtok($key, '#');
				$bucketName = '';
				$version = strtok('.');
				
				while ($bucketName !== false && $version !== false) {
					$bucket = $bucket['buckets'][$bucketName];
					if ($version < $bucket['version']) {
						$this->backend->delete(substr($key, 2));
						break;
					}
					$bucketName = strtok('#');
					$version = strtok('.');
				}
			}
			$this->backend->delete($arKey);
		}
	}
}

