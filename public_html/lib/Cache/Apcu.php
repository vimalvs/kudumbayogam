<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * Apcu Class
 * 
 * This class implements an APCu module for @class Cache
 * Use Cache::getInstance to use the module
 * $cache = Cache::getInstance(instance_name, 'apc')
 * 
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @package		Cache
 * @version		1.0.0
 * @date		$Date: 2010-07-03 21:47:00 +0530 (Fri, 03 Jul 2010	) $
 */
class Apcu {
	
	private $cacheGroup = null;
	private $cacheTime = 900;
	
	public function __construct($cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (!is_callable('apcu_fetch')) {
			throw new \Exception("Apcu extension not loaded");
		}
		
		if (!is_null($cacheTime)) $this->cacheTime = $cacheTime;
		if (!is_null($cacheGroup)) $this->cacheGroup = SITE_CODE . '.' . $cacheGroup;
		else $this->cacheGroup = SITE_CODE . '.cache';
		\Feedback\Stats::timing("timing.cache.apcu.init", (microtime(true) - $ts) * 1000000);
	}
	public function setCacheGroup($cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup =  'cache';
		$this->cacheGroup = SITE_CODE . '.' . $cacheGroup;
	}
	public function setCacheTime($cacheTime = null) {
		if (!is_null($cacheTime)) {
			$this->cacheTime = $cacheTime;
		}
	}
	public function __get($key) {
		return $this->get($key);
	}
	public function get($key, $cacheGroup = null) {
		$ts = microtime(true);
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);

		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}
		$data = apcu_fetch($cacheGroup . '.' . $key, $status);
		assert('strpos($cacheGroup . $key, "#") === false', new \AssertionError("[Apcu] Invalid key $cacheGroup.$key. Cache keys should not have # character in it"));
		assert('(new \APCUIterator("#^" . preg_quote("$cacheGroup.$key\.", "#") . "#"))->getTotalCount() > 0', new \AssertionError("[Apcu] A bucket exists with the name $key. Versioned keys cannot have items sharing a bucket name."));
		\Feedback\Stats::timing("timing.cache.apcu.get", (microtime(true) - $ts) * 1000000);
		return $status ? $data : null;
	}
	public function __set($key, $value) {
		$this->set($key, $value);
	}
	public function set($key, $value, $cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		$status = apcu_store($cacheGroup . '.' . $key, $value, (int)$cacheTime);
		\Feedback\Stats::timing("timing.cache.apcu.set", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	public function __isset($key){
		return $this->has($key);
	}
	public function has($key, $cacheGroup = null) {
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return apcu_exists($cacheGroup . '.' . $key);
	}
	public function __unset($key){
		return $this->delete($key);
	}
	public function delete($key, $cacheGroup = null) {
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return apcu_delete($cacheGroup . '.' . $key);
	}
	public function clear($prefix = null, $cacheGroup = null) {
		$ts = microtime(true);
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		if (is_null($prefix)) $prefix = '';
		$status = apcu_delete(new \APCUIterator("#^" . preg_quote("$cacheGroup.$prefix\.", '#') . "#"));
		\Feedback\Stats::timing("timing.cache.apcu.clear", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	public function inc($key, $value = 1,  $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return apcu_inc($cacheGroup . '.' . $key, $value, $cacheTime);
	}
	public function dec($key, $value = 1,  $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return apcu_dec($cacheGroup . '.' . $key, $value, $cacheTime);
	}
}

