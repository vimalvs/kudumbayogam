<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * NoCache Class
 * 
 * Dummy fallback class for caching
 * @package		Cache
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		1.0.0
 * @date		$Date: 2010-07-02 17:47:00 +0530 (Fri, 02 Jul 2010	) $
 */
class noCache{
	
	private $cacheGroup = 'cache';
	private $cacheTime = 900;
	private $cache = array();
	
	public function __construct($cacheTime = NULL, $cacheGroup = NULL){
		if(is_null($cacheGroup))$this->cacheGroup = $cacheGroup;
	}
	public function setCacheGroup($cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = 'cache';
		$this->cacheGroup = $cacheGroup;
	}
	public function setCacheTime($cacheTime = NULL){
	}
	public function __get($key){
		$this->get($key);
	}
	public function get($key, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		//$key = $cacheGroup . '_' . $key;
		
		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}

		return isset($this->cache[$cacheGroup][$key]) ? $this->cache[$cacheGroup][$key] : NULL;
	}
	public function __set($key, $value){
		$this->set($key, $value);
	}
	public function set($key, $value, $cacheTime = NULL, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		$this->cache[$cacheGroup][$key] = $value;
		return TRUE;
	}
	public function __isset($key){
		return $this->has($key);
	}
	public function has($key, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		return isset($this->cache[$cacheGroup][$key]);
	}
	public function __unset($key){
		return $this->delete($key);
	}
	public function delete($key, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		unset($this->cache[$cacheGroup][$key]);
		return TRUE;
	}
	public function clear($prefix = NULL, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		if (isset($this->cache[$cacheGroup])) {
			$keys = array_keys($this->cache[$cacheGroup]);
			foreach ($keys as $key) {
				if (strpos($key, $prefix) === 0) {
					unset($this->cache[$cacheGroup][$key]);
				}
			}
		}
	}
	public function inc($key, $value = 1,  $cacheTime = NULL, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		$this->cache[$cacheGroup][$key] = (int)$this->get($key, $cacheGroup) + $value;
		return $this->cache[$cacheGroup][$key];
	}
	public function dec($key, $value = 1,  $cacheTime = NULL, $cacheGroup = NULL){
		if(is_null($cacheGroup))$cacheGroup = $this->cacheGroup;
		$this->cache[$cacheGroup][$key] = (int)$this->get($key, $cacheGroup) - $value;
		return $this->cache[$cacheGroup][$key];
	}
}

?>