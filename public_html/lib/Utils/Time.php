<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Time Class
 * 
 * Utility class for time based functions
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */
namespace Utils;

class Time {
	
	
	public static function formatRelative($date) {
		$delta = TIME_NOW - $date;
		if ($delta < 60) {
		  return $delta == 1 ? 'one second ago' : "$delta seconds ago";
		} elseif ($delta < 120) {
		  return 'a minute ago';
		} elseif ($delta < 2700) { // 45 * 60
		  return floor( $delta / 60 ) . ' minutes ago';
		} elseif ($delta < 5400) { // 90 * 60
		  return 'an hour ago';
		} elseif ($delta < 86400) { // 24 * 60 * 60
		  return floor( $delta / 3600 ) . ' hours ago';
		} elseif ($delta < 172800) { // 48 * 60 * 60
		  return 'yesterday';
		} elseif ($delta < 2592000) { // 30 * 24 * 60 * 60
		  return floor( $delta / 86400 ) . ' days ago';
		} elseif ($delta < 31104000) { // 12 * 30 * 24 * 60 * 60
		  $months = floor( $delta / 2592000 );
		  return $months <= 1 ? 'one month ago' : "$months months ago";
		}
		$years = floor($delta / 31536000);
		return $years <= 1 ? "one year ago" : $years . " years ago";
	}

	public static function formatPartialDate($date_string, $format_full = 'F d, Y', $format_month = 'F Y', $format_year = 'Y') {
		if (!empty($date_string)) {
			$date = array_map('intval', explode('-', preg_replace('/-9+\b/', '-00', $date_string)));
			if (empty($date[0]) || $date[0] == 9999) {
				return null;
			} elseif (empty($date[1])) {
				return $date[0];
			} elseif (empty($date[2])) {
				return date($format_month, mktime(0, 0, 0, $date[1], 1, $date[0]));
			} else {
				return date($format_full, mktime(0, 0, 0, $date[1], $date[2], $date[0]));
			}
		}
		return null;
	}

}

