<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Utils Class
 * 
 * Utility class for common functions
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Utils;

class Utils {

	public static function generatePathName($title, $len = NULL, $def = NULL, $separator = '-') {
		$title = preg_replace_callback('/(?:(?<=^|\W)\w(?:\.| ) ?){2,}(?:\w )?/', function ($match) {
			return preg_replace('/[^\w]/', '', $match[0]) . '-';
		}, $title);
		$title = preg_replace('/(\w)[\'′]s /i', '$1s-', $title);
		$title = preg_replace('/[^a-z0-9]+/i', $separator, strtolower(html_entity_decode($title, ENT_QUOTES, 'UTF-8')));
		$title = preg_replace("/\$separator+/", $separator, $title);
		$title = trim($title, $separator);
		if (!is_null($len) && (strlen($title) > $len)) {
			$pos = strrpos(substr($title, 0, $len), $separator);
			if ($pos)$title = substr($title, 0, $pos);
		}
		return !empty($title) ? $title : (is_null($def) ? 'page' : $def);
	}

	// [Hack] Copying array of references copies the references, instead of value. Hence any change in new array will be reflected in old array
	public static function copyArray($array) {
		// Return value of functions are always copied
		return array_map(function($val) {return $val;}, $array);
	}
	
	public static function getPathFromUri($uri, $root = ROOT) {
		if (preg_match('#^(?:https?://[^/]+)?(/.*?)(\?|$)#', $uri, $match)) {
			return $root . $uri;
		}
		return false;
	}
	
	public static function rmdir($path, $breakOnError = false) {
		if (is_dir($path)) {
			$status = true;
			foreach (new \FilesystemIterator($path) as $fileInfo) {
				if ($fileInfo->isDir()) {
					$status = self::rmdir($fileInfo->getPathname());
				} else {
					$status = unlink($fileInfo->getPathname());
				}
				if ($breakOnError && !$status) {
					break;
				}
			}
			return $status && rmdir($path);
		}
		return false;
	}
}