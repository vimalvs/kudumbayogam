<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Time Class
 * 
 * Utility class for image based functions
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */
namespace Utils;

class Image {
	
	private $resource = null;
	private $width = null;
	private $height = null;
	private $canvasWidth = null;
	private $canvasHeight = null;
	private $x1 = 0;
	private $y1 = 0;
	private $x2 = 0;
	private $y2 = 0;
	private $format = null;
	private $dirty = false;
	
	const ERR_FILE_NOT_FOUND = 0;
	const ERR_FILE_READ = 1;
	const ERR_FILE_WRITE = 2;
	const ERR_FORMAT_INVALID = 4;
	
	// const ANCHOR_COORDINATE = 0;
	// const ANCHOR_PERCENTAGE = 1;
	
	const HORIZONTAL = 1;
	const VERTICAL = 2;
	
	const FIT_RESIZE = 0;
	const FIT_CROP = 1;
	const ANCHOR_COORDINATE = 0;
	const ANCHOR_PERCENTAGE = 1;
	
	const ANCHOR_CENTER = 0;
	const ANCHOR_TOP = 2;
	const ANCHOR_BOTTOM = 4;
	const ANCHOR_LEFT = 8;
	const ANCHOR_RIGHT = 16;
	
	const TYPE_GIF = IMAGETYPE_GIF;
	const TYPE_PNG = IMAGETYPE_PNG;
	const TYPE_JPEG = IMAGETYPE_JPEG;
	const TYPE_JPEG2000 = IMAGETYPE_JPEG2000;
	
	
	private function __construct($resource, $width, $height, $format = null) {
		$this->resource = $resource;
		$this->width = $width ?: imagesx($resource);
		$this->height = $height ?: ($resource ? imagesy($resource) : $this->width);
		$this->canvasWidth = $this->x2 = $this->width;
		$this->canvasHeight = $this->y2 = $this->height;
		
		$this->format = $format;
		
		$this->resource = $resource ?: imagecreatetruecolor($this->width, $this->height);
	}
	
	public function getSize() {
		return [$this->width, $this->height];
	}
	
	public function getType() {
		return $this->format;
	}
	
	public function getMimeType() {
		$arMimeType = [
			self::TYPE_GIF => 'image/gif',
			self::TYPE_PNG => 'image/png',
			self::TYPE_JPEG => 'image/jpeg',
			self::TYPE_JPEG2000 => 'image/jpeg',
		];
		return $arMimeType[$this->format];
	}
	
	public function getGdResource() {
		return $this->resource;
	}
	// public function crop($width, $height, $x = 0, $y = 0, $anchor = self::ANCHOR_COORDINATE) {
	// 	if ($anchor === self::ANCHOR_COORDINATE) {
	//
	// 	} else {
	//
	// 	}
	// }
	
	public function flip($direction = self::HORIZONTAL) {
		$resource = imagecreatetruecolor ($this->width, $this->height);
		
		if ($direction === self::HORIZONTAL) {
			$status = imagecopyresampled($resource, $this->resource, 0, 0, $this->width - 1, 0, $this->width, $this->height, -$this->width, $this->height);
		} else if ($direction === self::VERTICAL) {
			$status = imagecopyresampled($resource, $this->resource, 0, 0, 0, $this->height - 1, $this->width, $this->height, $this->width, -$this->height);
		} else {
			throw new \InvalidArgumentException("Invalid flip direction");
		}
		
		if ($status) {
			$this->resource = $resource;
			return $this;
		} else {
			throw new \RunTimeException("Failed to flip image");
		}
	}
	
	public function rotate($deg, $bg_color = null, $ignore_transparent = 0) {
		if ($deg) {
			$resource = imagerotate($this->resource, 0 - $deg, $bg_color, $ignore_transparent); 
			if ($resource) {
				return new Image($resource, null, null, $this->format);
			}
		}
		return $this;
	}
	
	public function resize($width = null, $height = null, $fit = self::FIT_RESIZE, $x = 0.5, $y = 0.5) {
		if ($fit === self::FIT_RESIZE) {
			$isEmptyWidth = empty($width);
			$isEmptyHeight = empty($height);
			if ($isEmptyWidth && $isEmptyHeight) {
				throw new \InvalidArgumentException("At least one dimension should be specified for resizing", self::ERR_FILE_READ);
			} else if ($isEmptyWidth) {
				$width = ($height * $this->width / $this->height);
			} else {
				$height = ($width * $this->height / $this->width);
			}
			// $resource = imagecreatetruecolor($width, $height);
			// imagecopyresampled($resource, $this->resource, 0, 0, 0, 0, $width, $height, $this->width, $this->height);
			$cropWidth = $this->width;
			$cropHeight = $this->height;
			$cropX = 0;
			$cropY = 0;
			
			
		} else if ($fit === self::FIT_CROP) {
			$requiredAspect = $width / $height;
			$actualAspect = $this->width / $this->height;

			if ($requiredAspect > $actualAspect) {
				$cropWidth = $this->width;
				$cropHeight = $this->width / $requiredAspect;
				$cropX = 0;
				$cropY = ($this->height - $cropHeight) * $y;

				// $this->y1 = ($this->height - $cropHeight) * $y;

			} else {
				$cropWidth = $this->height * $requiredAspect;
				$cropHeight = $this->height;
				$cropX = ($this->width - $cropWidth) * $x;
				$cropY = 0;
			}
			// $curImageWidth = ($this->x2 - $this->x1);
			// $curImageHeight = ($this->y2 - $this->y1);
			// $reqImageAspectRatio = $width / $height;
			// $curImageAspectRatio = $curImageWidth / $curImageHeight;
			//
			// if ($reqImageAspectRatio > $curImageAspectRatio) {
			// 	$cropHeight = $curImageWidth / $reqImageAspectRatio;
			//
			// 	$this->y1 = ($curImageHeight - $cropHeight) * $y;
			// 	$this->y2 = $this->y1 + $cropHeight;
			//
			// } else {
			// 	$cropWidth = $curImageHeight * $reqImageAspectRatio;
			// 	// $cropHeight = $this->height;
			// 	$this->x1 = ($curImageWidth - $cropWidth) * $x;
			// 	$this->x2 = $this->x1 + $cropWidth;
			// }
		}
		// $this->canvasWidth = $width;
		// $this->canvasHeight = $height;
		// $this->dirty = true;
		// printr($width, $height, $this->width, $this->height, $cropWidth, $cropHeight);
		// $status = imagecopyresampled($resource, $this->resource, 0, 0, $this->x1, $this->y1, $width, $height, $this->x2 - $this->x1, $this->y2 - $this->y1);
		// exit;
		$resource = imagecreatetruecolor($width, $height);
		$status = imagecopyresampled($resource, $this->resource, 0, 0, $cropX, $cropY, $width, $height, $cropWidth, $cropHeight);
		imagedestroy($this->resource);
		$this->resource = $resource;
		$this->width = $width;
		$this->height = $height;
		
		return $this;
		// return new Image($resource, $width, $height, $this->format);
	}
	
	public function overlay($imgOverlay, $anchor = null, $x = 0, $y = 0, $width = null, $height = null) {
		if (is_null($anchor)) $anchor = self::ANCHOR_COORDINATE;
		if (is_string($imgOverlay)) {
			$imgOverlay = Image::open($imgOverlay);
		}
		$width = $width ?: $imgOverlay->width;
		$height = $height ?: ($imgOverlay->height * $width / $imgOverlay->width);
		
		// printr($anchor & self::ANCHOR_RIGHT, ($anchor & self::ANCHOR_LEFT) === 0);
		// exit;
		if ($anchor & self::ANCHOR_RIGHT) {
			// Right Aligned
			$x = $this->width - $width - $x;
		} elseif (!($anchor & self::ANCHOR_LEFT)) {
			// Center Aligned
			$x = ($this->width - $width) / 2 + $x;
		}
		if ($anchor & self::ANCHOR_BOTTOM) {
			// Bottom Aligned
			$y = $this->height - $height - $y;
		} elseif (!($anchor & self::ANCHOR_TOP)) {
			// Center Aligned
			$y = ($this->height - $height) / 2 + $y;
		}
		// printr($this->height, $height, $y, ($this->height - $height - $y));
		// exit;
		// $x = $x < 0 ? ($this->width + $x - $width) : $x;
		// $y = $y < 0 ? ($this->height + $y - $height) : $y;
		
		$status = imagecopy($this->resource, $imgOverlay->resource, $x, $y, 0, 0, $width, $height);
		
		if ($status) {
			return $this;
		} else {
			throw new \RunTimeException("Failed to copy image");
		}
	}
	
	public function copy($src, $anchor = null, $dX = 0, $dY = 0, $sWidth = null, $sHeight = null, $sX = 0, $sY = 0, $dWidth = null, $dHeight = null) {
		if (is_null($anchor)) $anchor = self::ANCHOR_COORDINATE;
		if (is_string($src)) {
			$src = Image::open($src);
		}
		$sWidth = $sWidth ?: $src->width;
		$sHeight = $sHeight ?: ($src->height * $sWidth / $src->width);
	
		if ($anchor == self::ANCHOR_COORDINATE) {
			// Do Nothing
		} elseif ($anchor & self::ANCHOR_RIGHT) {
			// Right Aligned
			$dX = $this->width - $sWidth - $dX;
		} elseif (!($anchor & self::ANCHOR_LEFT)) {
			// Center Aligned
			$dX = ($this->width - $sWidth) / 2 + $dX;
		}
		
		if ($anchor == self::ANCHOR_COORDINATE) {
			// Do Nothing
		} elseif ($anchor & self::ANCHOR_BOTTOM) {
			// Bottom Aligned
			$dY = $this->height - $sHeight - $dY;
		} elseif (!($anchor & self::ANCHOR_TOP)) {
			// Center Aligned
			$dY = ($this->height - $sHeight) / 2 + $dY;
		}

		if (is_null($dWidth) && is_null($dHeight)) {
			$status = imagecopy($this->resource, $src->resource, $dX, $dY, $sX, $sY, $sWidth, $sHeight);
		} else {
			// printr($src);
			$status = imagecopyresampled($this->resource, $src->resource, $dX, $dY, $sX, $sY, $dWidth, $dHeight, $sWidth, $sHeight);
		}
		
		if ($status) {
			return $this;
		} else {
			throw new \RunTimeException("Failed to copy image");
		}
	}
	
	public function save($file = null, $format = null, $quality = 80, $optimize = false) {
		$format = $format ?: $this->format;
		if (!$format) {
			$arFormat = [
				'gif' => IMAGETYPE_GIF, 'png' => IMAGETYPE_PNG, 'jpg' => IMAGETYPE_JPEG, 'jpeg' => IMAGETYPE_JPEG
			];
			$ext = substr(strrchr($file, '.'), 1);
		}
		// $resource = $this->dirty ? imagecreatetruecolor($width, $height) : $this->resource;
		// if ($this->dirty) {
		// 	$resource = imagecreatetruecolor($this->canvasWidth, $this->canvasHeight);
		// 	imagecopyresampled($resource, $this->resource, 0, 0, $this->x1, $this->y1, $this->canvasWidth, $this->canvasHeight, $this->x2 - $this->x1, $this->y2 - $this->y1);
		// } else {
		// 	$resource = $this->resource;
		// }
		$status = false;
		switch ($format) {
			case IMAGETYPE_GIF:
				$status = imagegif($this->resource, $file);
				break;
			case IMAGETYPE_JPEG:
			case IMAGETYPE_JPEG2000:
				$status = imagejpeg($this->resource, $file, $quality);
				break;
			case IMAGETYPE_PNG:
				$status = imagepng($this->resource, $file);
				break;
			default:
				throw new \InvalidArgumentException("Unsupported file format - $format", self::ERR_FORMAT_INVALID);
		}
		if ($status) {
			if ($optimize && !empty($file)) {
				self::optimize($file, $format, $this->width, $this->height);
			}
		} else {
			throw new \InvalidArgumentException("Failed to create image - $file", self::ERR_FILE_WRITE);
		}
	}
	
	public function filter($filter, $value) {
		$args = func_get_args();
		array_shift($args);
		
		switch ($filter) {
			case 'contrast':
				imagefilter($this->resource, IMG_FILTER_CONTRAST, $args[0]);
				break;
			case 'brightness':
				imagefilter($this->resource, IMG_FILTER_BRIGHTNESS, $args[0]);
				break;
			default:
				throw new \InvalidArgumentException("Unknown filter - $filter");
		}
		
		return $this;
	}
	
	public static function create($width, $height = null, $format = IMAGETYPE_JPEG) {
		return new Image(null, $width, $height ?: $width, $format);
	}
	
	public static function open($file, $format = null, $skipExifOrientation = false) {
		if (!file_exists($file)) {
			throw new \InvalidArgumentException("File not found - $file", self::ERR_FILE_NOT_FOUND);
		} else if (!is_readable($file)) {
			throw new \InvalidArgumentException("File not readable - $file", self::ERR_FILE_READ);
		}
		
		if (!$format && function_exists('exif_imagetype')) {
			$format = exif_imagetype($file);
		}
		
		if ($format) {
			switch ($format) {
				case IMAGETYPE_GIF:
					$resource = imagecreatefromgif($file);
					break;
				case IMAGETYPE_JPEG:
				case IMAGETYPE_JPEG2000:
					$resource = imagecreatefromjpeg($file);
					break;
				case IMAGETYPE_PNG:
					$resource = imagecreatefrompng($file);
					break;
				default:
					throw new \InvalidArgumentException("Unsupported file format - $format", self::ERR_FILE_READ);
			}
		} else {
			$resource = imagecreatefromstring(file_get_contents($file));
		}
		
		if (!$resource) {
			throw new \InvalidArgumentException("Failed to read file", self::ERR_FILE_READ);
		}
		
		list($width, $height, $format) = getimagesize($file);
		
		$image = new Image($resource, $width, $height, $format);

		if (!$skipExifOrientation && in_array($format, [self::TYPE_JPEG, self::TYPE_JPEG2000])) {
			$exif = @exif_read_data($file);
			if ($exif && !empty($exif['Orientation'])) {
				\SiteManager::getLogger()->addInfo("Orientation fixed for image $file");
				$image = $image->fixOrientation($exif['Orientation']);
			// } else {
				// throw new \RunTimeException("Failed to detect image orientation");
			}
		}
		
		return $image;
	}
	
	public function fixOrientation($orientation) {
		
		switch ($orientation) {
			case 1:
				return $this;
			case 2:
				return $this->flip(self::HORIZONTAL);
			case 3:
				return $this->rotate(180);

			case 4:
				return $this->rotate(180)->flip(self::HORIZONTAL);

			case 5:
				return $this->rotate(90)->flip(self::HORIZONTAL);

			case 6:
				return $this->rotate(90);

			case 7:
				return $this->rotate(-90)->flip(self::HORIZONTAL);

			case 8:
				return $this->rotate(-90);

			default: 
				throw new \Exception("Invalid image orientation - '$orientation'");
				
		}
		
	}
	
	public static function optimize($file, $format, $width = null, $height = null) {
		$tmpFile = sys_get_temp_dir() . '/' . uniqid('util_image_optimize_');
		switch ($format) {
			case IMAGETYPE_GIF:
				// $resource = imagecreatefromgif($file);
				break;
			case IMAGETYPE_JPEG:
			case IMAGETYPE_JPEG2000:
				// $resource = imagecreatefromjpeg($file);
				$file = escapeshellarg($file);
				$tmpFile = escapeshellarg("$tmpFile.jpg");
				$status = exec("/usr/local/bin/jpegtran -copy none -optimize -progressive -outfile $tmpFile $file && mv $tmpFile $file", $opt, $ret);
				break;
			case IMAGETYPE_PNG:
				if (is_null($width) || is_null($height)) list($width, $height) = getimagesize($file);
				if (max($width, $height) <= 250) {
					exec("/usr/local/bin/pngnq -s1 -e '.png.nq.png' $file && /usr/local/bin/pngout -y '$file.nq.png' && mv '$file.nq.png' '$file'", $opt, $ret);
					if (file_exists("$file.nq.png")) {
						unlink("$file.nq.png");
					}
				} else {
					exec("/usr/local/bin/pngout -y '$file' '$file.pngout.png' && mv '$file.pngout.png' '$file'", $opt, $ret);
					if (file_exists("$file.pngout.png")) {
						unlink("$file.pngout.png");
					}
				}
				break;
		}
	}
	
	// public static function watermark($file, $x, $y, $width = null, $height = null) {
	//
	// }
	//
}
