<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Color Class
 * 
 * Utility class for manipulating color
 *
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Utils;

class Color {
	
	public static function hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);

		if (strlen($hex) == 3) {
			$r = hexdec($hex[0].$hex[0]);
			$g = hexdec($hex[1].$hex[1]);
			$b = hexdec($hex[2].$hex[2]);
		} else {
			$r = hexdec($hex[0].$hex[1]);
			$g = hexdec($hex[2].$hex[3]);
			$b = hexdec($hex[4].$hex[5]);
		}

		return array($r, $g, $b); // returns an array with the rgb values
	}
	
	public static function getRelativeLuminance($color) {
		$color = array_map(function ($val) {
			$val /= 255;
			if ($val <= 0.03928) {
				$val = $val / 12.92;
			} else {
				$val = pow((($val + 0.055) / 1.055), 2.4);
			}
			return $val;
		}, $color);

		return (0.2126 * $color[0]) + (0.7152 * $color[1]) + (0.0722 * $color[2]);
	}
	
	public static function getLuminosityContrast($color1, $color2 = [0, 0, 0]) {
		$L1 = self::getRelativeLuminance($color1);
		$L2 = self::getRelativeLuminance($color2);
		
		if ($L1 > $L2) {
			return ($L1 + 0.05) / ($L2 + 0.05);
		} else {
			return ($L2 + 0.05) / ($L1 + 0.05);
		}
	}
	
	public static function getBrightness($color) {
		return (($color[0] * 299) + ($color[1] * 587) + ($color[2] * 114)) / 1000;
	}
}