<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * UserMerge Class
 * 
 * Merge Two Users
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;
class UserMerge {
	
	private $user = NULL;
	private $ignoredTables = NULL;
	private $dbName;
	
	public function __construct($user_id = NULL, $ignoredTables = array(), $dbName = NULL) {
		$user = User::loadById($user_id);
		if ($user) {
			if ($user->verified) {
				$this->user = $user;
				$this->ignoredTables = $ignoredTables;
				if (empty($dbName)) $dbName = $GLOBALS['dbName'];
				$this->dbName = $dbName;
			} else {
				throw Exception("User accounts can be merged only to a verified account");
			}
		} else {
			throw Exception("Merging with invalid user");
		}
	}

	public function merge($user_id, &$errorInfo = NULL) {
		if ($this->user) {
			if ($this->user->id != $user_id) {
				if (User::loadById($user_id)) {
					$pdbo = \SiteManager::getDatabase();
					$stmt = $pdbo->query("SELECT table_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = " . $pdbo->quote($this->dbName) . " AND column_name = 'user_id'");
					if ($stmt) {
						while ($tableName = $stmt->fetchColumn(0)) {
							if (in_array($tableName, $this->ignoredTables)) {
								continue;
							} elseif ($tableName === 'user_email_verified') {
								// Delete duplicate email addresses verified with both accounts
								//$pdbo->exec("UPDATE $tableName SET user_id = {$this->user->id} WHERE user_id = $user_id");
							}
							$pdbo->exec("UPDATE {$this->dbName}.$tableName SET user_id = {$this->user->id} WHERE user_id = $user_id");
						}
					}
					return TRUE;
				} else {
					$errorInfo = 'Merging an invalid user';
				}
			} else {
				$errorInfo = 'Cannot merge a user with itself';
			}
		}
		return FALSE;
	}

}
