<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * UserGroup Class
 * 
 * Provides methods for managing users
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;

class UserManager extends User {
	
	/**
	 * Instance Methods / Properties
	 */

	private $updateData = array();
	
	public function __construct($data = NULL) {
		if ($data) {
			if (isset($data['user_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}
	
	public function getUser() {
		return new User($this->data);
	}
	
	public function update($data, &$arError = NULL) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$arError = NULL) {
		$isNew = empty($this->data['user_id']);
		$pdbo = \SiteManager::getDatabase();
		
		unset($this->updateData['user_id']);
		
		extract($this->data);
		array_walk_recursive($this->updateData, function(&$item, $key) {$item = trim($item);});
		extract($this->updateData, EXTR_REFS);
		if (is_null($arError)) $arError = array();
		
		$user_email = strtolower($user_email);
		
		$tmp = mb_strlen($user_name, 'utf-8');
		if ($tmp === 0) $arError['user_name'] = 'Name cannot be empty';
		else if ($tmp < 3 || $tmp > 20) $arError['user_name'] = 'Name should be between 3-20 characters';

		if ($isNew || (isset($this->updateData['user_email']) && ($this->data['user_email'] !== $this->updateData['user_email']))) {
			if (empty($user_name)) $arError['user_email'] = 'Email address is mandatory';
			elseif (!isValidEmail($user_email)) $arError['user_email'] = 'Invalid email address';
			elseif (strlen($user_email) > 60) $arError['user_email'] = 'Email should be 60 chars or less';
		}
		
		if (empty($user_hash)) $this->updateData['user_hash'] = md5(uniqid(10));
		if (empty($group_id)) $this->updateData['group_id'] = 2;

		if (empty($user_gender)) $this->updateData['user_gender'] = NULL;
		if (!empty($user_birthday)) {
			if (!preg_match('/((?:19|20)\d{2})-(\d{2})-(\d{2})/', $user_birthday, $match) || !checkdate($match[2], $match[3], $match[1])) {
				$arError['user_birthday'] = 'Birthday should be a valid date in YYYY-MM-DD format or leave empty';
			}
		}
		if (empty($this->updateData['user_meta_info'])) $this->updateData['user_meta_info'] = array();
		if (isset($this->updateData['user_meta_info'])) {
			$this->updateData['user_meta_info'] = array_filter(array_merge($this->data['user_meta_info'], $user_meta_info));
		}
		
		if (empty($user_level)) $this->updateData['user_level'] = 1;
		
		if (empty($user_is_registered)) $this->updateData['user_is_registered'] = NULL;
		// if (empty($user_level)) $user_level = 1;
		
		$status = empty($arError);
		$user_email = strtolower($user_email);
		if ($status) {
			if (isset($this->updateData['user_meta_info'])) $updateData = array_merge($this->updateData, array('user_meta_info' => serialize($user_meta_info)));
			else $updateData = $this->updateData;
			if ($isNew) {
				if (!isset($updateData['user_verified'])) $updateData['user_verified'] = 0;
				$status = ($pdbo->insertRecord('user_list', $updateData) == 1);
				if ($status) {
					$user_id = $pdbo->lastInsertId();
					\Logger::log("Added New User : $user_name", 'add' , 'user', $user_id);
					$this->data['user_id'] = $user_id;
				} else {
					trigger_error("[UserManager] Failed to create user", E_USER_WARNING);
				}
			} else {
				$status = $pdbo->updateRecord('user_list', compact('user_id'), $updateData);
				// Log event only if the entry was modified
				if ($status) {
					\Cache::getInstance()->delete($user_id, 'user');
					// \Logger::log("Updated User : $user_name", 'update' , 'user', $user_id);
				} elseif ($status === FALSE) {
					trigger_error("[UserManager] Failed to update user info", E_USER_WARNING);
				}
				// Return success if entry is not modified / there is no error
				$status = ($status !== FALSE);
			}
			if ($status) {
				$this->data = array_merge($this->data, $this->updateData);
				$this->updateData = array();
				$cache = \Cache::getInstance();
			}
		}
		return $status;
		
	}
	
	public function ban(&$arError = NULL) {
		return $this->update(array('user_status' => -1), $arError);
	}
	
	public function addEmail($auth_email, $auth_verified = FALSE) {
		$status = \SiteManager::getDatabase()->replaceRecord('user_auth_email', array('auth_email' => $auth_email, 'auth_verified' => (int)$auth_verified, 'user_id' => $this->data['user_id']));
		return ($status !== FALSE);
	}
	
	public function removeEmail($auth_email) {
		$status = \SiteManager::getDatabase()->removeRecord('user_auth_email', array('auth_email' => $auth_email, 'user_id' => $this->data['user_id']));
		return ($status !== FALSE);
	}

	public function addAuth($auth_type, $auth_identifier, $auth_comment = NULL, &$errorInfo = NULL) {
		$user_id = $this->data['user_id'];
		$auth = new UserAuth(compact('auth_type', 'auth_identifier', 'auth_comment', 'user_id'));
		if ($auth->save($errorInfo)) {
			return $auth;
		}
		return FALSE;
	}
	
	public function removeAuth($auth_id, &$errorInfo = NULL) {
		$auth = UserAuth::loadById($auth_id);
		return $auth ? $auth->delete($errorInfo) : FALSE;
	}
	
	public function getAuth() {
		return UserAuth::search(array('user_id' => $this->data['user_id']));
	}
	
	/**
	 * Static Methods
	 */
	public static function quickRegister($email, $name, $arData = array(), $autoLogin = TRUE, &$arError = NULL) {
		$arData = array_merge(array('user_email' => $email, 'user_name' => $name, 'user_is_registered' => NULL, 'group_id' => 3), $arData);
		$user = new UserManager($arData);
		if ($user->save($arError)) {
			if ($autoLogin) {
				$auth = UserAuth::check(UserAuth::TYPE_UNREGISTERED, $user->id);
				$userAuth = \Auth\Session::reload($auth);
				$userAuth->memorize();
			}
			return $user->getUser();
		}
		return FALSE;
	}
	
	public static function getUnregisteredUser($arDetail = NULL, &$arError = NULL) {
		global $userAuth;
		$user_id = 0;
		if ($arDetail) {
			extract($arDetail);
			$submitted = TRUE;
		} else {
			$user_name = getCleanVar('user_name');
			$user_email = getCleanVar('user_email');
			$submitted = (!empty($user_email) || !empty($user_name));
		}

		if (!$userAuth || !$userAuth->isLoggedIn() || ($userAuth->email !== $user_email)) {
			$user = \Auth\UserManager::quickRegister($user_email, $user_name, array(), TRUE, $arError);
			if ($user) $arDetail = array('user_id' => $user->id, 'user_email' => $user_email, 'user_name' => $user_name);
		} elseif (!$userAuth->isRegistered()) {
			if ($submitted) {
				if ($userAuth->name !== $user_name) {
					$mgr = UserManager::loadById($userAuth->id);
					if ($mgr->update(compact('user_name', 'user_email'), $arError)) {
						\Auth\Session::refresh();
					}
				}
			} else {
				$user_name = $userAuth->name;
				$user_email = $userAuth->email;
			}
		} else {
			$user_name = $userAuth->name;
			$user_email = $userAuth->email;
		}
		if ($userAuth) $user_id = $userAuth->id;
		return compact('user_email', 'user_name', 'user_id');
	}
	
	public function setPassword($auth_password, &$arError = NULL) {
		if (empty($auth_password)) $arError['auth_password'] = 'Password cannot be empty';
		elseif (strlen($auth_password) < 6) $arError['auth_password'] = 'Password should have a minimum of 6 characters';

		if (empty($arError)) {
			return UserAuth::setUserPassword($this->data['user_id'], password_hash($auth_password, PASSWORD_DEFAULT));
		} else {
			return false;
		}
	}
	
	private static function flattenConfig($array, $prefix = '') {
		$result = array();
		$prefix = trim("$prefix.", '.');
		$index = empty($prefix) ? 'other' : $prefix;
		$prefix .= '.';
		
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$result = $result + self::flattenConfig($value, $prefix . $key);
			} elseif (is_numeric($key)) {
				$result[$index][] = $value;
			} else {
				$result["$index.$key"] = $value;
			}
		}
		return $result;
	}
	
	public function setConfig($group, $key = null, $value = null, $clear = false) {
		$pdbo = \SiteManager::getDatabase();
		$user_id = $this->data['user_id'];
		$clear_prefix = '';
		$arData = [];
		
		if (is_string($group) && is_null($value)) {
			$clear = true;
			$clear_prefix = is_null($key) ? $group : "$group.$key";
		} elseif (is_array($group)) {
			// Options is provided as a multi dimentional array
			$clear_prefix = null; // Clear all existing data
			$data = self::flattenConfig($group);
		} elseif (is_array($key)) {
			$clear_prefix = $group; // Clear current group alone
			$data = self::flattenConfig($key, "$group.");
		} else {
			$data = ["$group.$key" => $value];
			if ($clear) {
				trigger_error('Clearing config is allowed only when data is set as array', E_USER_WARNING);
				$clear = false;
			}
		}
		
		foreach ($data as $config => $value) {
			if (strlen($config) > 50) {
				trigger_error('User configuration key is too long', E_USER_WARNING);
				return false;
			}
			if (is_array($value)) {
				foreach ($value as $v) {
					if (strlen($v) > 255) {
						trigger_error('User configuration value is too long', E_USER_WARNING);
						return false;
					}
					$arData[] = array('user_id' => $user_id, 'config' => $config, 'value' => $v);
				}
			} else {
				if (strlen($v) > 255) {
					trigger_error('User configuration value is too long', E_USER_WARNING);
					return false;
				}
				$arData[] =  compact('user_id', 'config', 'value');
			}
		}

		$status = $pdbo->beginTransaction();
		if ($clear) {
			$status = $status && $pdbo->deleteRecord('user_config', "user_id = {$this->data['user_id']} " . ($clear_prefix ? (" AND config LIKE " .  $pdbo->quote($clear_prefix)) : ''), null, null);
		} else {
			$status = $pdbo->deleteRecord('user_config', ['user_id' => $user_id, 'config' => array_keys($data)], null);
		}
		if (!empty($arData)) {
			$status = $status && ($pdbo->insertRecord('user_config', $arData) !== FALSE);
		}
		
		$status = $status && $pdbo->commit();
		if (!$status) $pdbo->rollback();
		
		return $status;
	}
	
	// public static function quickRegister() {
	// 	if (empty($arData['name'])) $errorInfo['user_name'] = array('invalid_input', 'Name cannot be empty');
	// 	elseif (!isValidEmail($arData['email'])) $errorInfo['user_email'] = array('invalid_input', 'Invalid email address');
	// 	elseif (strlen($arData['password']) < 5) $errorInfo['user_password'] = array('invalid_input', 'Password should have at least 5 characters');
	// 	
	// }
	// 
	// public static function connect($userid, $arData) {
	// 	if (!isset($arData['type']) || !isset($arData['identity'])) self::$errorInfo = array('invalid_input', 'Both identity and account type is required for identity based login');
	// 	elseif (!(($pdbo = SiteManager::getDatabase()) && $pdbo->insertRecord('user_identity', array_merge(array('userid' => $userid), $arData)))) {
	// 		self::$errorInfo = array('invalid_input', 'Failed to add login info to database');
	// 	} else {
	// 		return TRUE;
	// 	}
	// 	throw new Exception('Failed to connect login with account');
	// }
	
}

// class InternalErrorException extends Exception {
// 	private $arError = NULL;
// 	private function setErrorInfo($arError) {
// 		$this->arError = $arError;
// 	}
// 	
// 	public function __construct($message = '', $code = 0, $previous = NULL) {
// 		if (is_array($message)) {
// 			parent::__construct('', $code, $previous);
// 			$this->setErrorInfo($message);
// 		} else {
// 			parent::__construct($message, $code, $previous);
// 			$this->setErrorInfo(array($message));
// 		}
// 	}
// 
// 	public function errorInfo() {
// 		return $this->arError;
// 	}
// }