<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Authenticator Class
 * 
 * Authenticator provides password based and identity based user authentication facility
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;
class Authenticator {

	const TYPE_UNREGISTERED = 0;
	const TYPE_REGISTERED = 1;
	const TYPE_CONNECTED = 2;
	const TYPE_OAUTH = 3;
	const TYPE_OPENID = 4;
	const TYPE_TOKEN = 5;

	private $user = NULL;
	private $status = FALSE;
	private static $instance = NULL;
	private $provider = NULL;
	
    protected function __construct() {
		$this->user = User::loadGuest();
	}
	protected function __clone() {}
	
	public function authenticate($type, $identifier = NULL, $condition = NULL, $provider = NULL, $remember = TRUE) {
		$status = FALSE;
		
		switch ($type) {
			case self::TYPE_UNREGISTERED:
			case self::TYPE_TOKEN:
				$user = is_array($condition) ? User::loadById($condition['user_id']) : $condition;
				if ($user && ($user->hash === $identifier)) {
					$status = TRUE;
				}
				break;
			case self::TYPE_REGISTERED:
				$user = is_array($condition) ? User::loadByEmail($condition['user_email']) : $condition;
				if ($user) {
					$status = (UserAuth::load($type, $user->id . '-' . md5($identifier . $user->hash)) !== FALSE);
				}
				break;
			case self::TYPE_CONNECTED:
			case self::TYPE_OAUTH:
			case self::TYPE_OPENID:
				$auth = UserAuth::load($type, $identifier);
				if ($auth) {
					$user = $auth->getUser();
					$status = TRUE;
				}
				break;
			default:
				trigger_error('[Authenticator] Invalid authentication method');
		}
		$this->status = $status;
		if ($status) {
			$this->user = $user;
			$this->provider = $provider;
			return TRUE;
		}
		$this->user = User::loadGuest();
		$this->provider = NULL;
		return FALSE;
	}
	
	public function check() {
		return $this->status;
	}
	
	public function getUser() {
		return $this->user;
	}
	
	public function getProvider() {
		return $this->provider;
	}

	
	/**
	 * Static Methods
	 */
	
	public static function getInstance() {
		if (NULL === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
}