<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/
namespace Query;

class Filter {
	private static $operators = array(
		'=' => Condition::LIKE,
		'~' => Condition::FULLTEXT,
		'==' => Condition::EQUAL,
		'>' => Condition::GT,
		'<' => Condition::LT,
		'>=' => Condition::GTE,
		'<=' => Condition::LTE
	);
	
	private static function _parse($val) {
		if (preg_match('/^(!?)((?:~|==|=|>|<|>=|<=)?)(.*)/', $val, $tmp)) {
			if (empty($tmp[2])) {
				$tmp[2] = is_numeric($val) ? '==' : '=';
			}
			$data = array($tmp[3], static::$operators[$tmp[2] ?: '='], !empty($tmp[1]));
		} else {
			$data = array($val, static::$operators[is_numeric($val) ? '==' : '='], false);
		}
		return $data;
	}
	
	private static function _arrayInfo($array, &$is_numeric = NULL, &$is_associative = NULL) {
		$count1 = count($array);
		$count2 = count(array_filter(array_keys($array), 'is_string'));
		$is_numeric = $count2 === 0;
		$is_associative = $count1 === $count2;
	}
	
	public static function parse($qf) {
		$filters = array();
		self::_arrayInfo($qf, $is_numeric, $is_associative);
		
		// $qc = new Condition(null, $is_numeric ? Condition::ANY : Condition::ALL);
		$filters = array();
		
		foreach ($qf as $key => $value) {
			if (is_int($key)) {
				$filters[] = self::parse($value);
			} else {
				if (is_array($value)) {
					if (isset($value['RANGE_START'])) {
						$RANGE_START = $value['RANGE_START'];
						$RANGE_END = $value['RANGE_END'];
						$hasStart = ($RANGE_START !== '');
						$hasEnd = ($RANGE_END !== '');
						if ($hasStart && $hasEnd) $filters[$key] = [[$RANGE_START, $RANGE_END], Condition::RANGE, false];
						elseif ($hasStart) $filters[$key] = [$RANGE_START, Condition::GTE, false];
						elseif ($hasEnd) $filters[$key] = [$RANGE_END, Condition::LT, false];
					} else {
						foreach ($value as $val) {
							$filters[$key][] = self::_parse($val);
						}
					}
				} else {
					$filters[$key] = self::_parse($value);
				}
			}
		}
		return $filters;
	}
	
	public static function generateCondition($filters) {
		$condition = array();
		self::_arrayInfo($filters, $is_numeric, $is_associative);
		
		$qc = new Condition(null, $is_numeric ? Condition::ANY : Condition::ALL);
		
		foreach ($filters as $key => $value) {
			if (is_int($key)) {
				$qc->add(self::generateCondition($value));
			} else {
				if (is_array($value[0])) {
					if ($value[1] === Condition::RANGE) {
						$qc->add($key, $value[0], $value[1]);
					} else {
						$tmp = new Condition(NULL, Condition::ANY);
						foreach ($value as $val) {
							$op = $val[1];
							if ($val[2]) $op != Condition::NOT;
							$tmp->add($key, $val[0], $op);
						}
						$qc->add($tmp);
					}
					
				} else {
					$op = $value[1];
					if ($value[2]) $op != Condition::NOT;
					$qc->add($key, $value[0], $op);
				}
			}
		}
		return $qc;
	}
}