<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Query;

class Condition implements \Iterator, \ArrayAccess {
	const LIKE = 0;
	const FULLTEXT = 1;
	const EQUAL = 2;
	const RANGE = 4;
	const GT = 5;
	const LT = 6;
	const GTE = 7;
	const LTE = 8;
	const ANY = 64;
	const ALL = 65;
	const NOT = 128;
	
	private $arCondition = array();
	private $position = 0;
	private $type = self::ALL;
	
	public function __construct($conditions = array(), $type = Condition::ALL) {
		if ($conditions instanceof Condition) {
			$this->arCondition[] = $conditions;
		} else {
			if (is_string($conditions)) $conditions = func_get_args();
			else {
				$this->type = $type;
				if (null === $conditions) $conditions = array();
			}
		
			if (is_array($conditions)) {
				if (isset($conditions[0])) {
					if (!is_array($conditions[0])) $conditions = array($conditions);
					foreach ($conditions as $condition) {
						if (isset($condition[2])) {
							$this->add($condition[0], $condition[1], $condition[2], isset($condition[3]) ? $condition[3] : NULL);
						} else {
							$this->add($condition[0], $condition[1], Condition::EQUAL);
						}
					}
				} else {
					foreach ($conditions as $key => $val) {
						$this->add($key, $val, Condition::EQUAL);
					}
				}
			}
		}
	}
	
	public function add($field, $val = NULL, $relation = Condition::EQUAL, $opt = NULL) {
		if (!empty($field)) $this->arCondition[] = ($field instanceof Condition) ? $field : array($relation, $field, $val, $opt);
		return $this;
	}
	
	public function type($type = NULL) {
		if (is_null($type)) {
			return $this->type;
		} else {
			$this->type = $type;
		}
		return $this;
	}
	
	public function hasField($field) {
		$flag = false;
		foreach ($this->arCondition as $condition) {
			if (is_array($condition)) {
				$flag = ($condition[1] === $field);
			} else {
				$flag = $condition->hasField($field);
			}
			if ($flag) return true;
		}
		return false;
	}
	
	/**
	 * Methods from Iterator Interface
	 */
	public function rewind() {
		$this->position = 0;
	}

	public function current() {
		return $this->arCondition[$this->position];
	}

	public function key() {
		return $this->position;
	}

	public function next() {
		++$this->position;
	}

	public function valid() {
		return isset($this->arCondition[$this->position]);
	}
	
	/**
	 * Methods from ArrayAccess Interface
	 */
	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->arCondition[] = $value;
		} else {
			$this->arCondition[$offset] = $value;
		}
	}
	
	public function offsetExists($offset) {
		return isset($this->arCondition[$offset]);
	}
	
	public function offsetUnset($offset) {
		unset($this->arCondition[$offset]);
	}
	
	public function offsetGet($offset) {
		return isset($this->arCondition[$offset]) ? $this->arCondition[$offset] : NULL;
	}

}
class_alias('\\Query\\Condition', 'QC');