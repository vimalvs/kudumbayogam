<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * HttpRequest Class
 * 
 * HTTP Request Handler
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-05-18 14:43:00 +0530 (Wed, 18 May 2011	) $
 */

class HttpRequest {
	
	private static $device = NULL;
	private static $userAgent = NULL;
	private static $initialized = FALSE;
	public static $isMobileEdition = FALSE;
	private static $arScreenSize = array('240x320' => 'VGA 240x320', '480x640' => 'VGA 480x640', '320x240' => 'VGA 320x240', '640x480' => 'VGA 640x480', '240x400' => 'WVGA 240x400', '480x800' => 'WVGA 480x800', '400x240' => 'WVGA 400x240', '800x480' => 'WVGA 800x480', '320x480' => 'iPhone 320x480', '480x320' => 'iPhone 480x320', '640x960' => 'iPhone 640x960', '960x640' => 'iPhone 960x640', '480x272' => 'PSP 480x272', '272x480' => 'PSP 272x480', '176x220' => 'Phone 176x220', '220x176' => 'Phone 220x176');
	
	public static function initialize() {
		self::$userAgent = strtolower(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
	}
	

	public static function isAjax() {
		static $isAjaxRequest = NULL;
		if (is_null($isAjaxRequest)) {
			$isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') || isset($_GET['jsonp_callback']);
		}
		return $isAjaxRequest;
	}
	
	public static function isDesktopDevice() {
		$ua = self::$userAgent;
		
		if (!is_null(self::$device)) {
			return (self::$device === 'desktop');
		}

		if (!(self::isMobilePhone() || preg_match('/ipad|iphone|android|webos|maemo|blackberry|hpwos/', $ua))) {
			self::$device = 'desktop';
			return TRUE;
		}
		return FALSE;
	}
	
	public static function isTabletDevice() {
		$ua = self::$userAgent;

		if (!is_null(self::$device)) {
			return (self::$device === 'tablet');
		}

		if ((stripos($ua, 'tablet') !== FALSE) // All Tablets
		  || (stripos($ua, 'ipad') !== FALSE && stripos($ua, 'webkit') !== FALSE) // iPad
		  || ((stripos($ua, 'android') !== FALSE) && (stripos($ua, 'mobile') === FALSE)) || (stripos($ua, 'googletv') !== FALSE) // Android Tablet
		  || (stripos($ua, 'playbook') !== FALSE)  // BlackBerry Tablet
		  || ((stripos($ua, 'hpwos') !== FALSE) && (stripos($ua, 'tablet') !== FALSE)) // HP WebOS Tablet
		) {
			self::$device = 'tablet';
			return TRUE;
		}
		return FALSE;
	}
	
	public static function isSmartPhone() {
		$ua = self::$userAgent;
		
		if (!is_null(self::$device)) {
			return (self::$device === 'smartphone');
		}
		
		if (preg_match('/iphone|ipod/i', $ua) // iPhone / iPod
		  || ((stripos('android', $ua) !== FALSE) && (stripos('mobile', $ua) !== FALSE)) // Android Phone
		  || preg_match('/blackberry95|blackberry 98/i', $ua) // BlackBerry
		  || (stripos('webos', $ua) !== FALSE) // Palm WebOS
		  || (stripos('maemo', $ua) !== FALSE) || ((stripos('linux', $ua) !== FALSE) && (stripos('tablet', $ua) !== FALSE)) // Maemo
		) {
			self::$device = 'smartphone';
			return TRUE;
		}
		return FALSE;
	}
	
	public static function isMobilePhone() {
		
		if (!is_null(self::$device)) {
			return (self::$device === 'mobilephone');
		}

		$httpAccept = strtolower(isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : '');
		$arMobileUserAgent = array(
			'acs-','alav','alca','amoi','audi','aste','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco',
			'eric','hipt','htc','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp',
			'mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','opwv','palm','pana','pant','pdxg','phil','play','pluc',
			'port','prox','qtek','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar',
			'sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr',
			'webc','winw','winw','xda-'
		);
		if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])
			|| (strpos($httpAccept, 'text/vnd.wap.wml') !== FALSE) || (strpos($httpAccept, 'application/vnd.wap.xhtml+xml') !== FALSE) 
			|| preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|ipod|phone|vodafone|o2|pocket|mobi|pda|psp)/i', self::$userAgent)
			|| in_array(substr(self::$userAgent, 0, 4), $arMobileUserAgent)){
			self::$device = 'mobilephone';
			return TRUE;
		}
		return FALSE;
	}
	
	public static function isDebugMode() {
		return self::$debugMode;
	}
	
	public static function enableMobileRedirection() {
		if (!empty($_GET['mob'])) {
			self::$isMobileEdition = TRUE;
			cache(120);
		} else {
			if (isset($_GET['skip_ua_check'])) {
				setcookie('skip_ua_check', 1, TIME_NOW + 2592000, '/', COOKIE_DOMAIN, FALSE, TRUE);
			} elseif (!isset($_COOKIE['skip_ua_check']) && (static::isSmartPhone() || static::isMobilePhone())) {
				header('Location: ' . qsAddParam(MOBILE_HOME . $_SERVER['REQUEST_URI'], array('ua_redir' => 1)), TRUE, 302);
				exit;
			}
		}
	}
	
	public static function getScreenSize() {
		if (!defined('SCREEN_RESOLUTION')) {
			$screen_res = !empty($_POST['screen_res']) ? $_POST['screen_res'] : (!empty($_GET['screen_res']) ? $_GET['screen_res'] : (!empty($_COOKIE['screen_res']) ? $_COOKIE['screen_res'] : ''));	
			$arScreenResolution = array('240x320', '480x640', '320x240', '640x480', '240x400', '480x800', '400x240', '800x480', '320x480', '480x320', '640x960', '960x640', '480x272', '272x480', '176x220', '220x176');
			if (!isset(self::$arScreenSize[$screen_res])) {
				$screen_res = '240x320';
				define('SCREEN_RESOLUTION_SET', FALSE);
			} else {
				define('SCREEN_RESOLUTION_SET', TRUE);
			}
			define('SCREEN_RESOLUTION', $screen_res);
			$tmp = explode('x', $screen_res);
			define('SCREEN_WIDTH', (int)$tmp[0]);
			define('SCREEN_HEIGHT', (int)$tmp[1]);
		} else {
			$tmp = explode('x', SCREEN_RESOLUTION);
		}
		return $tmp;
	}
	
	public static function getAvailableScreenSizes() {
		return self::$arScreenSize;//array('240x320', '480x640', '320x240', '640x480', '240x400', '480x800', '400x240', '800x480', '320x480', '480x320', '640x960', '960x640', '480x272', '272x480', '176x220', '220x176');
	}
}

HttpRequest::initialize();