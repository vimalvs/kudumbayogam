<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Layout Class
 * 
 * Support for multiple layouts in themes
 * 
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2015-07-04 21:20:00 +0530 (Sat, 21 Jul 2015	) $
 */
namespace Theme;

class Layout {
	protected $layoutInfo = null;
	protected $layoutName = null;
	protected $theme = null;
	
	public function __construct($theme, $layoutName = 'default') {
		$this->theme = $theme;
		$this->layoutName = $layoutName;
		try {
			$this->layoutInfo = \SiteManager::getConfig("theme_layout_{$theme->getName()}::{$layoutName}", ['content_areas' => []]);
		} catch (\Exception $e) {
			$layoutInfo = $this->reload();
			$layoutInfo['content_areas'] = array_keys($layoutInfo['content_areas']);
			$this->layoutInfo = $layoutInfo;
		}
	}
	
	public function reload() {
		$layoutInfo = json_decode(file_get_contents($this->theme->getPath() . "/layouts/$this->layoutName.json"), true);
		if ($layoutInfo) {
			return $layoutInfo;
		} else {
			throw new \RuntimeException("Invalid layout");
		}
	}
	
	public function getContentAreas() {
		if (empty($this->arContentArea)) {
			foreach ($this->layoutInfo['content_areas'] as $contentArea) {
				$this->arContentArea[$contentArea] = new ContentArea($this, $contentArea);
			}
		}
		return $this->arContentArea;
	}
	
	public function getContentAreaList() {
		return $this->layoutInfo['content_areas'];
	}
	
	public function getTheme() {
		return $this->theme;
	}
	
	public function getName() {
		return $this->layoutName;
	}
}