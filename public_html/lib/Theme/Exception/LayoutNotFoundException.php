<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Exception
 * 
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 */

namespace Theme\Exception;

class LayoutNotFoundException extends \InvalidArgumentException implements ExceptionInterface {

}