<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Content Area Manager Class
 * 
 * Support for content areas in layouts
 * 
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */
namespace Theme;

class ContentAreaManager extends ContentArea {
	
	public function activate($reload = false) {
		$arContentArea = $this->layout->reload()['content_areas'];
		
		if (isset($arContentArea[$this->name])) {
			$defaults = \SiteManager::getConfig("wdgt-{$this->namespace}");
			if (!$defaults) {
				\SiteManager::getConfig("wdgt-{$this->namespace}", []);
			}
			return \SiteManager::setConfig("theme_layout_{$this->namespace}", $arContentArea[$this->name]);
		} else {
			throw new Exception("Invalid content area");
		}
	}
	
	public function deactivate() {
		return \SiteManager::setConfig("theme_layout_{$this->namespace}", null);
	}
	
	public function getDetails() {
		return \SiteManager::getConfig("theme_layout_{$this->namespace}");
	}
	
	public function clear($section) {
		$section = strtr($section, '.', '_');
		$sectionBucket = preg_replace(['#/#', '#\.$#'], ['.', '.home'], $section, 2, $count);
		$sectionBucket = substr($sectionBucket, 0, strrpos($sectionBucket, '.'));
		\Cache::getInstance()->clear("{$this->namespace}{$sectionBucket}", 'c_area');
	}
	
}