<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * HttpResponse Class
 * 
 * HTTP Response Handler
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-05-18 14:43:00 +0530 (Wed, 18 May 2011	) $
 */

class HttpResponse {
	const DB_RESOURCE_NOT_FOUND = 1;
	public static $message = NULL;
	
	public function getMessage() {
		if (is_null(self::$message)) {
			self::$message = new \Message;
		}
		return self::$message;
	}
	
	public static function error($status = 503, $options = array()) {
		if ($status === HttpResponse::DB_RESOURCE_NOT_FOUND) {
			$status = DB_DEFINED ? 404 : 503;
		}
		if (!in_array($status, array(404, 503))) $status = 404;
		include ROOT . "/$status.php";
	}
	
	public static function redirect($url, $params = array(), $session = FALSE, $status = 301) {
		header('Location: ' . $url, TRUE, 301);
		exit;
	}
}

//HttpResponse::initialize();
