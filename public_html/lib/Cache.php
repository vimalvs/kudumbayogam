<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Cache Class
 * 
 * Class for data caching
 * @package		Cache
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		1.0.0
 * @date		$Date: 2010-07-02 17:47:00 +0530 (Fri, 02 Jul 2010	) $
 */

/**
 * Cache class. This is a singleton class. Create / retrieve a cache object using
 * $cache = Cache::getInstance([$instance, [$cacheType, [$cacheTime]]]);
 */
class Cache {

    private static $xcobj = array();
	private static $cache = null;
	private static $db = null;

	/**
	 * Dummy constructor to prevent direct object creation
	 */
	private function __construct() {}

	/**
	 * Prevent cloning of Cache object
	 */
	public final function __clone() {
		throw new BadMethodCallException("Clone is not allowed");
	} 

	/**
	 * Creates / returns a cache object of $cacheType
	 * @param string $instance This parameter can be used to generate a separate cache object / retrieve a previously
	 *               created instance
	 * @param string $cacheType Cache type to be uesd
	 * @param integer $cacheTime Default cache interval
	 * 
	 * @return object Cache object of type $cacheType
	 */
	public static function getInstance($instance = 'cache', $cacheType = CACHE_TYPE, $cacheTime = NULL) {
		$class = "\\Cache\\" . CACHE_TYPE;

		if (!(isset(self::$xcobj[$instance]) && self::$xcobj[$instance] instanceof $class)) {
			if (!class_exists($class, false)) {
				include __DIR__ . "/Cache/$cacheType.php";
			}
			try {
				self::$xcobj[$instance] = new $class;
			} catch (\Exception $e) {
				trigger_error($e->getMessage(), E_USER_ERROR);
				self::$xcobj[$instance] = new \Cache\FileCache;
			}
		}
		self::$cache =& self::$xcobj[$instance]; 
		return self::$cache;
	}
	
	public static function load($key, $cacheGroup = NULL, $cacheTime = NULL, $load, $forceExpire = TRUE, $lockPeriod = FALSE, $forceCacheReload = FALSE) {
		if (is_null($forceExpire)) $forceExpire = TRUE;
		if (is_null($lockPeriod)) $lockPeriod = FALSE;

		$data = self::$cache->get($key, $cacheGroup);
		if (!$data || ($forceExpire && ($data['time'] < TIME_NOW)) || $forceCacheReload) {
			$locked = FALSE;
			if ($lockPeriod) {
				$tsLock = self::$cache->get($key . '_lock', $cacheGroup . '_clock');
				$locked = $tsLock && ($tsLock > (TIME_NOW - $lockPeriod));
				if (!$locked) self::$cache->set($key . '_lock', TIME_NOW, $lockPeriod, $cacheGroup . '_clock');
				elseif ($data) $data = $data['data'];
			}
			if (!$locked) {
				if (is_callable($load)) {
					$data = call_user_func_array($load, [$key, $cacheGroup, &$cacheTime, &$forceExpire]);
				} elseif ($load) {
					$data = $load;
				} else {
					$data = FALSE;
				}
				if ($data !== FALSE) {
					if ($forceExpire) {
						if (is_null($cacheTime)) $cacheTime = 900;
						$save = array('time' => TIME_NOW + $cacheTime, 'data' => $data);
						$cacheTime += $forceExpire;
					} else {
						$save = $data;
					}
					if ($cacheTime >= 0) self::$cache->set($key, $save, $cacheTime, $cacheGroup);
				}
				if ($lockPeriod) {
					self::$cache->delete($key . '_lock', $cacheGroup . '_clock');
				}
			} 
		} elseif ($forceExpire) {
			// Return the data part
			$data = $data['data'];
		}
		return $data;
	}
}

// Initialize
Cache::getInstance();