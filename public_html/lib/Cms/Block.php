<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Block Class
 * 
 * Class for cms block related actions
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class Block {
	protected $data = array();
	public static function load($key) {
		$db = \SiteManager::getDatabase();
		if ($db) {
			$block = $db->getRecord('cms_block', array('block_key' => $key));
			if ($block) {
				// Any updations here should be performed in BlockManager::save too
				return new static($block);
			}
		}
		return FALSE;	
	}

	public function __construct($data) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	public function __get($key){
		return isset($this->data['block_' . $key]) ? $this->data['block_' . $key] : NULL;
	}
	
	public function getData($key = NULL, $prefix = 'block_') {
		return is_null($key) ? $this->data : $this->data[$prefix . $key];
	}
	
	public static function search($condition = NULL, $order = NULL, $limit = NULL, &$count = FALSE) {
		$arItem = array();
		$db = \SiteManager::getDatabase();
		if ($order && !is_array($order)) $order = array($order, 'ASC');
		if ($limit && !is_array($limit)) $limit = array(0, $limit);

		if ($count !== FALSE) {
			$count = $db->getCount('cms_block', $whereSql);
		}

		if (is_string($condition)) {
			$whereSql = "block_key LIKE " . $db->quote("{$condition}_%");
		} else {
			$whereSql = empty($condition) ? 1 : $db->genQuery($condition);
		}

		$stmt = $db->search('cms_block', $whereSql, $order, $limit, 'block_key, block_title');
		$arItem = $stmt ? $stmt->fetchAll(\PDO::FETCH_KEY_PAIR) : FALSE;

		return $arItem;
	}
	
	public static function get($key, $cache = FALSE) {
		if ($cache) {
			return \Cache::load($key, 'cms_block', 300, function () use ($key) {
				return Block::get($key, FALSE);
			});
		} else {
			$block = self::load($key);
			return $block ? $block->data['block_content'] : FALSE;
		}
	}
}