<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * PageManager Class
 * 
 * Class for managing CMS pages
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class PageManager extends Page {
	
	private $updateData = array();
	private $isNew = false;
	private static $tidyConfig = array('clean' => 'yes', "indent" => true, 'output-html' => true, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => true);
	protected static $db = null;
	
	public function __construct($data = null, $isNew = true) {
		$this->isNew = $isNew;
		if ($data) {
			if ($isNew) {
				$this->updateData = $data;
			} else {
				$this->data = $data;
			}
		}
	}

	public function delete(&$error = null) {
		$status = static::$db->deleteRecord('cms_page', array('page_key' => $this->data['page_key']));
		if ($status) {
			\Logger::log("Deleted CMS Page : " . $this->data['page_content_title'], 'delete', 'cms_page', $this->data['page_key']);
		}
		return $status;
	}
	
	public function update($data, &$arError = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$errorInfo = null) {
		// $isNew = empty($this->data['page_key']);
		$pdbo = \SiteManager::getDatabase();
		
		if (empty($this->updateData)) {
			return true;
		} elseif (!$this->isNew) {
			unset($page_key);
			if (!isset($data['page_pathname'])) $data['page_pathname'] = null;
		}
		extract($this->data);
		extract($this->updateData, EXTR_REFS);

		if (is_null($errorInfo)) $errorInfo = array();
		// $page_allow_comments = (bool)$page_allow_comments;
		
		$ext = pathinfo($page_pathname, PATHINFO_EXTENSION);
		if (empty($page_title)) $errorInfo['page_title'] = 'Page title is mandatory';
		if (empty($page_content_title)) $errorInfo['page_content_title'] = 'Content title is mandatory';
		
		if (empty($page_key)) $errorInfo['page_key'] = 'Page key cannot be empty';
		elseif ($this->isNew && Page::loadById($page_key)) $errorInfo['page_key'] = 'Page key already in use';
		
		if ($page_pathname && $pdbo->getRecord('cms_page', $this->isNew ? compact('page_pathname') : "page_pathname = {$pdbo->quote($page_pathname)} AND page_key != '$page_key'")) {
			$errorInfo['page_pathname'] = 'The pathname already exists';
		}
		if ($page_indexed && !$page_pathname) $errorInfo['page_indexed'] = 'Page can be added to solr index only if it has a unique pathname';
		
		$config = array('clean' => 'yes', "indent" => true, 'output-html' => true, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => true);
		foreach (array('page_content1', 'page_content2', 'page_content3') as $c) {
			if (!isset($this->updateData[$c]) || preg_match('/^a:\d+:{/', $this->updateData[$c])) continue;
			$tidy = tidy_parse_string($this->updateData[$c], $config, 'utf8');
			if ($tidy->cleanRepair()) {
				$this->updateData[$c] = trim($tidy);
			} else {
				$errorInfo[$c] = 'Failed to clean input. The code is not valid HTML';
			}
		}
		
		$page_options = empty($page_options) ? array() : array_filter(array_map('trim', $page_options));

		$serialized_options = empty($page_options) ? 'a:0:{}' : serialize($page_options);

		if (($tmp = strlen($serialized_options)) > 250) {
			$errorInfo['page_options'] = "Options size exceeds maximum permitted length by " . ($tmp - 250) . ' characters';
		}

		$status = empty($errorInfo);
		if ($status) {
			/**
			 * Data Validated successfully
			 */
			$page_time_created = TIME_NOW;
			
			if (empty($page_pathname)) {
				$page_pathname = null;
			}
			
			$extra = array('page_options' => $serialized_options);

			if ($this->isNew) {
				$status = $pdbo->insertRecord('cms_page', $extra + $this->updateData);
				if ($status) {
					$this->data['page_id'] = $pdbo->lastInsertId();
					\Logger::log("Added New CMS Page : $page_content_title", 'add' , 'cms_page', $page_key);
				}
			} else {
				$status = $pdbo->updateRecord('cms_page', array('page_key' => $page_key), $extra + $this->updateData);
				// Log event only if the entry was modified
				if ($status !== false) {
					\Logger::log("Updated CMS Page : $page_content_title", 'update' , 'cms_page', $page_key);
					\Cache::getInstance()->clear("page_{$page_key}", 'cms');
				}
				// Return success even if data was not modified
				$status = ($status !== false);
			}
			if ($status) {
				// Update Solr on successful DB Update
				$this->data = array_merge($this->data, $this->updateData);
				$this->updateData = array();
				if ($page_indexed) {
					if (!$this->updateSolr()) {
						$errorInfo[] = 'Failed to update solr.';
					}
				}
			}
		}
		return $status;
	}
	
	public function updateSolr() {
		$d =& $this->data;
		$pathname = $d['page_pathname'];
		
		if ($d['page_indexed']) {
			$arSolrOptions = array('boost' => empty($d['page_options']['boost']) ? 1 : $d['page_options']['boost']);
			return \Solr::updateDocument(strtolower(SITE_CODE) . '-related-content', array(
				'item_uid' => "page_{$d['page_key']}", 
				'item_title' => $d['page_title'], 
				'item_description' => trim(implode("\n", array($d['page_content_title'], 	$d['page_content1'], $d['page_content2'], $d['page_content3']))), 
				'item_time_created' => TIME_NOW, 
				'item_tags' => 'cms_page',
				'item_id' => $d['page_id'],
				'item_canonical_path' => $pathname, 
				'item_section' => 'page'
			), $arSolrOptions);
		} else {
			return \Solr::deleteDocument(strtolower(SITE_CODE) . '-related-content', array('item_uid' => "c_{$d['page_key']}"));
		}
		
	}
	
	public static function cleanHtml($html, &$error = null) {
		$tidy = tidy_parse_string($html, self::$tidyConfig, 'utf8');
		if ($tidy->cleanRepair()) {
			return trim($tidy);
		} else {
			$error = 'Failed to clean input. The code is not valid HTML';
			return false;
		}
	}

	public static function initialize() {
		static::$db = \SiteManager::getDatabase();
	}
}
PageManager::initialize();