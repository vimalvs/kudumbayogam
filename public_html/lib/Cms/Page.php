<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Page Class
 * 
 * Class for cms page related actions
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class Page {
	protected $data = array('page_indexed' => 0, 'page_content_title' => '', 'page_content1' => '', 'page_content2' => '', 'page_content3' => '');
	
	private static function load($page_key) {

		$page = \Cache::load("page_{$page_key}", 'cms', 86400, function () use ($page_key) {
			$page = \SiteManager::getDatabase()->getRecord('cms_page', compact('page_key'));
			if ($page) {
				// Any updations here should be performed in PageManager::save too
				$page['page_options'] = unserialize($page['page_options']);
				return $page;
			}
			return false;
		}, false);
		return $page ? new static($page, false) : false;
	}
	
	public static function loadById($key) {
		return static::load($key);
	}
	
	public function __construct($data) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	public function __get($key){
		return isset($this->data['page_' . $key]) ? $this->data['page_' . $key] : null;
	}
	
	public function getData($key = null, $prefix = 'page_') {
		return is_null($key) ? $this->data : $this->data[$prefix . $key];
	}
	
	public function getParsedData($data) {
		$pageData = $this->data;
		$tmp = $data;
		array_multisort($tmp);
		$hash = md5(json_encode($tmp));
		return \Cache::load("page_{$this->data['page_key']}_parsed_$hash", 'cms', 86400, function () use ($data, $pageData) {
			$mustache = new \Mustache_Engine;
			$arField = array('page_title', 'page_meta_description', 'page_meta_keywords', 'page_content_title', 'page_content1', 'page_content2', 'page_content3');
			
			$changeKeyCase = function ($old) use (&$changeKeyCase) {
				$new = array();
				foreach ($old as $k => $v) {
					if (is_array($v)) {
						$new[strtoupper($k)] = $changeKeyCase($v);
					} else {
						$new[strtoupper($k)] = $v;
					}
				}
				return $new;
			};
			$data = $changeKeyCase($data);
			
			$parser = function(&$template) use ($mustache, $data, &$parser) {
				if (is_string($template)) {
					return $mustache->render($template, $data);
				} else if (is_array($template)) {
			    	return array_map($parser, $template);
			    } else {
			    	throw new \Exception("Unsupported template type");
			    }
			};
			$parsedData = array();
			foreach ($arField as $field) {
				$parsedData[$field] = $parser($pageData[$field]);
			}
			return $parsedData;
		});
	}
	
	public static function search($condition = null, $order = null, $limit = null, &$count = false) {
		$arItem = array();
		$pdbo = \SiteManager::getDatabase();
		if ($order && !is_array($order)) $order = array($order, 'ASC');
		if ($limit && !is_array($limit)) $limit = array(0, $limit);

		$whereSql = empty($condition) ? 1 : $pdbo->genQuery($condition);
		if ($count !== false) {
			$count = $pdbo->getCount('cms_page', $whereSql);
		}
		$stmt = $pdbo->search('cms_page', $whereSql, $order, $limit);
		$arItem = $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : false;
		return $arItem;
	}
	

}
