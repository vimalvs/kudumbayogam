<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * ArticleManager Class
 * 
 * Class for managing cms articles
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class ArticleManager extends Article {
	
	private $updateData = array();
	private static $tmpDir = NULL;
	private static $tpl = NULL;
	
	public function __construct($data = NULL) {
		if ($data) {
			if (isset($data['article_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}
	
	public function getUpdateData() {
		return $this->updateData + $this->data + array('article_id' => 0);
	}

	public function delete(&$error = NULL) {
		$status = $this->removeTemplate() && static::$db->deleteRecord('cms_article', array('article_id' => $this->data['article_id']));
		if ($status) {
			\Logger::log("Deleted CMS Article : " . $this->data['article_title'], 'delete', 'cms_article', $this->data['article_id']);
		}
		return $status;
	}
	
	public function update($data, &$arError = NULL) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$errorInfo = NULL) {
		$isNew = empty($this->data['article_id']);
		$pdbo = \SiteManager::getDatabase();
		
		// Unset id from update requests
		unset($this->updateData['article_id']);
		
		if (empty($this->updateData)) return TRUE;
		
		extract($this->data);
		extract($this->updateData, EXTR_REFS);

		if (is_null($errorInfo)) $errorInfo = array();

		$article_pathname = trim($article_pathname, '-');
		$path_orig = preg_replace('#/+#', '/', $article_pathname);
		$this->updateData['article_require_trailing_slash'] = (substr($article_pathname, -1) === '/');
		$this->updateData['article_pathname'] = rtrim($path_orig, '/');
		if (empty($this->updateData['article_pathname'])) $this->updateData['article_pathname'] = NULL;
		$article_allow_comments = (bool)$article_allow_comments;
		
		if ($article_status == Article::STATUS_PUBLISHED) {
			$ext = pathinfo($article_pathname, PATHINFO_EXTENSION);
			if (empty($article_title)) $errorInfo['article_title'] = 'Title is mandatory';
			if (empty($article_pathname)) $errorInfo['article_pathname'] = 'Pathname cannot be empty';
			elseif ($article_pathname[0] !== '/') $errorInfo['article_pathname'] = 'Path should begin with /';
			elseif (!in_array($ext, array('', 'htm', 'html', 'php'))) $errorInfo['article_pathname'] = "$ext is not a valid extension. [.htm, .html, .php]";
			elseif ($pdbo->getRecord('cms_article', $isNew ? compact('article_pathname') : "article_pathname = {$pdbo->quote($article_pathname)} AND article_id != $article_id")) {
				$errorInfo['article_pathname'] = 'The pathname already exists';
			}
			if (empty($article_content)) $errorInfo['article_content'] = 'Article cannot be empty';
			if (empty($article_tags)) $errorInfo['article_tags'] = 'Please enter related tags';
			if (empty($article_page_title)) $article_page_title = $article_title;
		}
		$config = array('clean' => 'yes', "indent" => TRUE, 'output-html' => TRUE, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => TRUE);
		$tidy = tidy_parse_string($article_content, $config, 'utf8');
		if ($tidy->cleanRepair()) {
			$article_content = (string)$tidy;
		} else {
			$tpl->addMessage('Failed to clean input. The code is not valid HTML');
		}
		
		$article_alt_title = array_filter(array_map('trim', $article_alt_title));
		$article_options = empty($article_options) ? array() : array_filter(array_map('trim', $article_options));

		$serialized_alt_title = empty($article_alt_title) ? 'a:0:{}' : serialize($article_alt_title);
		$serialized_related_links = empty($article_related_links) ? 'a:0:{}' : serialize($article_related_links);
		$serialized_options = empty($article_options) ? 'a:0:{}' : serialize($article_options);

		if (($tmp = strlen($serialized_related_links)) > 500) {
			$errorInfo['article_related_links'] = "Related links size exceeds maximum permitted length by " . ($tmp - 500) . ' characters';
		}
		if (($tmp = strlen($serialized_options)) > 1500) {
			$errorInfo['article_options'] = "Options size exceeds maximum permitted length by " . ($tmp - 1500) . ' characters';
		}
		if (strlen($serialized_alt_title) > 800) {
			$errorInfo['article_alt_title'] = 'Combined length of alternate title exceeds permitted maximum. Shorten the titles.';
		}

		$status = empty($errorInfo);
		if ($status) {
			/**
			 * Data Validated successfully
			 */
			$this->updateData['article_is_published'] = empty($article_status) ? NULL : 1;

			$article_created = TIME_NOW;
			// $arArticleData = compact('title', 'path', 'require_trailing_slash', 'tags', 'content', 'summary', 'page_title', 'meta_keywords', 'meta_description', 'section', 'alt_title', 'related_links', 'options', 'allow_comments', 'status', 'time');
			
			if (!empty($article_pathname)) {
				// if ($this->updateData['article_require_trailing_slash']) $tplFileCache .= '/index.php';
			} else {
				$article_pathname = NULL;
			}
			
			$extra = array();
			$extra['article_alt_title'] = $serialized_alt_title;
			$extra['article_related_links'] = $serialized_related_links;
			$extra['article_options'] = $serialized_options;

			// If article is published generate cache and perform syntax check
			if ($article_status == Article::STATUS_PUBLISHED) {
				TemplateGenerator::setTempDir(CMS_CACHE_PATH . '/tmp');
				// Generate temporary template file for syntax checking
				$generator = new TemplateGenerator($this, TRUE);
				if (!$generator->validate(TRUE, $output)) {
					$error = end($output);
					$errorInfo = "PHP code validation failed. Please ensure that the page contains no error.<br><pre>$error</pre>";
					return FALSE;
				}
				$generator->destroy();
			}
			if ($status) {
				// $tplFileCache = NULL;
				if ($isNew) {
					$status = $pdbo->insertRecord('cms_article', $extra + $this->updateData);
					if ($status) {
						$article_id = static::$db->lastInsertId();
						\Logger::log("Added New CMS Article : $article_title", 'add' , 'cms_article', $article_id);
						$this->data['article_id'] = $article_id;
					}
				} else {
					$status = $pdbo->updateRecord('cms_article', array('article_id' => $article_id), $extra + $this->updateData);
					// Log event only if the entry was modified
					if ($status !== FALSE) {
						\Logger::log("Updated CMS Article : $article_title", 'update' , 'cms_article', $article_id);
					}
					// Return success even if data was not modified
					$status = ($status !== FALSE);
				}
				if ($status) {
					// Remove Cached Template
					if (($this->data['article_pathname'] !== $article_pathname) || (Article::STATUS_DRAFT == $article_status)) {
						if (!$this->removeTemplate()) {
							$errorInfo[] = 'Failed to delete the cached template file. The content is still available to public.';
						}
					}
					
					// Update Solr on successful DB Update
					$this->data = array_merge($this->data, $this->updateData);
					if ($article_status == Article::STATUS_PUBLISHED) {
						// Generate and cache the final template
						foreach (['Classic', 'Mobile', 'Basic'] as $themeName) {
							$theme = new \Theme($themeName, new \PHPTemplate);
							$generator = new TemplateGenerator($this, false, $theme);
							$status = $status && $generator->save();
							if (!$status) break;
						}
						// $generator = new TemplateGenerator($this);
						if ($status) {
							if (!$this->updateSolr()) {
								$errorInfo[] = 'Failed to update solr.';
							}
						} else {
							$errorInfo[] = "Failed to generate template for '$themeName' theme";
							if ($output) $errorInfo[] = $output;
						}
					} elseif (!$isNew) {
						if (($this->data['article_status'] != $article_status) && !$this->updateSolr()) {
							$errorInfo[] = 'Failed to delete the document from solr. It might still show up in related search.';
						}
					}
				}
			}
		}
		return $status;
	}
	
	public function updateSolr() {
		extract($this->data);
		$pathname = $article_pathname . (empty($article_require_trailing_slash) ? '' : '/');
		
		if ($article_status) {
			$arSolrOptions = array('boost' => empty($article_options['boost']) ? 1 : $article_options['boost']);
			$arSolrData = [
				'item_uid' => "article_$article_id",
				'item_canonical_path' => $pathname,
				'item_section' => 'article',
				'item_title' => $article_title,
				'item_description' => $article_content,
				'item_time_created' => $article_created,
				'item_id' => $article_id,
				'item_tags' => $article_tags,
				'article_id' => $article_id,
				'article_title' => $article_title,
				'article_pathname' => $article_pathname,
				'article_require_trailing_slash' => $article_require_trailing_slash,
				'multi_alt_title' => $article_alt_title
			];
			return \Solr::updateDocument(strtolower(SITE_CODE) . '-related-content', $arSolrData, $arSolrOptions);
		} else {
			return \Solr::deleteDocument(strtolower(SITE_CODE) . '-related-content', array('item_uid' => "article_{$article_id}"));
		}
		
	}
	
	public function removeTemplate() {
		if (!empty($this->data['article_pathname'])) {
			$tplFileCache = $this->data['article_pathname'] . (empty($this->data['article_require_trailing_slash']) ? '' : '/index.php');
			return (!file_exists(CMS_CACHE_PATH . $tplFileCache) || unlink(CMS_CACHE_PATH . $tplFileCache)) && 
				   (!file_exists(CMS_CACHE_PATH . '/m' . $tplFileCache) || unlink(CMS_CACHE_PATH . '/m' . $tplFileCache));
		} else {
			return TRUE;
		}
	}
	
	public function generateTemplate($dryRun = FALSE, &$output = NULL) {

		static::$tpl = $tpl = new \PHPTemplate();
		$cache = \SiteManager::getCache();
		$tpl->setTemplatePath(ROOT . '/res/cms/templates/' . (\SiteManager::isMobileEdition() ? '/m' : ''));
		$tpl->setTemplate('cms-article');

		$data = $dryRun ? ($this->updateData + $this->data + array('article_id' => 0)) : $this->data;
		$data['article_content_parsed'] = Cms::parse($this->data, $data['article_content']);
		$tpl->addData($data);

		// Section Info
		$sectionInfo = \SiteManager::getConfig("cms_section_{$data['article_section']}");
		$tpl->addData($sectionInfo);

		$tpl->data['section_menu'] = Block::get($sectionInfo['menu'], '');

		if (!$sectionInfo['wide_layout']) {
			$tpl->data['midcontent'] = Cms::parse($this->data, Block::get($sectionInfo['midcontent']));
		}
		
		$code = $tpl->render('cms-article', NULL, TRUE);

		$tpl = NULL;
		if ($dryRun || ($status = self::checkSyntax($code, TRUE, $output))) {
			$tplFileCache = (CMS_CACHE_PATH . $data['article_pathname'] . (empty($data['article_require_trailing_slash']) ? '' : '/index.php'));
			$tplPath = substr($tplFileCache, 0, strrpos($tplFileCache, '/'));
			if (!file_exists($tplPath)) {
				mkdir($tplPath, 0777, TRUE);
			}
			return $dryRun ? (!file_exists($tplFileCache) || unlink($tplFileCache)) : (file_put_contents($tplFileCache, $code, LOCK_EX) !== FALSE);
		} else {
			error_log("[Cms\ArticleManager] PHP " . end($output));
		}
		return FALSE;
	}
	
	public static function setTempDir($path) {
		self::$tmpDir = rtrim($path, '/');
	}
	
	public static function getTemplate() {
		return static::$tpl;
	}
	
	public static function checkSyntax($code, $execute = FALSE, &$output = NULL) {
		$tmpDir = is_null(self::$tmpDir) ? sys_get_temp_dir() : self::$tmpDir;
		$tmpFile = $tmpDir . '/' . md5($code) . '.tmp.php';
		if (file_put_contents($tmpFile, $code, LOCK_EX)) {
			$return = FALSE;
			exec("php -l $tmpFile", $output, $rval);
			$a = (strpos($output[0], 'No syntax errors detected') !== FALSE);
			if ($rval && (strpos($output[0], 'No syntax errors detected') !== FALSE)) {
				$rval = 0;
				$output = '';
			}
			// var_dump($return);
			// exit;
			if (!$rval && $execute) {
				exec("php -f $tmpFile", $output, $rval);
				if (!$rval) {
					$output = NULL;
				} elseif (empty($output)) {
					$output = array('Failed to execute php file.');
				}
			} elseif (empty($output)) {
				$output = array("Syntax check failed");
			}
			unlink($tmpFile);
			return ($rval === 0);
		} else {
			error_log("[Cms\ArticleManager] Error : Failed to create temporary file $tmpFile for syntax checking.");
			return 0;
		}
	}
}