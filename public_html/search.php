<?php
include 'include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

extractCleanVars('q');

$pdbo = \SiteManager::getDataBase();

$arPageData= $pdbo->search("page_content", "page_content_en LIKE '%".$q."%'")->fetchAll();

$arSearchData = [];

foreach ($arPageData as $pageData){
	$arSearchData[] = ['title' => $pageData['page_title'], 'pathname' => $pageData['page_pathname'], 'description' => $pageData['page_content_en']];
}

$arEventData= $pdbo->search("event", "event_description LIKE '%".$q."%'")->fetchAll();
foreach($arEventData as $eventData){
	$arSearchData[] = ['title' => $eventData['event_title'], 'pathname' => 'events/'.$eventData['event_pathname'], 'description' => $eventData['event_description']];
}


$tpl->addData(compact('q', 'arSearchData'));

$tpl->setTemplate('search');

$tpl->generate();
