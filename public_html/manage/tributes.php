<?php
include 'prepend.php';
$mode = getCleanVar('mode', 'dashboard');

$tpl->setTemplate("tributes");

$mode = getCleanVar('mode', 'tributes');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();


if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/movies/tributes.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($mode)) $mode = 'tributes';
if (empty($action)) $action = 'list';

if ($mode === 'tributes') {
	if ($action === 'list') {
		$stmt = $pdbo->search("tribute");
		$arPage = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arPage'));
	} elseif($action === 'add') {
		$tpl_view = 'form';
		if($p){
			extractVars('page_pathname', 'page_title', 'page_content_title', 'page_description', 'page_content');
			$page_key = makeSafeName(str_ireplace(".html", "", $page_pathname));
			$page_pathname = "/tributes/".makeSafeName($page_pathname).".html";
			$tribute = new \Tributes\TributeManager(compact('page_key', 'page_pathname', 'page_title', 'page_content_title', 'page_description', 'page_content'));
			if ($tribute->save()) {
				$tpl->showMessage('Inserted', 'Successfully saved',['back' => $redirect, 'visit' => SITE_HOME.$page_pathname], 'success');
			} else {
				$tpl->showMessage("error", "Not Inserted");
			}
		}
	}
} elseif ($mode === 'tribute') {
	$id = getCleanVar('id');
	$tpl_view = 'form';
	if ($action === 'update') {
		$page = \Tributes\TributeManager::loadById($id);
		if (!$p) {
			$page = \Tributes\TributeManager::loadById($id);
		} else {
			extractVars('page_pathname', 'page_title', 'page_content_title', 'page_description', 'page_content');

			$page_key = makeSafeName(str_ireplace(".html", "", $page_pathname));
			$page_pathname = str_ireplace("/tributes/", "", $page_pathname);
			$page_pathname = str_ireplace(".html", "", $page_pathname);
			$page_pathname = "/tributes/".makeSafeName($page_pathname).".html";

			$arData = compact('page_pathname', 'page_title', 'page_content_title', 'page_description', 'page_content');
			// $page = new \Page\PageManager($arData, false);
			$status = $page->update($arData, $arError);
			if ($status) {
				$tpl->showMessage('Updated', 'Successfully Updated',['back' => $redirect, 'edit' => $redirect, 'visit' => SITE_HOME.$page->getData()['page_pathname']], 'success');
			} else {
				$tpl->showMessage("error", "Not Updated");
			}
		}
		$pageData = $page->getData();
		$tpl->addData($pageData+compact('id'));
	} elseif ($action === 'remove') {
		$tpl->data['breadcrumb']['Remove'] = '/manage/tributes.php?mode=tribute&action=remove&id='. $id;
		if (!$p) {
			$tpl->addQuestion('Delete tribute page', "Are you sure that you want to delete this tribute page", array(array('Cancel', JS_BACK), array('Delete', "/manage/tributes.php?mode=tribute&action=remove&id=$id&p=1&redirect=" . urlencode($redirect))));
		} else {
			if ($pdbo->deleteRecord("tribute", ['id' => $id])) {
				$tpl->showMessage('Success', "Tribute page deleted successfully.", $redirect, 'success');
			} else {
				$tpl->showMessage('Error', "Failed to remove tribute page from database.<br/>{$pdbo->getErrorReport()}", JS_BACK);
			}
		}
	}
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();