<?php
include 'prepend.php';
$tpl->setTemplate("gallery");

$mode = getCleanVar('mode', 'gallery_categories');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();

if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/manage/header-images.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($action)) $action = 'list';

if ($mode === 'gallery_categories') {
	if ($action === 'list') {
		$stmt = $pdbo->search("gallery_categories", null, ['position', 'ASC']);
		$arCategory = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arCategory'));

	} elseif($action === 'add') {
		extractCleanVars('gallery_category_name', 'gallery_description', 'p');
		$tpl_view = 'form';
		if ($p) {
			$stmt = $pdbo->query("SELECT * FROM gallery_categories h ORDER BY h.id DESC LIMIT 1");
			$lastInsertRecord = $stmt ? $stmt-> fetch() : [];

			if ($lastInsertRecord) {
				$position = $lastInsertRecord['position'] + 1;
			} else {
				$position = 1;
			}

			if ($pdbo->insertRecord("gallery_categories", ['category' => trim($gallery_category_name), 'category_pathname' => makeSafeName(trim($gallery_category_name)), 'description' => trim($gallery_description), 'position' => $position])){
		   		echo json_encode(array('status' => 1, 'category_id' => $pdbo->getLastInsertID()));   
				exit;
			} else {
		   		echo json_encode(array('status' => 0));   
				exit;
			}
		}
	} else if ($action === 'update-category-order') {
		extractVars('order');
		foreach ($order as $key => $value) {
			$pdbo->updateRecord("gallery_categories", ['id' => $value['id']], ['position' => $value['index']]);
		}
		echo 1;
		exit;
	}

} elseif ($mode === 'gallery_category') {

	extractCleanVars('id');
	
	$stmt = $pdbo->search("gallery_categories", ['id' => $id]);
	$galleryCategory = $stmt ? $stmt->fetch() : null;
	
	if (!$galleryCategory) {
		$tpl->showMessage('Invalid Option', 'Selected gallery category is not found in our database', ['Back' => $redirect]);
	} else {

		if ($action === 'update') {
			extractCleanVars('gallery_category_name', 'gallery_description');
			$tpl_view = "form";
			if ($p) {
				$pdbo->updateRecord("gallery_categories", ['id' => $id], ['category' => $gallery_category_name,  'category_pathname' => makeSafeName($gallery_category_name), 'description' => trim($gallery_description)]);
		   			echo json_encode(array('status' => 1, 'category_id' => $id));   
		    	exit;
			}
			$stmt = $pdbo->search("gallery_images", ['gid' => $galleryCategory['id']], ['image_order', 'ASC']);
			$galleryImages = $stmt ? $stmt->fetchAll() : null;
			$tpl->data['galleryImages'] = $galleryImages;
			$gallery_category_name = $galleryCategory['category'];
			$gallery_description = $galleryCategory['description'];
			$tpl->addData(compact('tpl_view', 'gallery_category_name', 'gallery_description')+$galleryCategory);
		} else if ($action === 'update-image') {
			extractCleanVars('image_title', 'image_description', 'image_id');
			if ($p) {
				$pdbo->updateRecord("gallery_images", ['id' => $image_id], ['caption' => trim($image_title), 'description' => trim($image_description)]);
		   			echo json_encode(array('status' => 1, 'category_id' => $id));   
		    	exit;
			}
	    	exit;
		} else if ($action === 'get-image-details') {
			extractCleanVars('image_id');

			$stmt = $pdbo->search("gallery_images", ['id' => $image_id, 'gid' => $id]);
			$imageDetails = $stmt ? $stmt->fetch() : null;
			echo json_encode($imageDetails);
			exit;
		} else if ($action === 'remove') {
			
			$tpl->data['breadcrumb']['Remove'] = '/manage/gallery.php?mode=gallery_category&action=remove&id='. $id;
			if (!$p) {
				$tpl->addQuestion('Delete Gallery Category', "Are you sure that you want to delete this Gallery Category", array(array('Cancel', JS_BACK), array('Delete', "/manage/gallery.php?mode=gallery_category&action=remove&id=$id&p=1&redirect=" . urlencode($redirect))));
			} else {
				if ($pdbo->deleteRecord("gallery_categories", ['id' => $id])) {
					$tpl->showMessage('Success', "Gallery Category deleted successfully.", $redirect, 'success');
				} else {
					$tpl->showMessage('Error', "Failed to remove Gallery Category from database.<br/>{$pdbo->getErrorReport()}", JS_BACK);
				}
			}
		}  elseif($action === 'upload') {
			$stmt = $pdbo->query("SELECT * FROM gallery_images h WHERE h.gid = $id ORDER BY h.id DESC LIMIT 1");
			$lastInsertRecord = $stmt ? $stmt-> fetch() : [];

			if ($lastInsertRecord) {
				$image_order = $lastInsertRecord['image_order'] + 1;
			} else {
				$image_order = 1;
			}
			$image_count = $galleryCategory['image_count'] + 1;
			$ds          = DIRECTORY_SEPARATOR;  //1
			$storeFolder = ROOT . "/gallery/assets/images/{$galleryCategory['category_pathname']}/";   //2
			$thumb_path = ROOT . "/gallery/assets/imgs/{$galleryCategory['category_pathname']}/";   //2
			if (!file_exists($storeFolder))
				mkdir($storeFolder);
			if (!file_exists($thumb_path))
				mkdir($thumb_path);

			$uploader = new \Uploader\ImageUpload('uploadfile');
			// Handle the upload
			$fileName = $storeFolder.$_FILES['uploadfile']['name'];
			$image_path = "/gallery/assets/images/{$galleryCategory['category_pathname']}/".$_FILES['uploadfile']['name'];
			$thumbnail = "/gallery/assets/imgs/{$galleryCategory['category_pathname']}/".$_FILES['uploadfile']['name'];
			$ext = pathinfo($fileName, PATHINFO_EXTENSION);
			
			$result = $uploader->handleUpload($storeFolder);
			$arThumbSize = getProportionateImgSize($_FILES['uploadfile']['name']);
			
			// $uploader->generateThumbnail($fileName, $ext, $thumb_path, $_FILES['uploadfile']['name'], 200, 200);
			$uploader->generateThumbnail($fileName, $ext, $thumb_path, $_FILES['uploadfile']['name'], $arThumbSize[0], $arThumbSize[1]);

			if (!$result) {
			    echo json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg()));   
			} else {
				if (!$pdbo->getRecord("gallery_images", ['image_pathname' => $image_path, 'gid' => $id])) {
					if ($pdbo->insertRecord("gallery_images", ['image_pathname' => $image_path, 'thumbnail' => $thumbnail, 'gid' => $id,  'image_order' => $image_order, 'created_time' => TIME_NOW])) {
						$cStmt = $pdbo->query("SELECT COUNT(*) FROM gallery_images g WHERE g.gid = $id");
						$image_count = $cStmt ? $cStmt->fetchColumn(0) : 1;
						if ($pdbo->updateRecord("gallery_categories", ['id' => $id], ['image_count' => $image_count])) {
				    		echo json_encode(array('success' => true, 'file' => $uploader->getFileName()));
				    	}
					} else {
				    	echo json_encode(array('success' => false, 'msg' => "Database Error"));   
					}
				} else {
			    	echo json_encode(array('success' => false, 'msg' => "Image already exists "));   
				}
			}
			exit;
		} elseif($action === 'update-order') {
			extractVars('order');
			foreach ($order as $key => $value) {
				$pdbo->updateRecord("gallery_images", ['id' => $value['id']], ['image_order' => $value['index']]);
			}
			echo 1;
			exit;
		} elseif($action === 'remove-image') {
			extractVars('image_id');
			if ($pdbo->deleteRecord("gallery_images", ['id' => $image_id, 'gid' => $id])) {
	   			echo 1;   
			} else {
				echo 0;
			}
			exit;
		} else {
		
		}
	} 
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();