<?php
include 'prepend.php';

$mode = getCleanVar('mode', 'events');

$tpl->setTemplate("news");

extractCleanVars('action', 'p', 'redirect', 'id');

use News\News;

$pdbo = \SiteManager::getDataBase();

if ($mode === 'events') {
	if (empty($action)) $action = 'list';
	if($action == 'list'){
		$tpl_view = 'list-news';
		$arEvents = \News\News::search(null, ['event_id', 'DESC']);
	}elseif($action == 'add'){
		$tpl_view = 'news-form';
		if($p){
			extractCleanVars('event_title','event_description','event_date');
			$file = $_FILES['event_image'];
            $image_pathname = makeSafeName($event_title);
            $arImageType = ['image/jpeg' => '.jpg'];
            $tempFile = $file['tmp_name'];
            set_time_limit(0);
            $arInfo = getimagesize($tempFile);
            $origname = $file['name'];
            $lastInsertedTag =  \News\News::search(null, ['event_id', 'DESC'], 1);
            $last_inserted_image_id = $lastInsertedTag[0]['event_id'];
            $image_id = $last_inserted_image_id+1;
            if (isset($arImageType[$arInfo['mime']])) {
            	$ext = $arImageType[$arInfo['mime']];
                $filename = $image_pathname . '-' . $image_id . $ext;
                $file = "/assets/images/". $filename;
                if(move_uploaded_file($tempFile, $file)){
                	$arData = compact('event_title', 'event_description', 'event_date');
                	$arData['event_image_pathname'] = $filename;
					$news = new News\NewsManager($arData);
					$status = $news->save();
                	if ($status) {
						$tpl->showMessage('Added', 'Successfully Added',['back' => $redirect], 'success');
					} else {
						$tpl->addMessages($errorInfo);
						$tpl->addData(compact('event_title', 'event_description', 'event_date'));
					}
           		}
			}
		}
	}
} elseif($mode=='event'){
	$event = \News\NewsManager::loadById($id);
	if($event){
		$tpl_view = 'news-form';	
		if($action == 'update'){	
			$eventDetails = $event->getData();
			extract($eventDetails);
			$tpl->addData(compact('event_id', 'event_title', 'event_description', 'event_date','event_image_pathname'));
			if($p){
				extractCleanVars('event_title','event_description','event_date');
				$data = compact('event_title','event_description','event_date');
				$status = $event->update($data, $errorInfo);
				 if ($status) {
	                $tpl->showMessage('Success', 'The Event Details Details Updated Succesfully', [['View List', '/manage/news.php?&mode=events'], ['Back', JS_BACK]], 'success');
	            } else {
	                $tpl->addMessages($errorInfo);
	            }
			}
		}elseif($action == 'remove'){
			
			if ($p) {
                $status = $event->delete($arError);
                if ($status) {
                    $tpl->showMessage('Success', 'Data Deleted from Database', '/manage/news.php?&mode=events', 'success');
                } else {
                    $tpl->showMessage('Error', 'Failed to delete this data.', JS_BACK);
                }
            } else {
                $tpl->addQuestion('Delete Data', 'Are you sure you want to continue. This data will be permanently removed from database', [['Continue', "/manage/news.php?mode=event&action=remove&id=$id&p=1"], ['Cancel', JS_BACK]]);
            }
		}
	}else{
		 $tpl->showMessage('error', 'Event not found in your database',['back' => '/manage/news.php?&mode=events']);
	}
	
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect','arEvents'));

$tpl->generate();