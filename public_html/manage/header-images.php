<?php
include 'prepend.php';
$tpl->setTemplate("header-images");

$mode = getCleanVar('mode', 'images');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();

if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/movies/header-images.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($action)) $action = 'list';

if ($mode === 'images') {
	if ($action === 'list') {
		$stmt = $pdbo->search("header_images", null, ['image_order', 'ASC']);
		$arImage = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arImage'));
	} elseif($action === 'add') {
		$tpl_view = 'form';

	} elseif($action === 'upload') {
		$stmt = $pdbo->query("SELECT * FROM header_images h ORDER BY h.id DESC LIMIT 1");
		$lastInsertRecord = $stmt ? $stmt-> fetch() : [];

		if ($lastInsertRecord) {
			$image_order = $lastInsertRecord['image_order'] + 1;
		} else {
			$image_order = 1;
		}
		$ds          = DIRECTORY_SEPARATOR;  //1
		$storeFolder = ROOT . '/assets/images/';   //2
		$thumb_path = ROOT . '/assets/imgs/';   //2

		$uploader = new \Uploader\ImageUpload('uploadfile');
		// Handle the upload
		$fileName = $storeFolder.$_FILES['uploadfile']['name'];
		$image_path = '/assets/images/'.$_FILES['uploadfile']['name'];
		$thumbnail = '/assets/imgs/'.$_FILES['uploadfile']['name'];
		$ext = pathinfo($fileName, PATHINFO_EXTENSION);
		
		$result = $uploader->handleUpload($storeFolder);
		$uploader->generateThumbnail($fileName, $ext, $thumb_path, $_FILES['uploadfile']['name'], 200,200);
		if (!$result) {
		    echo json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg()));   
		} else {
			$pdbo->insertRecord("header_images", ['image_path' => $image_path, 'thumbnail' => $thumbnail, 'image_order' => $image_order, 'created_time' => TIME_NOW]);
		    echo json_encode(array('success' => true, 'file' => $uploader->getFileName()));
		}
		exit;
	} elseif($action === 'update-order') {
		extractVars('order');
		foreach ($order as $key => $value) {
			$pdbo->updateRecord("header_images", ['id' => $value['id']], ['image_order' => $value['index']]);
		}
		echo 1;
		exit;
	}
} elseif ($mode === 'image') {
	extractVars('image_id');
	if ($action === 'remove') {
		$status = $pdbo->exec("DELETE FROM header_images WHERE ID = $image_id");
		if ($status) {
			echo 1;
		} else {
			echo 0;
		}
		exit;
	}
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();