<?php
include 'prepend.php';
$mode = getCleanVar('mode', 'dashboard');

$tpl->setTemplate("page");

$mode = getCleanVar('mode', 'pages');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();


if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/movies/manage.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($mode)) $mode = 'pages';
if (empty($action)) $action = 'list';

if ($mode === 'pages') {
	if ($action === 'list') {
		$stmt = $pdbo->search("page_content");
		$arPage = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arPage'));
	}
} elseif ($mode === 'page') {
	$id = getCleanVar('id');
	$tpl_view = 'form';
	if ($action === 'update') {
		$page = \Page\PageManager::loadById($id);
		if (!$p) {
			$page = \Page\PageManager::loadById($id);
		} else {
			extractVars('page_content_ml', 'page_content_en');
			$arData = compact('page_content_en', 'page_content_ml');
			// $page = new \Page\PageManager($arData, false);
			$status = $page->update($arData, $arError);
			if ($status) {
				$tpl->showMessage('Updated', 'Successfully Updated',['back' => $redirect, 'edit' => $redirect, 'visit' => SITE_HOME.$page->getData()['page_pathname']], 'success');
			} else {
				$tpl->showMessage("error", "Not Updated");
			}
		}
		$pageData = $page->getData();
		$tpl->addData($pageData+compact('id'));
	}
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();