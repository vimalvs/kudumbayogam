<?php
include 'prepend.php';
$tpl->setTemplate("members");

$mode = getCleanVar('mode', 'members');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();

if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/manage/members.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($action)) $action = 'list';

if ($mode === 'members') {
	if ($action === 'list') {
		$familyList = $pdbo->search("family")->fetchAll();
		$arFamily = [];
		foreach ($familyList as $family) {
			$arFamily[$family['id']] = $family['family_name'];
		}
		$stmt = $pdbo->search("members", null, ['id', 'DESC']);
		$arMember = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arFamily'));
		$tpl->addData(compact('arMember'));

	} elseif($action === 'get-family-members') {

		//code for getting family members for a particular family id
		exit;
	} elseif($action === 'add') {
		$arFamily = $pdbo->search("family")->fetchAll();
		extractVars('member_name','family_id', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio', 'p', 'parent_id','spouse_details', 'spouse_dob', 'spouse_dod');
		$tpl_view = 'form';
		if ($p) {
			$member_pathname = makeSafeName($member_name);
			$file = $_FILES['photo'];
			$file2 = $_FILES['spouse_photo'];
			$stmt = $pdbo->query("SELECT * FROM members h WHERE h.family_id = $family_id ORDER BY h.member_uid DESC LIMIT 1");
			$lastFamilymember = $stmt ? $stmt->fetch() : [];
			if ($lastFamilymember) {
				$member_uid = $lastFamilymember['member_uid'] + 1;
			} else {
				$member_uid = 1;
			}
			$stmt = $pdbo->query("SELECT * FROM members h ORDER BY h.id DESC LIMIT 1");
			$lastInsertRecord = $stmt ? $stmt->fetch() : [];
			if ($lastInsertRecord) {
				$number = $lastInsertRecord['id'] + 1;
			} else {
				$number = 1;
			}

			if (empty($arFamily)) {
				$family_id = 0;
			}
			if(empty($parent_id)){
				$parent_id = 0;
			}
			if(empty($family_id)) $tpl->addMessage('The family_name cannot be empty');
			if($tpl->hasError()){		
				$tpl->addData(compact('member_name','member_pathname', 'family_id', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','description', 'bio', 'p', 'parent_id','spouse_details', 'spouse_dob', 'spouse_dod'));
			}else{
			
				if(!empty($file['name'])){
					$ds = DIRECTORY_SEPARATOR;  //1
					$storeFolder = ROOT . "/members/assets/images/$family_id/";   //2
					$thumb_path = ROOT . "/members/assets/imgs/$family_id/";   //2
					if (!file_exists($storeFolder)) {
						mkdir($storeFolder);
					}
					if (!file_exists($thumb_path)){
						mkdir($thumb_path);
					}

					$uploader = new \Uploader\ImageUpload('photo');

					// Handle the upload
					$fileName = $storeFolder.$_FILES['photo']['name'];
					
					$thumbnail = "/members/assets/images/$family_id/".$_FILES['photo']['name'];
					$ext = pathinfo($fileName, PATHINFO_EXTENSION);
					$uploader->newFileName = $image_pathname = "{$family_id}-{$number}.".$ext;
					
					$result = $uploader->handleUpload($storeFolder);
					$arThumbSize = getProportionateImgSize($_FILES['photo']['name']);
			
					$uploader->generateThumbnail($storeFolder.$image_pathname, $ext, $thumb_path, $image_pathname, $arThumbSize[0], $arThumbSize[1]);
				}
				if(!empty($file2['name'])){
					$ds = DIRECTORY_SEPARATOR;  //1
					$storeFolder = ROOT . "/members/assets/images/$family_id/";   //2
					$thumb_path = ROOT . "/members/assets/imgs/$family_id/";   //2
					if (!file_exists($storeFolder)) {
						mkdir($storeFolder);
					}
					if (!file_exists($thumb_path)){
						mkdir($thumb_path);
					}

					$uploader = new \Uploader\ImageUpload('spouse_photo');

					// Handle the upload
					$fileName = $storeFolder.$_FILES['spouse_photo']['name'];
					
					$thumbnail = "/members/assets/images/$family_id/".$_FILES['photo']['name'];
					$ext = pathinfo($fileName, PATHINFO_EXTENSION);
					$uploader->newFileName = $spouse_image_pathname = "spouse-{$family_id}-{$number}.".$ext;
					$result = $uploader->handleUpload($storeFolder);
					$arThumbSize = getProportionateImgSize($_FILES['spouse_photo']['name']);
			
					$uploader->generateThumbnail($storeFolder.$spouse_image_pathname, $ext, $thumb_path, $spouse_image_pathname, $arThumbSize[0], $arThumbSize[1]);
				}

				$arData = compact('member_name', 'member_pathname', 'spouse_image_pathname', 'family_id', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio', 'parent_id','member_uid','image_pathname','spouse_details', 'spouse_dob', 'spouse_dod');
			
				$status = $pdbo->insertRecord('members', $arData);
				if ($status) {
					$tpl->showMessage('Added', 'Successfully Added',['back' => $redirect], 'success');
				} else {
					$tpl->addMessages($errorInfo);
					$tpl->addData(compact('member_name','family_id', 'member_pathname', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio', 'p', 'parent_id','spouse_details', 'spouse_dob', 'spouse_dod'));
				}
			}
		}
		$tpl->addData(compact('tpl_view', 'arFamily'));

	} else if ($action === 'update-category-order') {
		extractVars('order');
		foreach ($order as $key => $value) {
			$pdbo->updateRecord("family", ['id' => $value['id']], ['position' => $value['index']]);
		}
		echo 1;
		exit;
	}

} elseif ($mode === 'member') {

	extractCleanVars('id');
	$stmt = $pdbo->search("members", ['id' => $id]);
	$member = $stmt ? $stmt->fetch() : null;
	
	if (!$member) {
		$tpl->showMessage('Invalid Option', 'Selected member is not found in our database', ['Back' => $redirect]);
	} else {
		if ($action === 'update-member-image') {
			extractCleanVars('family_id');
			$file = $_FILES['memberImage'];

			if(!empty($file['name'])){
				$ds = DIRECTORY_SEPARATOR;  //1
				$storeFolder = ROOT . "/members/assets/images/$family_id/";   //2
				$thumb_path = ROOT . "/members/assets/imgs/$family_id/";   //2
				if (!file_exists($storeFolder)) {
					mkdir($storeFolder);
				}
				if (!file_exists($thumb_path)){
					mkdir($thumb_path);
				}

				$uploader = new \Uploader\ImageUpload('memberImage');
				// Handle the upload
				$fileName = $storeFolder.$_FILES['memberImage']['name'];
				
				$thumbnail = "/members/assets/images/$family_id/".$_FILES['memberImage']['name'];
				$ext = pathinfo($fileName, PATHINFO_EXTENSION);
				$uploader->newFileName = $image_pathname = "{$family_id}-{$id}.".$ext;
				
				$result = $uploader->handleUpload($storeFolder);
				$uploader->generateThumbnail($storeFolder.$image_pathname, $ext, $thumb_path, $image_pathname, 200,200);
				if ($result) {
					$pdbo->updateRecord("members", ['id' => $id], ["image_pathname" => $image_pathname]);
				}
				echo $result ? 1 : 0;
			} else {
				echo 0;
			}
			exit;
		}elseif ($action === 'change-spouse-image') {
			extractCleanVars('family_id');
			$file = $_FILES['spouseImage'];

			if(!empty($file['name'])){
				$ds = DIRECTORY_SEPARATOR;  //1
				$storeFolder = ROOT . "/members/assets/images/$family_id/";   //2
				$thumb_path = ROOT . "/members/assets/imgs/$family_id/";   //2
				if (!file_exists($storeFolder)) {
					mkdir($storeFolder);
				}
				if (!file_exists($thumb_path)){
					mkdir($thumb_path);
				}

				$uploader = new \Uploader\ImageUpload('spouseImage');
				// Handle the upload
				$fileName = $storeFolder.$_FILES['spouseImage']['name'];
				
				$thumbnail = "/members/assets/images/$family_id/".$_FILES['spouseImage']['name'];
				$ext = pathinfo($fileName, PATHINFO_EXTENSION);
				$uploader->newFileName = $spouse_image_pathname = "spouse-{$family_id}-{$id}.".$ext;
				
				$result = $uploader->handleUpload($storeFolder);
				$uploader->generateThumbnail($storeFolder.$spouse_image_pathname, $ext, $thumb_path, $spouse_image_pathname, 200,200);
				if ($result) {
					$pdbo->updateRecord("members", ['id' => $id], ["spouse_image_pathname" => $spouse_image_pathname]);
				}
				echo $result ? 1 : 0;
			} else {
				echo 0;
			}
			exit;
		} elseif ($action === 'update') {
			extract($member);
			$arFamily = $pdbo->search("family")->fetchAll();
			$arMember = $pdbo->search('members',['family_id' => $family_id, 'gender' => 'male'])->fetchAll();
			$tpl_view = "form";
			if ($p) {
				extractVars('member_name','family_id', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio', 'parent_id','spouse_details', 'spouse_dob', 'spouse_dod');
				$member_pathname = makeSafeName($member_name);
				$arData = compact('member_name','family_id', 'member_pathname', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio', 'parent_id','spouse_details', 'spouse_dob', 'spouse_dod');

				$status = $pdbo->updateRecord("members", ['id' => $id], $arData);
			
				if($status){
					$tpl->showMessage('Success', "The entry was updated successfully", '/manage/members.php', 'success');			
				}else{
					$tpl->showMessage('Failed', "Failed to update the entry", JS_BACK);	
				}
			}
			$tpl->addData(compact('tpl_view', 'parent_id', 'member_name','family_id', 'gender', 'spouse','dob', 'dod', 'email', 'phone', 'address','bio','image_pathname','id','arFamily','arMember','spouse_image_pathname','spouse_details', 'spouse_dob', 'spouse_dod' , 'description', 'bio'));
		} else if ($action === 'remove') {
			
			$tpl->data['breadcrumb']['Remove'] = '/manage/family.php?mode=family&action=remove&id='. $id;
			if (!$p) {
				$tpl->addQuestion('Delete family', "Are you sure that you want to delete this family", array(array('Cancel', JS_BACK), array('Delete', "/manage/members.php?mode=member&action=remove&id=$id&p=1&redirect=" . urlencode($redirect))));
			} else {
				if ($pdbo->deleteRecord("members", ['id' => $id])) {
					$tpl->showMessage('Success', "Member deleted successfully.", $redirect, 'success');
				} else {
					$tpl->showMessage('Error', "Failed to remove member from database.<br/>{$pdbo->getErrorReport()}", JS_BACK);
				}
			}
		} else {
		
		}
	} 
}elseif($mode == 'list-members'){
	extractCleanVars('family_id');
	$arMember = $pdbo->search('members',['family_id' => $family_id, 'gender' => 'male'])->fetchAll();
	echo json_encode($arMember);   
	exit;
}

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();