<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Family Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<link href="<?=THEME_ASSETS_PATH?>/css/jquery-ui.min.css" rel="stylesheet" data-skip-ajax="true">
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-ui.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/notify.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script crossorigin="anonymous" src="/manage/assets/js/SimpleAjaxUploader.min.js"></script>
<script crossorigin="anonymous" src="/manage/assets/js/jquery-ui/jquery-ui.min.css"></script>
<script crossorigin="anonymous" src="/manage/assets/js/jquery-ui/jquery-ui.min.js"></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
#description {
	height: 280px !important;
	overflow: auto;
}
</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
	<li class="nav-item <?=($action === 'list') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/members.php?mode=members&action=list"><span class="icon icon-list"></span> Member List</a>
	</li>
	<li class="nav-item <?=($action === 'add') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/members.php?mode=members&action=add"><span class="icon icon-plus"></span> Add Member</a>
	</li>
	<?php if ($action === 'update'):?>
		<li class="nav-item <?=($action === 'update') ? 'active' : ''?>">
			<a class="nav-link" href="/manage/members.php?mode=member&action=update&id=<?=$id?>"><span class="icon icon-edit"></span> Edit</a>
		</li>
	<?php endif;?>
</ul>
<?php
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<?php if(!empty($arMember)):?>
		<div class="pad">
			<table class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Members</th>
						<th>Manage</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($arMember as $key => $member):?>
					<tr id="<?=$member['id']?>" data-id="<?=$member['id']?>">
						<td><?=$member['id']?> <i class="icon icon-arrows-v"></i></td>
						<td>
							<img src="/assets/imgs/member.png" alt="<?=$arFamily[$member['family_id']]?>" width="100" height="100">
							<?=$member['member_name']?> - <?=$arFamily[$member['family_id']]?>
						</td>
						<td>
							<div class="btn-group">
							<a class="btn btn-default btn-sm" href="/manage/members.php?mode=member&action=update&id=<?=$member['id']?>">Edit</a>
							<a class="btn btn-default btn-sm" href="/manage/members.php?mode=member&action=remove&id=<?=$member['id']?>">Delete</a>
							<a class="btn btn-default btn-sm" href="/members/<?=$member['family_pathname']?>">View</a>
						</div>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	<?php endif;?>
	
<?php elseif ($tpl_view === 'form'):?>
	<div class="container pad-large">
		<form class="form" action="/manage/members.php" method="post" enctype="multipart/form-data">
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Member Name</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="member_name" id="member_name" placeholder="John P Abraham" <?php if($action == 'update'):?> value="<?=$member_name?>"<?php endif ;?>>
			    </div>
			</div>
			<?php if (!empty($arFamily)):?>
				<div class="form-group row">
				    <label for="family_id" class="col-xs-12">Choose Family</label>
				    <div class="col-xs-12">
				    	<select type="text" class="form-control" name="family_id" id="family_id" placeholder="Parent Family">
				    		<?php foreach($arFamily as $fkey => $fvalue):?>
				    			<option value="<?=$fvalue['id']?>" <?php if(isset($id) && $family_id == $fvalue['id']):?> selected <?php endif;?>><?=$fvalue['family_name']?></option>
				    		<?php endforeach;?>
				    	</select>
				    </div>
				</div>
			<?php endif;?>
			<div class="form-group row list-members">
			    <label for="parent_id" class="col-xs-12">Choose Parent</label>
			    <div class="col-xs-12">
			    	<select type="text" class="form-control members" name="parent_id" id="parent_id" placeholder="Parent">
			    		<?php if(isset($arMember)):?>
							<option value="0"> Choose Parent </option>
			    			<?php foreach($arMember as $pkey => $pvalue):?>
				    			<option value="<?=$pvalue['member_uid']?>" <?php if(isset($parent_id) && $parent_id == $pvalue['member_uid']):?> selected <?php endif;?>><?=$pvalue['member_name']?></option>
				    		<?php endforeach;?>
			    		<?php endif; ?>
			    	</select>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Gender</label>
			    <div class="col-xs-12">
			    	<select type="text" class="form-control" name="gender" id="gender" placeholder="gender">
		    			<option value="Male">Male</option>
		    			<option value="Female">Female</option>
			    	</select>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Member Date of Birth</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="dob" id="spouse-datepicker" placeholder="04/12/2012" <?php if($action == 'update'):?> value="<?=$dob?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Member Date of Demise</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="dod" id="spouse-dod" placeholder="04/12/2012" <?php if($action == 'update'):?> value="<?=$dod?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Spouse</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="spouse" id="spouse_name" placeholder="Mary John" <?php if($action == 'update'):?> value="<?=$spouse?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Spouse Details</label>
			    <div class="col-xs-12">
					<textarea name="spouse_details" id="spouse_details" rows="5" cols="80">
						<?=(!empty($spouse_details) ? $spouse_details : '')?>
					</textarea>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Spouse Date of Birth</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="spouse_dob" id="datepicker" placeholder="04/12/2012" <?php if($action == 'update'):?> value="<?=$spouse_dob?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Spouse Date of Demise</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="spouse_dod" id="dod" placeholder="04/12/2012" <?php if($action == 'update'):?> value="<?=$spouse_dod?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Email</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="email" id="email" placeholder="john@gmail.com" <?php if($action == 'update'):?> value="<?=$email?>"<?php endif ;?>>
			    </div>
			</div>
			<?php if($action == 'add'):?>
				<div class="form-group row">
				    <label for="family_name" class="col-xs-12">Member Image</label>
				    <div class="col-xs-12">
				    	<input type="file" class="form-control" name="photo" id="image" placeholder="Choose image">
				    </div>
				</div>
			<?php elseif($action === 'update'):?>
				<div class="form-group row">
				    <label for="family_name" class="col-xs-12">Member Image</label>
				    <div class="col-xs-12">
				    	<img width="100%" height="100%" style="max-width: 250px;" src="/members/assets/imgs/<?=$family_id?>/<?=$image_pathname?>">
				    	<div class="tl pad-large">
				    		<button type="button" class="btn btn-primary" id="change-image" data-family_id="<?=$family_id?>" data-member_id="<?=$id?>"> Click to change image</button>
				    	</div>
				    </div>
				</div>
			<?php endif ;?>	
			<?php if($action == 'add'):?>
				<div class="form-group row">
				    <label for="family_name" class="col-xs-12">Spouse Image</label>
				    <div class="col-xs-12">
				    	<input type="file" class="form-control" name="spouse_photo" id="image" placeholder="Choose image">
				    </div>
				</div>
			<?php elseif($action === 'update'):?>
				<div class="form-group row">
				    <label for="family_name" class="col-xs-12">Spouse Image</label>
				    <div class="col-xs-12">
				    	<img width="100%" height="100%" style="max-width: 250px;" src="/members/assets/imgs/<?=$family_id?>/<?=$spouse_image_pathname?>">
				    	<div class="tl pad-large">
				    		<button type="button" class="btn btn-primary" id="spouse-change-image" data-family_id="<?=$family_id?>" data-member_id="<?=$id?>"> Click to change image</button>
				    	</div>
				    </div>
				</div>
			<?php endif ;?>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Phone No.</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="phone" id="phno" placeholder="9946463913" <?php if($action == 'update'):?> value="<?=$phone?>"<?php endif ;?>>
			    </div>
			</div>
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Address</label>
			    <div class="col-xs-12">
					<textarea name="address" id="address" rows="5" cols="80">
						<?=(!empty($address) ? $address : '')?>
					</textarea>
			    </div>
			</div>
			<div class="pad-large">
				<label for="description">Other Details (Short Description)</label>
				<textarea name="bio" id="description" rows="10" cols="80">
		            <?=(!empty($bio) ? $bio : '')?>
	           	</textarea>
			</div>
			
			 <?php if ($action === 'update'):?>
			 	<input type="hidden" name="id"  value="<?=$id?>">
			 	<input type="hidden" name="mode" value="member">
			 	<input type="hidden" name="action" value="update">
			 <?php else:?>
			 	<input type="hidden" name="mode" value="members">
			 	<input type="hidden" name="action" value="add">
			 <?php endif;?>

			 <input type="hidden" name="p" value="1">
			<div class="pull-right pad-large">
			 	<button type="submit" class="btn btn-large btn-primary <?=($action === 'add') ? 'btn-save-gallery' : 'btn-update-gallery'?>"><?=($action === 'add') ? 'Save' : 'Update'?></button>
			</div>
		</form>
	</div>
	
<?php endif;?>
	
</div>


<div id="addInfoModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Change Image</h4>
			</div>
			<div class="modal-body">
				<form id="uploadForm" action="upload.php" class="form pad-large" method="post">
					<div class="row">
						<div class="col-xs-8">
							<label>Upload Image File:</label><br/>
							<div class="change-member">
								<input name="memberImage" type="file" id="memberImage" class="inputFile" />
							</div>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary save-image-details">Save changes</button>
							<input type="hidden" id="family_id" name="family_id" value="">
							<input type="hidden" id="id" name="id" value="">
							<input type="hidden" id="mode" name="mode" value="member">
							<input type="hidden" id="action" name="action" value="update-member-image">
						</div>
						<div class="col-xs-4">
							<img id="member-new-image" class="hide" src="">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $this->render('theme::footer');?>

<script src="/manage/js/ckeditor/ckeditor.js"></script>
<script>
	"use strict";
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'description' );

    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "1900:2025"
    });
    $( "#dod" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "1900:2025"
      
    }); 
    $( "#spouse-datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "1900:2025"
    });
    $( "#spouse-dod" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "1900:2025"
      
    });
    $("#memberImage").change(function (){
       var fileName = $(this).val();
       // $("#member-new-image").attr('src', fileName);
     });

	$('#change-image').click(function(){
		var family_id = $(this).data('family_id');
		var member_id = $(this).data('member_id');
		$('#uploadForm').find("#family_id").val(family_id);
		$('#uploadForm').find("#id").val(member_id);
		$('#addInfoModal').modal('toggle');
	});
	$('#spouse-change-image').click(function(){
		var family_id = $(this).data('family_id');
		var member_id = $(this).data('member_id');
		$('#uploadForm').find("#family_id").val(family_id);
		$('#uploadForm').find("#id").val(member_id);
		$('#addInfoModal').modal('toggle');
		$('.change-member').html('<input name="spouseImage" type="file" id="spouseImage" class="inputFile" />');
		$('#action').val('change-spouse-image');
	});
	$("#uploadForm").on('submit',(function(e){
		e.preventDefault();
		var action = $('#action').val();
		$.ajax({
			url: "/manage/members.php?mode=member&action="+action,
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
		success: function(data){
			$('#addInfoModal').modal('toggle');
			$.notify("Successfully Uploaded", "success");
			location.reload();
		},
		error: function(){} 	        
		});
	}));

$('#family_id').change(function(){
	var family_id = $('#family_id').val();

	$.ajax({
  	  type: "POST",
	  url: "members.php",
	  data: {family_id : family_id, mode : 'list-members'},
	  success: function(data){
	  	if (data) {
	  		var code = '<option value="0">Select Parent</option>';
			for(var k in data) {
				code = code + '<option value= "'+data[k]['member_uid']+'">'+data[k]['member_name']+'</option>';   
			}
		} 
		if(code == ''){
			code = "<option>No members found </option>";
		}
		$('.members').html(code);
	  },
	  dataType: 'json'
	});
});
</script>
</body>
</html>