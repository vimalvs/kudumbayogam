<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi" async></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
	<li class="nav-item <?=($tpl_view === 'home') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/tributes.php?mode=tributes&action=list"><span class="icon icon-list"></span> Tributes List</a>
	</li>
	<li class="nav-item <?=($tpl_view === 'form') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/tributes.php?mode=tributes&action=add"><span class="icon icon-plus"></span> Add Tribute</a>
	</li>
</ul>
<?php if ($mode === 'home'):?>
<?php endif;?>
<?php 
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<div class="pad border">
		<?php if(!empty($arPage)):?>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Page Name</td>
						<td>Manage</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($arPage as $key => $page):?>
						<tr>
							<td><?=$page['id']?></td>
							<td><?=$page['page_title']?></td>
							<td>
								<div class="btn-group" role="group">
									<a type="button" class="btn btn-primary" href="/manage/tributes.php?mode=tribute&amp;action=update&amp;id=<?=$page['id']?>">Edit</a>
									<a type="button" class="btn btn-primary" href="<?=$page['page_pathname']?>">View</a>
									<a type="button" class="btn btn-danger" href="/manage/tributes.php?mode=tribute&amp;action=remove&amp;id=<?=$page['id']?>">Delete</a>
								</div>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		<?php endif;?>
	</div>
<?php elseif ($tpl_view === 'form'):?>
	<form class="form form-default pad" action="/manage/tributes.php" method="post">
		
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">page pathname</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="page_pathname" id="page_pathname" placeholder="page_pathname" <?php if($action == 'update'):?> value="<?=$page_pathname?>" <?php endif ;?>>
		    </div>
		 </div>
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">page title</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="page_title" id="page_title" placeholder="page_title" <?php if($action == 'update'):?> value="<?=$page_title?>"<?php endif ;?>>
		    </div>
		 </div>
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">page content title</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="page_content_title" id="page_content_title" placeholder="page_content_title" <?php if($action == 'update'):?> value="<?=$page_content_title?>"<?php endif ;?>>
		    </div>
		 </div>
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">page description</label>
		    <div class="col-sm-10">
		    	<textarea cols="110	" rows="6" name="page_description" id="page_description" placeholder="page_description">
		    		<?php if($action == 'update'):?>
		    			<?=$page_description?>
		    		<?php endif ;?>
		    	</textarea>
		    </div>
		 </div>

		<div class="pad-large">
			<label for="english_content">Content</label>
			<textarea name="page_content" id="page_content" rows="10" cols="80">
	            <?=(!empty($page_content) ? $page_content : '')?>
	        </textarea>
		</div>
		
		<div class="pad tr">
			<button type="submit" class="btn btn-lg btn-success">Submit Content</button>
			<input type="hidden" name="mode" value="<?=$mode?>">
			<input type="hidden" name="action" value="<?=$action?>">
			<input type="hidden" name="id" value="<?=$id?>">
			<input type="hidden" name="p" value="1">
		</div>
	</form>
<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<script src="/manage/js/ckeditor/ckeditor.js"></script>
  <script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'page_content' );
</script>
</body>
</html>