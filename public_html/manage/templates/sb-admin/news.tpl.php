<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<script src="/manage/assets/tinymce/tinymce.min.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
.border, .CodeMirror-wrap {
	border: 1px solid #ddd;
}

</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">
<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
  <li role="presentation" <?=($action === 'list') ? 'class="active"' : ''?>><a href="/manage/news.php?mode=events&action=list">List</a></li>
  <li role="presentation" <?=($action == 'add') ? 'class="active"' : ''?>><a href="/manage/news.php?mode=events&amp;action=add">Add</a></li>
  <?php if ($action === 'update'):?>
  	<li role="presentation" <?=($action == 'update') ? 'class="active"' : ''?>><a href="/manage/news.php?mode=event&amp;action=update&id=<?=$event_id?>">Edit</a></li>
  <?php endif;?>
</ul>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'list-news'):?>
	<div class="pad border">
		<table class="table">
 			<tr>
 				<th>#</th>
 				<th>Category</th>
 				<th>Title</th>
 				<th>Manage</th>
 			</tr>
 			<?php foreach($arEvents as $key => $event): ?>
 			<tr>
 				<td><?=++$key?></td>
 				<td><?=$event['event_category']?></td>
 				<td><?=$event['event_title']?></td>
 				<td>
 					<div class="btn-group" role="group">
					  	<a href="/news/<?=$event['event_pathname']?>" class="btn btn-primary" role="button" aria-disabled="true">View</a>
					  	<a href="/manage/news.php?mode=event&amp;action=update&amp;id=<?=$event['event_id']?>" class="btn btn-primary" role="button" aria-disabled="true">Edit</a>
						<a href="/manage/news.php?mode=event&amp;action=remove&amp;id=<?=$event['event_id']?>" class="btn btn-primary" role="button" aria-disabled="true">Delete</a>
					</div>
 				</td>
 			</tr>
 			<?php endforeach ; ?>
		</table>
	</div>
<?php elseif ($tpl_view === 'news-form'):?>

	<?php if($action == 'add') :?>
		<h1>Add News &amp; Achievements </h1>
	<?php elseif ($action == 'update') : ?>
		<h1>Update News &amp; Achievements </h1>
	<?php endif; ?>

	<form class="form form-default pad-large" action="/manage/news.php" method="post" enctype="multipart/form-data">
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">Event Category</label>
		    <div class="col-sm-10">
		    	<select class="form-control" name="event_category" id="event-category" required=true>
		    		<option value="">Choose</option>
		    		<option <?=(!empty($event_category) && $event_category === 'wedding-bells') ? 'selected' : ''?> value="wedding-bells">Wedding Bells</option>
		    		<option <?=(!empty($event_category) && $event_category === 'obituary') ? 'selected' : ''?> value="obituary">Obituary</option>
		    		<option <?=(!empty($event_category) && $event_category === '') ? 'selected' : 'congratulations'?> value="congratulations">Congratulations</option>
		    		<option <?=(!empty($event_category) && $event_category === 'new-borns') ? 'selected' : ''?> value="new-borns">New Borns</option>
		    		<option <?=(!empty($event_category) && $event_category === 'Other') ? 'selected' : ''?> value="Other">Other</option>
		    	</select>
		    </div>
		 </div>
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">Title</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="event_title" id="news-title" placeholder="Title" <?php if($action == 'update'):?>value="<?=$event_title?>"<?php endif ;?>>
		    </div>
		 </div>
		 <div class="form-group row">
		    <label for="event_summery" class="col-sm-2">Summery</label>
		    <div class="col-sm-10">
				<textarea id="event_summery" name='event_summery' class="form-control" cols="4" rows="10">
					<?php if($action == 'update'):?><?=!empty($event_summery) ? $event_summery : ''?><?php endif ;?>
				</textarea>
		    </div>
		 </div>
		 <div class="form-group row">
		    <label for="news-title" class="col-sm-2">Description</label>
		    <div class="col-sm-10">
		    	<textarea cols="110	" rows="6" name="event_description" id="event_description" placeholder="Event Description">
		    		<?php if($action == 'update'):?>
		    			<?=$event_description?>
		    		<?php endif ;?>
		    	</textarea>
		    </div>
		 </div>
		 <div class="form-group row">
		    <label for="event-date" class="col-sm-2">Event Date (Not Compulsory)</label>
		    <div class="col-sm-10">
		    	<input type="date" name="event_date" class="form-control" id="event-date" <?php if($action == 'update'):?>value="<?=$event_date?>"<?php endif ;?>>
		    </div>
		 </div>
		 <strong class="pad">*Ignore event date if it's an announcement</strong>
		 <?php if($action != 'update'):?>
			  <div class="form-group row pad">
			    <label for="event-photo" class="col-sm-2">Select image to upload</label>
			    <div class="col-sm-10">
			    	<input type="file" name="event_image" class="form-control" id="event-photo" >
			    </div>
			 </div>
		<?php endif; ?>
		 <div class="tc">
			<button class="btn btn-warning submit">Submit</button>
			<input type="hidden" name="p" value="1">
			<input type="hidden" name="mode" value="<?=$mode?>">
			<input type="hidden" name="action" value="<?=$action?>">
			<?php if($action == 'update'):?>
				<input type="hidden" name="id" value="<?=$event_id?>">
			<?php endif ; ?>
		</div/>
	</form>

<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<script src="/manage/js/ckeditor/ckeditor.js"></script>
  <script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'event_description' );
</script>
</body>
</html>