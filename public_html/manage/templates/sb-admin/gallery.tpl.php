<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Gallery Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<link href="<?=THEME_ASSETS_PATH?>/css/jquery-ui.min.css" rel="stylesheet" data-skip-ajax="true">
<script src="/manage/assets/tinymce/tinymce.min.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-ui.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/notify.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script crossorigin="anonymous" src="/manage/assets/js/SimpleAjaxUploader.min.js"></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
.border {
	border: 1px solid #ddd;
}
.progress {
    margin-bottom:0;
    margin-top:6px;
    margin-left:10px;
}
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 100%;
}
#sortable li {
	margin: 0px 10px 0px 10px;
	padding: 10px;
	float: left;
	font-size: 4em;
	text-align: center;
}
#gallery_description {
	height: 200px !important;
	overflow: auto;
}
</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
	<li class="nav-item <?=($tpl_view === 'home') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/gallery.php?mode=gallery_categories&action=list"><span class="icon icon-list"></span> Gallery List</a>
	</li>
	<li class="nav-item <?=($tpl_view === 'form') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/gallery.php?mode=gallery_categories&action=add"><span class="icon icon-plus"></span> Add Gallery</a>
	</li>
</ul>
<?php
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<?php if(!empty($arCategory)):?>
		<div class="pad">
			<table class="table table-sm table-bord ered">
				<thead>
					<tr>
						<th>#</th>
						<th>Category</th>
						<th>Manage</th>
					</tr>
				</thead>
				<tbody id="sortable-tbl">
				<?php foreach($arCategory as $key => $category):?>
					<tr id="<?=$category['position']?>" data-id="<?=$category['id']?>">
						<td><?=$category['id']?> <i class="icon icon-arrows-v"></i></td>
						<td>
							<img src="/assets/imgs/gallery.png" alt="<?=$category['category']?>" width="100" height="100">
							<?=$category['category']?>
						</td>
						<td>
							<div class="btn-group">
							<a class="btn btn-default btn-sm" href="/manage/gallery.php?mode=gallery_category&action=update&id=<?=$category['id']?>">Edit</a>
							<a class="btn btn-default btn-sm" href="/manage/gallery.php?mode=gallery_category&action=remove&id=<?=$category['id']?>">Delete</a>
							<a class="btn btn-default btn-sm" href="/gallery/<?=$category['category_pathname']?>">View</a>
						</div>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	<?php endif;?>
	
<?php elseif ($tpl_view === 'form'):?>
	<div class="container pad-large">
		<div class="form-group row">
		    <label for="news-title" class="col-sm-2">Gallery Category</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="gallery_category_name" id="gallery_category_name" placeholder="Category name" <?php if($action == 'update'):?> value="<?=$gallery_category_name?>"<?php endif ;?>>
		    </div>
		 </div>
		 <div class="form-group row">
		    <label for="gallery_description" class="col-sm-2">Description</label>
		    <div class="col-sm-10">
		    	<div id="gallery_description" class="form-control">
					<?php if($action == 'update'):?><?=!empty($gallery_description) ? $gallery_description : ''?><?php endif ;?>
				</div>
		    </div>
		 </div>
		 <div class="pull-right pad-large">
		 	<button class="btn btn-large btn-primary <?=($action === 'add') ? 'btn-save-gallery' : 'btn-update-gallery'?>"><?=($action === 'add') ? 'Save' : 'Update'?></button>
		 </div>
		 <?php if ($action === 'update'):?>
		 	<input type="hidden" name="id" id="gallery_category_id" value="<?=$id?>">
		 <?php endif;?>
	</div>
	<?php if (!empty($galleryImages)): ?>
		<div class="container pad-large">
			<ul id="sortable">
			<?php foreach($galleryImages as $key => $value):?>
				<li class="ui-state-default" id="<?=$value['image_order']?>" data-id="<?=$value['id']?>">
					<img src="<?=$value['thumbnail']?>" alt=" asdasd">
					<div class="t-xsmall"><button class="btn btn-small btn-primary btn-add-info"  data-id="<?=$value['id']?>"  type="button">Add Info</button><button class="btn btn-small btn-remove-img btn-primary" data-id="<?=$value['id']?>" type="button">Remove</button></div>
				</li>
			<?php endforeach;?>
			</ul>
		</div>
	<?php endif;?>
	<div id="file_upload_wrapper" class="container pad-large <?=($action === 'add') ? 'hide' : ''?>">
		<fieldset>
			<legend><h2>Choose Your Files</h2></legend>
		
			<div class="row">
				<div class="col-xs-2">
					<button id="uploadBtn" class="btn btn-large btn-primary">Choose File</button>
				</div>
				<div class="col-xs-8">
					<div id="progressOuter" class="progress progress-striped active" style="display:none;">
						<div id="progressBar" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
						</div>
					</div>
				</div>
				 <div  class="col-xs-2" id="msgBox">
			  </div>
			</div>
		</fieldset>
	</div>
<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<div id="addInfoModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add Image Info</h4>
			</div>
			<div class="modal-body">
				<div class="form-group row">
				    <label for="image_title" class="col-sm-2">Image Title</label>
				    <div class="col-sm-10">
				    	<input type="text" class="form-control" name="image_title" id="image_title" placeholder="Image Title">
				    </div>
				 </div>
				 <div class="form-group row">
				    <label for="image_description" class="col-sm-2">Description</label>
				    <div class="col-sm-10">
						<textarea id="image_description" name="image_description" class="form-control" cols="4" rows="5">
							
						</textarea>
				    </div>
				 </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save-image-details">Save changes</button>
				<input type="hidden" name="category_image" id="category_image" value="">
			</div>
		</div>
	</div>
</div>

<?php if ($action === 'update'):?>
	<script>
		var category_id = "<?=$id?>";
	</script>
<?php endif;?>
<script>

	$(function() {
		tinymce.init({
	    selector: '#gallery_description',
	    inline: true
	  });
		$('.save-image-details').click(function() {
			var image_title = $("#image_title").val();
			var image_description = $("#image_description").val();
			var image_id = $("#category_image").val();
			var postData = {
				image_title : image_title,
				image_description : image_description,
				mode: 'gallery_category',
				action: 'update-image',
				p: 1,
				id: category_id,
				image_id: image_id,
			}
			Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
				if (response['status'] == 1) {
					$('#addInfoModal').modal('toggle');
					$.notify("Image Details Added Successfully", "success");
				} else {
					$.notify("Failed", "error");
				}
			});	
		});

		$('.btn-add-info').click(function() {
			var $t  = $(this);
			var image_id  = $t.data('id');
			$('#category_image').val(image_id);
			$('#addInfoModal').modal('toggle');
			
			$.ajax({
	        	url: "/manage/gallery.php",
				type: "POST",
				data:  {action:'get-image-details', id : category_id, image_id:image_id, mode:'gallery_category'},
				dataType: "json",
				success: function(data)
			    {
			    	$("#image_title").val(data.caption);
					$("#image_description").val(data.description);
			    },
			  	error: function() 
		    	{
		    	} 	        
		   });
		});

		$('.btn-update-gallery').click(function() {
			var gallery_category_name = $("#gallery_category_name").val();
			var gallery_description = $("#gallery_description").val();
			var postData = {
				gallery_category_name : gallery_category_name,
				gallery_description : gallery_description,
				mode: 'gallery_category',
				action: 'update',
				p: 1,
				id: category_id,
			}
			Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response['status'] == 1) {
					inItfileUpload(response['category_id']);
					$(this).text('Update');
					$.notify("Gallery Category Added Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Gallery creation failed", "error");
				}
			});	
		});

		$('.btn-save-gallery').click(function() {
			var gallery_category_name = $("#gallery_category_name").val();
			var gallery_description = $("#gallery_description").val();
			var postData = {
				gallery_category_name : gallery_category_name,
				gallery_description : gallery_description,
				mode: 'gallery_categories',
				action: 'add',
				p: 1,
			}
			Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response['status'] == 1) {
					inItfileUpload(response['category_id']);
					$(this).text('Update');
					$.notify("Gallery Category Added Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Gallery creation failed", "error");
				}
			});	
		});





		$( "#sortable-tbl" ).sortable();
		$('#sortable-tbl').sortable({
		  update: function(event, ui) {
		  	var arOrder = [];
		  	$( "#sortable-tbl tr").each(function(index, item) {
		  	  arOrder.push({'id':$(item).data('id'), 'index' : index});
		  	});
		    var postData = {
				order : arOrder,
				mode: 'gallery_categories',
				action: 'update-category-order',
			}
			console.log('asdadsadadd a');
		  	Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response == 1) {
					$.notify("Order Changed Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Order Changing failed", "error");
				}
			});	
		    var order = $(this).sortable('toArray');
		  }
		});


		$( "#sortable" ).sortable();
		$('#sortable').sortable({
		  update: function(event, ui) {
		  	var arOrder = [];
		  	$( "#sortable li").each(function(index, item) {
		  	  arOrder.push({'id':$(item).data('id'), 'index' : index});
		  	});
		    var postData = {
				order : arOrder,
				mode: 'gallery_category',
				id: category_id,
				action: 'update-order',
			}

		  	Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response == 1) {
					$.notify("Order Changed Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Order Changing failed", "error");
				}
			});	
		    var order = $(this).sortable('toArray');
		  }
		});


		$('.btn-remove-img').click(function() {
			var $t  = $(this);
			var image_id  = $t.data('id');
			var postData = {
				id: category_id,
				image_id : image_id,
				mode: 'gallery_category',
				action: 'remove-image',
			}
			Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
				if (response == 1) {
					$.notify("Successfully Deleted", "success");
					$t.closest('li').remove();	
				} else {
					$.notify("Not Deleted", "error");
				}
			});	
		});
	});
function escapeTags( str ) {
	return String( str )
	.replace( /&/g, '&amp;' )
	.replace( /"/g, '&quot;' )
	.replace( /'/g, '&#39;' )
	.replace( /</g, '&lt;' )
	.replace( />/g, '&gt;' );
}

 function inItfileUpload(gallery_category_id) {
 	$('#file_upload_wrapper').removeClass('hide');
	var btn = document.getElementById('uploadBtn'),
		progressBar = document.getElementById('progressBar'),
		progressOuter = document.getElementById('progressOuter'),
		msgBox = document.getElementById('msgBox');

	var uploader = new ss.SimpleUpload({
	    button: btn,
	    url: '/manage/gallery.php?mode=gallery_category&action=upload&id='+gallery_category_id,
	    name: 'uploadfile',
	    // multipart: true,
	    // multipleSelect: true,
	    // multiple: true,
	    hoverClass: 'hover',
	    focusClass: 'focus',
	    responseType: 'json',
	    startXHR: function() {
	        progressOuter.style.display = 'block'; // make progress bar visible
	        this.setProgressBar( progressBar );
	    },
	    onSubmit: function() {
	        msgBox.innerHTML = ''; // empty the message box
	        btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
	      },
	    onComplete: function( filename, response ) {
	    	console.log(response, filename);
	        btn.innerHTML = 'Choose Another File';
	        progressOuter.style.display = 'none'; // hide progress bar when upload is completed

	        if ( !response ) {
	            msgBox.innerHTML = 'Unable to upload file';
	            return;
	        }

	        if ( response.success === true ) {
	            msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';
	            window.location = '/manage/gallery.php?mode=gallery_category&action=update&id='+gallery_category_id;
	            // location.reload();
	        } else {
	            if ( response.msg )  {
	                msgBox.innerHTML = escapeTags( response.msg );

	            } else {
	                msgBox.innerHTML = 'An error occurred and the upload failed.';
	            }
	        }
	      },
	    onError: function(filename, type, status, statusText, response, uploadBtn, size) {
			console.log(filename, type, status, statusText, response, uploadBtn, size);
		}
	});
}
</script>
<?php if ($action === 'update'):?>
	<script>
		inItfileUpload(category_id);
	</script>
<?php endif;?>
</body>
</html>