<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Family Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<link href="<?=THEME_ASSETS_PATH?>/css/jquery-ui.min.css" rel="stylesheet" data-skip-ajax="true">
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-ui.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/notify.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script crossorigin="anonymous" src="/manage/assets/js/SimpleAjaxUploader.min.js"></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
#description {
	height: 280px !important;
	overflow: auto;
}
</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
	<li class="nav-item <?=($tpl_view === 'home') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/family.php?mode=families&action=list"><span class="icon icon-list"></span> Family List</a>
	</li>
	<li class="nav-item <?=($tpl_view === 'form') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/family.php?mode=families&action=add"><span class="icon icon-plus"></span> Family</a>
	</li>
</ul>
<?php
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<?php if(!empty($arFamily)):?>
		<div class="pad">
			<table class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Family</th>
						<th>Manage</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($arFamily as $key => $family):?>
					<tr id="<?=$family['position']?>" data-id="<?=$family['id']?>">
						<td><?=$family['id']?> <i class="icon icon-arrows-v"></i></td>
						<td>
							<img src="/assets/imgs/family.png" alt="<?=$family['family_name']?>" width="100" height="100">
							<?=$family['family_name']?>
						</td>
						<td>
							<div class="btn-group">
							<a class="btn btn-default btn-sm" href="/manage/family.php?mode=family&action=update&id=<?=$family['id']?>">Edit</a>
							<a class="btn btn-default btn-sm" href="/manage/family.php?mode=family&action=remove&id=<?=$family['id']?>">Delete</a>
							<a class="btn btn-default btn-sm" href="/family/<?=$family['family_pathname']?>">View</a>
						</div>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div>
	<?php endif;?>
	
<?php elseif ($tpl_view === 'form'):?>
	<div class="container pad-large">
		<form class="form" action="/manage/family.php" method="post">
			<div class="form-group row">
			    <label for="family_name" class="col-xs-12">Family Name</label>
			    <div class="col-xs-12">
			    	<input type="text" class="form-control" name="family_name" id="family_name" placeholder="Padiyara " <?php if($action == 'update'):?> value="<?=$family_name?>"<?php endif ;?>>
			    </div>
			</div>
			<?php if (!empty($arFamily)):?>
				<div class="form-group row">
				    <label for="family_name" class="col-xs-12">Parent Family</label>
				    <div class="col-xs-12">
				    	<select type="text" class="form-control" name="parent_id" id="parent_id" placeholder="Parent Family">
				    		<?php foreach($arFamily as $fkey => $fvalue):?>
				    			<option value="<?=$fvalue['id']?>" <?=(!empty($parent_id) && $parent_id == $fvalue['id']) ? "selected='true'" : ''?>><?=$fvalue['family_name']?></option>
				    		<?php endforeach;?>
				    	</select>
				    </div>
				</div>
			<?php endif;?>
			<div class="pad-large">
				<label for="description">Content</label>
				<textarea name="description" id="description" rows="10" cols="80">
		            <?=(!empty($description) ? $description : '')?>
	           	</textarea>
			</div>
			
			 <?php if ($action === 'update'):?>
			 	<input type="hidden" name="id"  value="<?=$id?>">
			 	<input type="hidden" name="mode" value="family">
			 	<input type="hidden" name="action" value="update">
			 <?php else:?>
			 	<input type="hidden" name="mode" value="families">
			 	<input type="hidden" name="action" value="add">
			 <?php endif;?>

			 <input type="hidden" name="p" value="1">
			<div class="pull-right pad-large">
			 	<button type="submit" class="btn btn-large btn-primary <?=($action === 'add') ? 'btn-save-gallery' : 'btn-update-gallery'?>"><?=($action === 'add') ? 'Save' : 'Update'?></button>
			</div>
		</form>
	</div>
	
<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<script src="/manage/js/ckeditor/ckeditor.js"></script>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'description' );

	$(function() {

		$( "#sortable-tbl" ).sortable();
		$('#sortable-tbl').sortable({
		  update: function(event, ui) {
		  	var arOrder = [];
		  	$( "#sortable-tbl tr").each(function(index, item) {
		  	  arOrder.push({'id':$(item).data('id'), 'index' : index});
		  	});
		    var postData = {
				order : arOrder,
				mode: 'gallery_categories',
				action: 'update-category-order',
			}
			console.log('asdadsadadd a');
		  	Util.ajaxProcess("/manage/gallery.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response == 1) {
					$.notify("Order Changed Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Order Changing failed", "error");
				}
			});	
		    var order = $(this).sortable('toArray');
		  }
		});
	});
</script>
</body>
</html>