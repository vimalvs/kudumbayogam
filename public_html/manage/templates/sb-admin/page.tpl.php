<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi" async>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
#page_content_en, #page_content_ml {
	height: 400px !important;
	overflow: auto;
}
</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<?php if ($mode === 'home'):?>
<?php endif;?>
<?php 
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<div class="pad border">
		<?php if(!empty($arPage)):?>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Page Name</td>
						<td>Manage</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($arPage as $key => $page):?>
						<tr>
							<td><?=$page['id']?></td>
							<td><?=$page['page_title']?></td>
							<td>
								<div class="btn-group" role="group">
									<a type="button" class="btn btn-primary" href="/manage/page.php?mode=page&amp;action=update&amp;id=<?=$page['id']?>">Edit</a>
									<a type="button" class="btn btn-primary" href="<?=$page['page_pathname']?>">View</a>
								</div>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		<?php endif;?>
	</div>
<?php elseif ($tpl_view === 'form'):?>
	<form class="form form-default pad" action="/manage/page.php" method="post">
		
		<div class="pad-large">
			<label for="page_content_en">Content In Malayalam</label>
			<textarea name="page_content_en" id="page_content_en" rows="10" cols="80">
	            <?=(!empty($page_content_en) ? $page_content_en : '')?>
	        </textarea>
		</div>
		
		<div class="pad-large">
			<label for="page_content_ml">Content In Malayalam</label>
			<textarea name="page_content_ml" id="page_content_ml" rows="10" cols="80">
	            <?=(!empty($page_content_ml) ? $page_content_ml : '')?>
	        </textarea>
		</div>
		<div class="pad tr">
			<button type="submit" class="btn btn-lg btn-success">Submit Content</button>
			<input type="hidden" name="mode" value="<?=$mode?>">
			<input type="hidden" name="action" value="<?=$action?>">
			<input type="hidden" name="id" value="<?=$id?>">
			<input type="hidden" name="p" value="1">
		</div>
	</form>
<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<script src="/manage/js/ckeditor/ckeditor.js"></script>
  <script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'page_content_en' );
    CKEDITOR.replace( 'page_content_ml' );
</script>
</body>
</html>