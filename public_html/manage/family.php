<?php
include 'prepend.php';
$tpl->setTemplate("family");

$mode = getCleanVar('mode', 'families');
$tpl_view = "home";

extractCleanVars('action', 'p', 'redirect');

$pdbo = \SiteManager::getDataBase();

if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/manage/family.php' : htmlentities($_SERVER['HTTP_REFERER']);
}

if (empty($action)) $action = 'list';

if ($mode === 'families') {
	if ($action === 'list') {
		$stmt = $pdbo->search("family", null, ['position', 'ASC']);
		$arFamily = $stmt ? $stmt->fetchAll() : [];	
		$tpl->addData(compact('arFamily'));

	} elseif($action === 'add') {
		$arFamily = $pdbo->search("family")->fetchAll();
		extractVars('family_name', 'description', 'p', 'parent_id');
		$tpl_view = 'form';
		if ($p) {
			if (empty($arFamily)) {
				$parent_id = 0;
			}
			if(empty($family_name)) $tpl->addMessage('The family_name cannot be empty');
			// if(empty($description)) $tpl->addMessage('The description cannot be empty');
			if(empty($parent_id)) $tpl->addMessage('The parent_family cannot be empty');
			
				
			if(!$tpl->hasError()){	
				$stmt = $pdbo->query("SELECT * FROM family h ORDER BY h.id DESC LIMIT 1");
				$lastInsertRecord = $stmt ? $stmt-> fetch() : [];
				if ($lastInsertRecord) {
					$position = $lastInsertRecord['position'] + 1;
				} else {
					$position = 1;
				}
				$family_uid = substr(strtolower($family_name), 0, 2).$position;
				$family_pathname = makeSafeName(trim($family_name));
				if(!$pdbo->getRecord("family", ["family_pathname" => $family_pathname])){
					$status = $pdbo->insertRecord("family", ['family_name' => trim($family_name), 'family_pathname' => $family_pathname, 'description' => trim($description), 'parent_id' => $parent_id, 'position' => $position, 'family_uid' => $family_uid]);
					if($status){
						$tpl->showMessage('Success', "The new entry was added successfully", '/manage/family.php', 'success');			
					}else{
						$tpl->showMessage('Failed', "Failed to add the new entry", JS_BACK);	
					}
				} else {
					$tpl->showMessage('Failed', "The pathname $family_pathname exists", JS_BACK);	
				}
			}
		}
		$tpl->addData(compact('tpl_view', 'arFamily'));

	} else if ($action === 'update-category-order') {
		extractVars('order');
		foreach ($order as $key => $value) {
			$pdbo->updateRecord("family", ['id' => $value['id']], ['position' => $value['index']]);
		}
		echo 1;
		exit;
	}

} elseif ($mode === 'family') {

	extractCleanVars('id');
	
	$stmt = $pdbo->search("family", ['id' => $id]);

	$family = $stmt ? $stmt->fetch() : null;
	// printr($id, $family);
	// exit;
	if (!$family) {
		$tpl->showMessage('Invalid Option', 'Selected family is not found in our database', ['Back' => $redirect]);
	} else {

		if ($action === 'update') {
			extract($family);
			$arFamily = $pdbo->search("family")->fetchAll();

			$tpl_view = "form";
			if ($p) {
				extractCleanVars('family_name', 'parent_id', 'description');
				$status = $pdbo->updateRecord("family", ['id' => $id], ['family_name' => $family_name,  'family_pathname' => makeSafeName($family_name), 'description' => trim($description), 'parent_id' => $parent_id]);
				if($status){
					$tpl->showMessage('Success', "The entry was updated successfully", '/manage/family.php', 'success');			
				}else{
					$tpl->showMessage('Failed', "Failed to update the entry", JS_BACK);	
				}
			}
			$tpl->addData(compact('tpl_view', 'family_name', 'parent_id', 'description', 'arFamily'));
		} else if ($action === 'remove') {
			
			$tpl->data['breadcrumb']['Remove'] = '/manage/family.php?mode=family&action=remove&id='. $id;
			if (!$p) {
				$tpl->addQuestion('Delete family', "Are you sure that you want to delete this family", array(array('Cancel', JS_BACK), array('Delete', "/manage/family.php?mode=family&action=remove&id=$id&p=1&redirect=" . urlencode($redirect))));
			} else {
				if ($pdbo->deleteRecord("family", ['id' => $id])) {
					$tpl->showMessage('Success', "family deleted successfully.", $redirect, 'success');
				} else {
					$tpl->showMessage('Error', "Failed to remove family from database.<br/>{$pdbo->getErrorReport()}", JS_BACK);
				}
			}
		} else {
		
		}
	} 
}

$tpl->addData(compact('mode', 'id', 'action', 'tpl_view', 'redirect', 'arPage'));

$tpl->generate();