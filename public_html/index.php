<?php
include 'include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('site_home');

$stmt = $pdbo->search("header_images", null, ['image_order', 'ASC']);
$arImage = $stmt ? $stmt->fetchAll() : [];


// $stmt = $pdbo->query("SELECT  gc.id as gallery_id, gi.id as image_id, gc.category_pathname,gc.category,gi.image_pathname,gi.thumbnail,gi.caption FROM gallery_categories gc INNER JOIN gallery_images gi ON (gc.id = gi.gid) ORDER BY RAND() LIMIT 10");
// $arGalleryImage = $stmt ? $stmt->fetchAll() : [];


$stmt = $pdbo->search("event", null, ['event_created_time', 'DESC'], 3);
$arEvent = $stmt ? $stmt->fetchAll() : [];

$stmt = $pdbo->query("SELECT  * FROM members m INNER JOIN family f ON (m.family_id = f.id) ORDER BY RAND() LIMIT 4");
$arMember = $stmt ? $stmt->fetchAll() : [];

$tpl->addData(compact('arImage', 'arGalleryImage', 'arEvent', 'arMember', 'page_view', 'site_view'));

$tpl->generate();
