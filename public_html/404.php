<?php
header("HTTP/1.0 404 Not Found");
$arPath = explode('/', $_SERVER['REQUEST_URI']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>404 Error - Page cannot be found</title>
<meta name="robots" name="noindex, nofollow, noarchive, nosnippet"/>
<style type="text/css">
*{margin:0px;padding:0px;}
body{background-color: #FFF;color: #111;font: normal 95%/130% "Trebuchet MS", Verdana, Arial, sans-serif;margin:0px;padding:20px 0px;}
a {color: #d17e62;text-decoration: none;border-bottom: 1px solid #EEE;}
a:hover{border-bottom: 1px solid #ddd;color: #c30;text-decoration: none;}
h1{color:#407a40;font:normal 1.7em Georgia, "Times New Roman", Times, serif;margin:0px;padding:0px;}
h2,h3{color: #dc7000;font: normal 1.25em Georgia, "Times New Roman", Times, serif;margin: 20px 0px;padding: 0px;}
ul{margin:10px 20px;}
li{list-style:disc;}
#wrapper{background-color:#FFFFFF;border:2px solid #DDDDDD;margin:auto;width:40em;}
#header {background:#EEE;border-bottom:2px solid #DDD;color:#888;font-size:85%;margin:0px;padding:3px 10px;}
#header img{height:32px;width:32px;margin:0px 10px 0px 0px;vertical-align:middle;}
#breadcrumb{margin:0px 0px 20px 0px;font-style:italic;color:#666;font-size:85%;}
#body{padding:10px;}
#goog-wm { }
#goog-wm h3.closest-match { }
#goog-wm h3.closest-match a { }
#goog-wm h3.other-things { }
#goog-wm ul li { }
#goog-wm li.search-goog { display: block; }

.comment{color:#999;}
</style>
</head>
<body>
<div id="wrapper">
<div id="header">
<img src="/favicon.ico" alt="Home" style="width:32px;height:32px;"/><a title="MalluBar Home Page" href="http://www.mallubar.com/">MalluBar</a> &#8594; Page not found</div>
<div id="body">
<h1>Page not found</h1>
<div id="breadcrumb">
<?php
/*
$host = $_SERVER['HTTP_HOST'];
$path = $_SERVER['REQUEST_URI'];
$arPath = explode('/', $path);
$count = count($arPath);
unset($arPath[0]);
echo "<a href='/'>$host</a> &#8594; ";

if($count > 2){
	$errno = $errstr = '';
	$path = '/';
	for($i = 1, $count--; $i < $count; $i++){
		if($fp = pfsockopen($host, 80, $errno, $errstr, 5)){
			$path .= "$arPath[$i]/";
			$out = "GET $path HTTP/1.1\r\n";
			$out .= "Host: $host\r\n";
			//$out .= "Connection: Close\r\n\r\n";
			$out .= "Keep-Alive: 30"; 
			$out .= "Connection: keep-alive\r\n\r\n";
			fwrite($fp, $out);
			$content = fgets($fp);
			$code = trim(substr($content, 9, 4));

			//echo "$path - $content<br/><br/>";
			if($code === '200'){
				echo "<a href='$path'>$arPath[$i]</a> &#8594; ";
				unset($arPath[$i]);
			}else{
				break;
			}
		   fclose($fp);
		}
	}
}
echo '/'.implode('/', $arPath);
*/
?>
</div>
<p>Sorry, the requested page <code class="comment"><?php echo $_SERVER['REQUEST_URI'];?></code> cannot be found on the server.</p>
<p>Are you sure you have entered the url correctly? If so, the page may have been moved, renamed or even deleted.</p>
<?php
if(isset($customError))echo "$customError";
?>
<ul>
<li><a href="/">Click here to go to the homepage.</a></li>
<li><a href="javascript:history.back();">Click here to go back.</a></li>
</ul>
<div id="leftContent">
<script type="text/javascript">
  var GOOG_FIXURL_LANG = 'en';
  var GOOG_FIXURL_SITE = 'http://www.mallubar.com/';
</script>
<script type="text/javascript" 
    src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js"></script>
</div>
<div id="rightContent"></div>
</div>
</div>
</body>
</html>
<?php
exit;
?>