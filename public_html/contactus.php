<?php
define('SECTION_NAME', 'Home');
define('SECTION_PATHNAME', '');
include 'include/prepend.php';
$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('contactus');

if (AJAX_REQUEST){
	extractCleanVars('name', 'surname', 'email', 'phone', 'message');
	$mailData = [
		'fromEmail' => 'info@padiyaravallikattu-kudumbayogam.com',
		'fromName' => 'From Contact Page User : '.$name . ' '.$surname,
		'toEmail' => 'contact@padiyaravallikattu-kudumbayogam.com',
		'toName' => 'Admin',
		'subject' => 'Feedback Contact Page',
		'body' => $message."\n\n".$name."({$phone})",
	];
	$status = sendSendGridEmail($mailData);
	if ($status) {
    	echo json_encode(['type' => 'success', 'message' => 'Successfully submitted']);
	} else {
    	echo json_encode(['type' => 'danger', 'message' => 'Unable to submit your message']);
	}
	exit;
}

$tpl->generate();
