// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @externs_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/jquery-1.8.js
// @js_externs jQuery.lazyLoad = function (str, fn) {};
// @js_externs jQuery.prototype.button = function (str) {};
// @js_externs jQuery.prototype.fileupload = function (opt) {};
// @js_externs /** @constructor */ jQuery.notification = function (str, opt) {}
// ==/ClosureCompiler==
(function($){
	/** @const */ var __TARGET_HTML = '<div class="fileupload-dropzone"><span>Drag and Drop files here</span> <span class="label label-default">or</span> <button class="btn btn-default btn-xs fileupload-button">Browse</button></div>';
	/** @const */ var __INPUT_HTML = '<input name="file" type="file" multiple="multiple" />';
	/**
	 * @constructor
	 */
	function AjaxFileUploader (elem, options) {
		options = $.extend(true, {}, AjaxFileUploader.defaults, options);
		elem = $(elem);
		if (options.trigger) {
			var uploader = this;
			elem.one('click', function () {
				$(this).not('.fileupload-button').hide();
				uploader.init(elem, options);
			});
		} else {
			this.init(elem, options);
		}
	};
	AjaxFileUploader.defaults = {
		target: '',
		callback : {success : null, error : null, complete : null},
		extra : {}
	};
	AjaxFileUploader.prototype = {
		init : function(elem, options) {
			var uploader = this;
			this.options = options;
			this.element = $(elem);
			var path = (THEME_ASSETS_PATH || '/assets') + '/lib/jquery-fileupload/v5/js/';
			$.lazyLoad([[path + 'vendor/jquery.ui.widget.js'], path + 'jquery.fileupload.js']).done(function() {
				var $target;
				if (options.target === '') {
					$target = elem;
				} else {
					$target = options.target || $(__TARGET_HTML).appendTo(elem);
				}
				uploader.__target = $target;

				var $input = $(__INPUT_HTML).appendTo($target),
					$dropzone = $target.parent().find('.fileupload-dropzone');

				var action = options.action || $target.closest('form').attr('action');
				var $btn = $('.fileupload-button', $target).click(function() {
					// The input field is replaced after each upload. Hence use a selector instead of the object
					uploader.__browse();
					return false;
				});
				uploader.__browse = function () {
					this.__target.find('[name=file]').click();
				};

				var isButton = $btn.is('.btn');
				$target.show();
				var o = $.extend({url : action, dataType : 'json', formData : options.formData, dropZone : $dropzone}, options.extra);

				$input.fileupload(o).addClass('fileupload-input')
				.bind('fileuploadalways', function(e, data) {
					if (isButton) {
						$btn.button('reset');
					}
					if (data.textStatus === "success") {
						if (uploader.__trigger('complete', e, data)) {
							var msgs = data.result.messages;
							if (msgs.type === "error") {
								if (uploader.__trigger('error', e, data)) {
									new $.notification(msgs.message[0].error, {'class' : 'alert-danger'});
								}
							} else {
								uploader.__trigger('success', e, data);
							}
						}
					} else {
						if (uploader.__trigger('success', e, data)) {
							Util.alert('Error', 'Failed to upload. Try again later');
						}
					}
				})
				.bind('fileuploadsend', function(e, data){
					if (uploader.__trigger('start', e, data)) {
						if (isButton) {
							$btn.button('loading');
						}
					}
				});
				uploader.__trigger('init');
			});
		},
		browse : function () {
			this.__browse();
		},
		// From jQuery UI Widget
		__trigger : function (type, event, data) {
			var prop, callback = this.options[type];
			data = data || {};
			event = $.Event(event);
			event.type = 'ajaxupload' + type;
			event.target = this.element[0];
			var orig = event.originalEvent;
			if (orig) {
				for (prop in orig) {
					if (!(prop in event)) {
						event[prop] = orig[prop];
					}
				}
			}
			$(this.element).trigger(event, [this].concat(data));
			return !($.isFunction(callback) && callback.apply(this.element, [event].concat(data)) === false || event.isDefaultPrevented());
		}
	};
	$.fn["ajaxFileUploader"] = function (options) {
		return this.each(function () {
			var $this = $(this);
			var uploader = $this.data('ajaxFileUploader');
			if (!uploader) {
				uploader = new AjaxFileUploader(this, options);
				$(this).data('ajaxFileUploader', uploader);
			}
			if (uploader[options]) {
				return uploader[options].apply(uploader, Array.prototype.slice.call(arguments, 1));
			}
		});
	};
})(jQuery);