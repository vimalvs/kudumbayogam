/**
 * jQuery.autosave - Automatically save form data in localstorage
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 * Dual licensed under MIT and GPL.
 * Date: 2011/12/06
 * @author Joyce Babu
 * @version 1.0.0
 *
 */
(function($) {
	var AutoSave = function(elem, options) {
		// console.log('Initializing', elem);
		this.init(elem, options);
	};
	$.extend(AutoSave, {
		purge : function () {
			var ms = (new Date()).getMilliseconds();
			for (var i = 0; i < localStorage.length; i++) {
			    var key = localStorage.key(i);
				if (key.substr(0, 10) !== '__autosave__') continue;
				if (JSON.parse(localStorage.getItem(key)).__expiry__ < ms) {
					localStorage.removeItem(key);
				}
			}
		},
		discard : function (id) {
			localStorage.removeItem('__autosave__' + id);
		},
		prototype : {
			hasSaved : false,
			init : function (elem, options) {
				var autosave = this,
				handle = this.element = $(elem).addClass('autosave');
				this.options = options;
				if (!localStorage || (elem.tagName.toLowerCase() !== 'form') || !options.autosave) return;

				this.key = '__autosave__' + this.options.autosave;
				var tmp = localStorage.getItem(this.key);
				this._hasSaved = tmp;
				this.data = tmp ? JSON.parse(tmp) : {};
				this.data['__expiry__'] = (new Date()).getMilliseconds() + 604800000;

				$(elem).on('keypress.autosave change.autosave', options.selector, function () {
					autosave.save(this);
				});
				this.__trigger('init', null, this);
			},
			check : function () {
				return this._hasSaved;
			},
			load : function () {
				if (this.__trigger('load')) {
					var autosave = this;
					this.element.find(this.options.selector).each(function () {
						if (typeof autosave.data[this.name] !== "undefined") {
							$(this).val(autosave.data[this.name]);
						}
					});
					this.__trigger('loaded');
					return true;
				}
				return false;
			},
			save : function (input) {
				this.data[input.name] = input.value;
				localStorage.setItem(this.key, JSON.stringify(this.data));
			},
			discard : function () {
				if (this.__trigger('discard')) {
					return localStorage.removeItem(this.key);
				}
				return false;
			},
			// From jQuery UI Widget
			__trigger : function (type, event, data) {
				var orig, prop, data = data || {},
				callback = this.options[type];
				event = $.Event(event);
				event.type = 'autosave' + type;
				event.target = this.element[0];
				var orig = event.originalEvent;
				if (orig) {
					for (prop in orig) {
						if (!(prop in event)) event[prop] = orig[prop];
					}
				}
				$(this.element).trigger(event, [this].concat(data));
				return !($.isFunction(callback) && callback.apply(this.element, [event].concat(data)) === false || event.isDefaultPrevented());
			}
		}
	});
	if (localStorage) AutoSave.purge();
	
	$.fn.autosave = function (options) {
		return this.each(function () {
			var $this = $(this), 
			autosave = $this.data('$autosave'), 
			opt = $.extend({autosave : this.autosave}, $.fn.autosave.defaults, $this.data(), typeof options == 'object' && options);
			if (!autosave) $this.data('$autosave', (autosave = new AutoSave(this, opt)));
			if (autosave[options]) {
				return autosave[options].apply(autosave, Array.prototype.slice.call(arguments, 1));
			} else if ( typeof options === 'object' || ! options ) {
				// return methods.init.apply( this, args );
			} else {
				$.error( 'Method ' +  options + ' does not exist on jQuery.autosave' );
			}
		});
	}

	$.fn.autosave.defaults = {
		init : function () {
			if (this._hasSaved) {
				if (options.action === 'load') this.load();
			}
		},
		load : null,
		selector : ':input[name]:not([readonly])'
	};

	$.autosave = AutoSave
		
})(jQuery);