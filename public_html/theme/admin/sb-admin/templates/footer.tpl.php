<div class="container">
<footer class="footer">
  <p class="pull-right"><a class="btn" href="#"><i class="icon icon-chevron-up"> </i>  Back to top</a></p>
  <p>&copy; <a href="http://www.ennexa.com/">Ennexa Technologies &#9413; Ltd</a> <?=date('Y')?></p>
</footer>
</div>