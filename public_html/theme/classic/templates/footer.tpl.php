<footer>
	<div id="footer">
        <div class="footer-links container p-2">
            <ul class="footer-nav">
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/">Home</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/aboutus.php">About Our Family</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/history.php">Family History</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/members/">Who is Who </a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/geneology/">Family Geneology</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/gallery/">Photo Gallery</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/contactus.php">Contact Us</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/newsletter/">Family News Letter</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/committee-members.php">Committee Members</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/tributes/">Tributes</a></li>
            </ul>    
        </div>
        <div class="footer-info p-2">
            <p class="text-center">Copyright &copy; <?php echo date('Y')?><br>Padiyara Vallikattu Kudumbayogam<br>All rights reserved.</p>
        </div>
  </div>
</footer>
<script type="text/javascript">
    if (!window.jQuery) {
        document.write('<' + 'script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/jquery.min.js"><'+'/script>');
    }
</script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/popper.min.js"></script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/src/js/common.js"></script>
