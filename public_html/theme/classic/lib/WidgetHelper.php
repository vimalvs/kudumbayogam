<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Widget Helper
 * 
 * Theme specific widget helper
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Theme\Classic;

class WidgetHelper {
	
	
	private $contentArea = null;
	private $contentAreaName = null;
	
	private static function hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) {
			$r = hexdec($hex[0].$hex[0]);
			$g = hexdec($hex[1].$hex[1]);
			$b = hexdec($hex[2].$hex[2]);
		} else {
			$r = hexdec($hex[0].$hex[1]);
			$g = hexdec($hex[2].$hex[3]);
			$b = hexdec($hex[4].$hex[5]);
		}

		return array($r, $g, $b); // returns an array with the rgb values
	}
	
	public function __construct($contentArea) {
		$this->contentArea = $contentArea;
		$this->contentAreaName = $contentArea->getName();
	}
	
	public function format($widget) {
		$data = $widget->getHtml();
		if (!$data) {
			return false;
		}
		$options = $widget->options();
		
		$extra = $options['extra_options'];
		
		$code = "<div " . ($extra['id'] ? "id='{$extra['id']}'" : '') . " class='wdgt-base {$extra['class']}'>";
		if ($extra['title']) {
			$code .= "<h3 class=\"wdgt-title\">{$extra['title']}</h3>";
		}
		return $code . $data . '</div>';
		
	}
	
	public function add($wdgt) {
		
	}
	
	public function update($wdgt, &$errorInfo) {
		$theme_options = getCleanPostVar('theme_options');
		extract($theme_options);
		// printr($theme_options);
		if (!$theme_options) {
			$wdgt->options(['theme_options' => null]);
			return;
		}
		
		if ($bg === '#ffffff') $theme_options['bg'] = null;
		if ($color === '#000000') $theme_options['color'] = null;
		if ($icon_bg === '#ffffff') $theme_options['icon_bg'] = null;
		if ($icon_color === '#000000') $theme_options['icon_color'] = null;
		
		if (!empty($theme_options['image_fade'])) {
			$theme_options['bg_rgb'] = implode(',', self::hex2rgb($theme_options['bg'] ?: '#ffffff'));
		} else {
			$theme_options['image_fade'] = false;
		}
		$wdgt->options(compact('theme_options'));
		// switch ($wdgt_layout) {
		// 	case 'list':
		// 		break;
		// 	case 'thumbnail':
		//
		// 		break;
		// 	case 'thumbnail-list':
		//
		// 		break;
		// 	case 'slide':
		//
		// 		break;
		// 	default:
		//
		// }
	}
	public static function initialize() {
		
	}
	
}