<?php
namespace Theme\Classic;

class Template extends \PHPTemplate {
	
	private $defaults = [
		'field' => [
			'id' => null, 'class' => null, 'cols' => 3
		],
		'input' => [
			'id' => '', 'class' => '', 
			'type' => 'string', 'value' => '', 
			'description' => '', 'attr' => '', 'required' => '', 
			'size' => null, 'list_use_key' => true, 'disable_html5' => true
		],
	];
	
	public function __construct($tpl = NULL) {
		parent::__construct($tpl);
	}
	
	public function setDefaults($options, $key = null) {
		if (is_null($key)) {
			$this->defaults = array_merge($this->defaults, $options);
		} else {
			$this->defaults[$key] = array_merge($this->defaults[$key], $options);
		}
	}
	
	public function pagination($nPages, $nCurrentPage = 1, $linkName = NULL, $enableRewrite = NULL, $addNoFollow = FALSE, $index = NULL) {
		$nCurrentPage = (int)$nCurrentPage;
		$nCurrentPage = ($nCurrentPage >= 1) ? $nCurrentPage : 1;
		$nPages = ceil($nPages);
		if (!$linkName) {
			// $linkName = 'http';
			// if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') $linkName .= 's';
			// $linkName .= '://';
			// if ($_SERVER['SERVER_PORT'] != '80') $linkName .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['PHP_SELF'];
			//else $linkName .= $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
			$linkName = rtrim(preg_replace('/(?<=[&?])page=[0-9]*(&|$)/', '', $_SERVER['REQUEST_URI']), '&');
			$linkName .= ((strpos($linkName, '?') === FALSE) ? '?' : '&') . 'page=';
			$enableRewrite = FALSE;
		} elseif (is_assoc_array($linkName)) {
			if (isset($linkName['navBaseUrl'])) {
				$baseUrl = $linkName['navBaseUrl'];
			} else {
				$baseUrl = end(explode('/', $_SERVER['SCRIPT_FILENAME']));			
			}
			$baseUrl .= '?';
			$pageVar = isset($linkName['navPageVar']) ? $linkName['navPageVar'] : 'page';
			unset($linkName['navBaseUrl'], $linkName['navPageVar']);
			foreach ($linkName as $k => $v) {
				if (!empty($v))$baseUrl .= "$k=$v&amp;";
			}
			$linkName = "$baseUrl$pageVar=";
		
		}
		if (is_null($enableRewrite)) $enableRewrite = (substr($linkName, -1) !== '=');
		if (is_null($index)) $index = $enableRewrite ? './' : $linkName . 1;
		if ($enableRewrite) {
			if (substr($linkName, -1) != '-') $linkName .= '-';
			$linkExt = '.html';
		} else {
			$linkExt = '';	
		}
		$i = ($nCurrentPage !== 1) ? 2 : 1;
		$rel = $addNoFollow ? 'rel="nofollow"' : '';
		if ($nPages <= 1) return FALSE;
		ob_start();
		?>
		<ul class="pagination">
		<?php if ($nCurrentPage === 2):?> 
			<li class="prev"><a <?=$rel?> title='Previous Page' href='<?=$index?>'>&laquo; </a></li>
		<?php elseif ($nCurrentPage > 2):?>
			<li class="prev"><a <?=$rel?> title='Previous Page' href='<?php echo $linkName, ($nCurrentPage - 1), $linkExt?>'>&laquo; </a></li>
		<?php else:?>
			<li class="prev disabled"><a title="This is the first page">&laquo;</a></li>
		<?php endif;?>
	
		<?php if ($nPages < 8):?>
			<?php if ($i === 2):?>
				<li><a <?=$rel?> href='<?=$index?>'>1</a></li>
			<?php endif;?>
			<?php for (; $i <= $nPages; $i++):?>
				<?php if ($i == $nCurrentPage):?><li class="current active"><a><?=$i?></a></li>
				<?php else:?><li><a <?=$rel?> href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
			<?php endfor;?>
		<?php else:?>
			<?php if ($nCurrentPage <= 4):?>
				<?php if ($i === 2):?><li><a <?=$rel?> href='<?=$index?>'>1</a></li><?php endif;?>
				<?php for(; $i <= $nCurrentPage + 2; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active" title='Current Page'><a><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
				<li class="disabled"><a>...</a></li><li><a <?=$rel?> title='Last Page' href='<?=$linkName, $nPages, $linkExt?>'><?=$nPages?></a></li>
			<?php elseif ($nCurrentPage >= $nPages - 3):?>
				<li><a <?=$rel?> title='First Page' href='<?=$index?>'>1</a></li><li class="disabled"><a>...</a></li>
				<?php for ($i = $nCurrentPage - 2; $i <= $nPages; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active"><a <?=$rel?> title='Current Page'><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
			<?php else:?>
				<li><a <?=$rel?> title='First Page' href='<?=$index?>'>1</a></li><li class="disabled"><a>...</a></li>
				<?php for ($i = $nCurrentPage - 2; $i <= $nCurrentPage + 2; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active"><a <?=$rel?> title='Current Page'><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
				<li class="disabled"><a>...</a></li><li><a <?=$rel?> title='Last Page' href='<?=$linkName, $nPages, $linkExt?>'><?=$nPages?></a></li>
			<?php endif;?>
		<?php endif;?>
		<?php if ($nCurrentPage < $nPages):?><li class="next"><a class="next" <?=$rel?> title='Next Page' href='<?=$linkName, ($nCurrentPage + 1), $linkExt?>'>&raquo;</a></li>
		<?php else:?><li class="next disabled"><a title="This is the last page">&raquo;</a></li><?php endif;?>
		</ul>
		<?php return ob_get_clean();
	}

	public function table($arData, $header, $options = array()) {
		$defaultOption = array('startIndex' => 1, 'placeholder' => 'There is no data to display', 'class' => 'table table-striped', 'rowAttrs' => '', 'sort' => array('fields' => array(), 'field' => '', 'direction' => 'asc'));
		if (isset($options['sort'][0])) $options['sort'] = array('fields' => $options['sort']);
		if (!empty($options['sort']['fields'])) {
			$sort =& $options['sort'];
			if (empty($sort['href'])) {
				$sort['href'] = isset($_SERVER['SCRIPT_URL']) ? $_SERVER['SCRIPT_URL'] : (($pos = strpos($_SERVER['REQUEST_URI'], '?')) ? substr($_SERVER['REQUEST_URI'], 0, $pos) : $_SERVER['REQUEST_URI']);
				parse_str($_SERVER['QUERY_STRING'], $arTmp);
				if (!empty($arTmp['sort_field'])) {
					$sort['field'] = $arTmp['sort_field'];
					unset($arTmp['sort_field']);
				}
				if (!empty($arTmp['sort_direction'])) {
					$sort['direction'] = $arTmp['sort_direction'];
					unset($arTmp['sort_direction']);
				}
				unset($arTmp['page']);
				if (!empty($arTmp)) $sort['href'] .= '?' . http_build_query($arTmp);
			}
			$sort['href'] .= (strpos($sort['href'], '?') !== FALSE) ? '&' : '?';
			if (!isset($sort['field'])) $sort['field'] = '';
			if (!isset($sort['direction'])) $sort['direction'] = 'asc';
		}
		extract(array_merge($defaultOption, $options));
		if (!empty($options['dataFilter'])) {
			$arData = array_filter(array_map($options['dataFilter'], $arData));
		}
		$noData = empty($arData);
		
		foreach ($header as $k => $v) {
			if (is_int($v)) $v = $header[$k] = array(ucwords(str_replace('_', ' ', $k)), $v);
			if (!is_array($v) || (empty($v[1]) && ($v = $v[0]))) {
				if (!$noData && array_key_exists($k, $arData[0])) $v = array($v, T_STRING);
				else $v = array($v, T_TEMPLATE, $v);
				$header[$k] = $v;
			} elseif ($v[1] === T_CHECKBOX) {
				$name = empty($v[2]) ? $k : $v[2];
				$header[$k] = array('<input type="checkbox" class="chk-table-bulk-action"/>', T_TEMPLATE, "<input type='checkbox' value='{{$k}}' name='{$name}[]' />");
			} elseif (count($v) === 2) {
				if ($v[1] === T_DATE && empty($v[2])) {
					$header[$k][2] = 'Y-m-d';
				} elseif ($v[1] === T_TIME && empty($v[2])) {
					$header[$k][2] = 'Y-m-d g:i a';
				} elseif (is_callable($v[1])) {
					$header[$k] = array($v[0], T_FUNCTION, $v[1]);
				} elseif (!is_int($v[1])) {
					$header[$k] = array($v[0], T_TEMPLATE, $v[1]);
				}
			}
			if ($header[$k][1] === T_TEMPLATE && !$noData) {
				if (empty($header[$k][2])) trigger_error("Template string cannot be empty for column <em>{$v[0]}</em>", E_USER_WARNING);
				// $arTemplateKey = array_keys($arData[0]);
				// foreach ($arTemplateKey as $k1 => $v2) {
				//  	$arTemplateKey[$k1] = '{' . $v2 . '}';
				// }
			}
		}
		$rowAttrsCallable = is_callable($rowAttrs);
		$row_attrs = $rowAttrsCallable ? '' : $rowAttrs;
	?>
		<table<?=empty($options['id']) ? '' : " id=\"{$options['id']}\""?> class="<?=$class?>">
		<thead>
		<tr>
			<?php foreach ($header as $k => $v):?>
				<?php if (in_array($k, $sort['fields'])):?>
					<?php if ($sort['field'] === $k):?>
						<th class="tablesorter-header tablesorter-headerSort<?=($sort['direction'] === 'desc' ? 'Up' : 'Down')?>"><a class="tablesorter-header-inner" href="<?=htmlspecialchars($sort['href'])?>sort_field=<?=urlencode($k)?><?=($sort['direction'] !== 'desc' ? '&amp;sort_direction=desc' : '')?>"><?=$v[0]?></a></th>
					<?php else:?>
						<th class="tablesorter-header"><a class="tablesorter-header-inner" href="<?=htmlspecialchars($sort['href'])?>sort_field=<?=urlencode($k)?>"><?=$v[0]?></a></th>
					<?php endif;?>
				<?php else:?>
					<th><?=$v[0]?></th>
				<?php endif;?>
			<?php endforeach;?>
		</tr>
		</thead>
		<tbody>
		<?php if ($noData):?>
			<tr><td class="tc" colspan="<?=count($header) + 1?>"><em><?=$placeholder?></em></td></tr>
		<?php endif;?>
		<?php foreach ($arData as $idx => $data):
			$data['$INDEX'] = $startIndex;
			if ($rowAttrsCallable) $row_attrs = $rowAttrs($data, $idx);
		?>
		<tr <?=$row_attrs?>>
			<?php foreach ($header as $k => $v):?>
				<?php if ($v[1] === T_STRING):
					if (empty($data[$k])) {
						$val = is_null($data[$k]) ? 'NULL' : 'EMPTY';
						echo "<td><span class='label label-default'>$val</span></td>";
					} else {
						echo  '<td>', htmlspecialchars($data[$k]), '</td>';
					}
				?>
				<?php elseif ($v[1] === T_INDEX):?>
					<td class="tindex"><?=$startIndex++?></td>
				<?php elseif ($v[1] === T_DATE || $v[1] === T_TIME):
					if (is_numeric($data[$k])):
						$tmp = new \DateTime;
						$tmp->setTimestamp($data[$k]);
					else:
						$tmp = new \DateTime($data[$k]);
					endif;
				?>
					<td><?=$tmp->format($v[2]);?></td>
				<?php elseif ($v[1] === T_TEMPLATE):
					$INDEX = $idx;
					$FIELD = $k;
				?>
					<td><?=preg_replace_callback('/\{(>?)(\$?[a-z0-9_]+)\}/i', function($match) use($data, $v){
						if (empty($data[$match[2]])) {
							$val = is_null($data[$match[2]]) ? 'NULL' : 'EMPTY';
							return  ($v[2] === $match[0]) ? "<span class='label label-default'>$val</span>" : "{{$val}}";
						} else {
							return empty($match[1]) ? $data[$match[2]] : htmlspecialchars($data[$match[2]]);
						}
					}, $v[2]);?></td>
				<?php elseif ($v[1] === T_FUNCTION):?>
					<?php call_user_func($v[2], $data);?>
				<?php else:?>
					<td><?=$data[$k]?></td>
				<?php endif;?>
			<?php endforeach;?>
			</tr>
		<?php endforeach;?>
		</tbody>
		</table>
	<?php	
	}
	
	public function formField($label, $field, $opt = NULL, $placeholder = FALSE, $hint = NULL, $cols = null) {
		if (null === $opt) $opt = array();
		if (empty($opt['id'])) $opt['id'] = "fin_" . preg_replace('/[^a-z0-9-_]/i', '__', $field);
		$hasError = ($this->options['print_message'] === \PHPTemplate::PRINT_MESSAGE_GENERAL && !empty($this->messages->message[$field]['error']));
		$groupOpt = array_merge($this->defaults['field'], isset($opt['group']) ? $opt['group'] : []);
		if (is_null($label)) $cols = 0;
		elseif (is_null($cols)) $cols = $groupOpt['cols'];
		?>
		<div <?=$groupOpt['id'] ? "id=\"{$groupOpt['id']}\" " : ''?>class="form-group <?=$cols ? 'row' : ''?> <?=$hasError ? 'has-error' : ''?> <?=$groupOpt['class']?>">
			<?php if (!is_null($label)):?>
				<label class="control-label <?=$cols ? "col-md-$cols" : ''?>" for="<?=$opt['id']?>"><?=$label?></label>
			<?php endif;?>
		    <div class="controls <?=$cols ? ("col-md-" . (12 - $cols)) : ''?>">
				<?php $this->formInput($field, $opt, $placeholder, array('hasError' => $hasError))?>
				<?php if ($hint):?><p class="help-block"><?=$hint?></p><?php endif;?>
			</div>
		</div>
		<?php
	}
	
	public function formInput($field, $opt = NULL, $placeholder = FALSE, $fromFormField = FALSE) {
		if (null === $opt) $opt = array();
		$opt['attributes'] = '';
		if (!$fromFormField) {
			if (empty($opt['id'])) $opt['id'] = "fin_" . preg_replace('/[^a-z0-9-_]/i', '__', $field);
			$fromFormField['hasError'] = ($this->options['print_message'] === \PHPTemplate::PRINT_MESSAGE_GENERAL && !empty($this->messages->message[$field]['error']));
		}
		$opt = array_merge($this->defaults['input'], $opt);
		extract($fromFormField);
		extract($opt, EXTR_REFS);
		
		$field_name = htmlspecialchars($field);
		if ($type === 'uneditable') $class .= ' uneditable-input';
		if ($hasError) $class .= " error";
		if ($placeholder) $attr['placeholder'] = $placeholder;
		if ($required) $attr['required'] = 'required';
		
		if (empty($value)) {
			$fname = $field;
			if ($type === 'multi_list') $fname = substr($fname, 0, -2);
			if (isset($this->data[$fname])) $value = $this->data[$fname];
			else {
				if (preg_match('/([a-z0-9_]+)(?:\[([^]]+)\])(?:\[([^]]+)\])?/i', $fname, $matches) && !empty($this->data[$matches[1]])) {
					$value = $this->data[$matches[1]];
					for ($i = 2, $cnt = count($matches); $i < $cnt; $i++) {
						if (isset($value[$matches[$i]])) $value = $value[$matches[$i]];
						else {
							$value = NULL;
							break;
						}
					}
				}
			}
		}
        
		if (!empty($multi)) $list = $multi;
        if (in_array($type, ['list', 'multi_list'])) {
			if ($size > 0) {
				$attr['size'] = $size;
			}
			if (!$opt['list_use_key']) {
	            $tmp = reset($list);
	            if (is_array($tmp)) {
	                $list = array_map(function ($val) {
	                    return array_combine($val, $val);
	                }, $list);
	            } else {
	                $list = array_combine($list, $list);
	            }
			}
        }
		
		$attributes .= "id=\"$id\" ";
		$baseClass = ($type === 'file') ? '' : 'form-control ';
		if ($type !== 'boolean') $attributes .= "class=\"$baseClass $class\" ";
		if (!empty($attr)) {
			if (isset($attr['minlength']) && empty($attr['title'])) {
				$attr['title'] = "Minimum {$attr['minlength']} characters required";
				$attr['pattern'] = '.{3,}';
			}
			foreach ($attr as $k => $v) {
				$attributes .= ($k . '="' . $v . '" ');
			}
		}
		
		if ($type === 'string'):
			echo "<input name=\"$field_name\" $attributes value=\"$value\" " . (empty($list) ? '' : "list=\"{$id}_datalist\"") . " type=\"text\" />";
			?>
			<?php if (!empty($list)):?>
				<datalist id="<?=$id?>_datalist">
					<?php if (isset($list[0]) || !$opt['list_use_key']):?>
						<?php foreach ($list as $v):?>
							<option value="<?=$v?>">
						<?php endforeach;?>
					<?php else:?>
						<?php foreach ($list as $k => $v):?>
							<option value="<?=$k?>" label="<?=htmlspecialchars($v)?>">
						<?php endforeach;?>
					<?php endif;?>
				</datalist>
			<?php endif;
		elseif ($type === 'text'):
	    	echo "<textarea name=\"$field_name\" $attributes rows='5' cols='22'>$value</textarea>";
		elseif ($type === 'code'):
			if (empty($editor)) $editor = 'code';
	    	echo "<div class='$class input-editor input-field input-textarea'><textarea name=\"$field_name\" data-editor='$editor' $attributes rows='5' cols='22'>" . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '</textarea></div>';
		elseif ($type === 'boolean'):
			echo "<div class='checkbox'><label><input class='$class' type='checkbox' name=\"$field_name\" value='1' " . (!empty($value) ? 'checked="checked"' : '') . " $attributes/> $description</label></div>";
		elseif ($type === 'list'):?>
			<?php if ($size !== -1):?>
				<select name="<?=$field_name?>" <?=$attributes?>>
				<?php foreach ($list as $k => $v):?>
					<?php if (is_array($v)):?>
						<optgroup label="<?=$k?>">
							<?php foreach ($v as $k1 => $v1):?>
								<option value="<?=$k1?>"  <?=(($value == $k1) ? 'selected="selected"' : '')?>><?=$v1?></option>
							<?php endforeach;?>
						</optgroup>
					<?php else:?>
						<option value="<?=$k?>"  <?=(($value == $k) ? 'selected="selected"' : '')?>><?=$v?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			<?php else:?>
				<fieldset class="form-list-wrapper">
				<?php foreach ($list as $k => $v):?>
					<?php if (is_array($v)):?>
						<fieldset>
							<legend><?=$k?></legend>
							<?php foreach ($v as $k1 => $v1):?>
								<div class="radio"><label><input type="radio" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k1) ? 'checked="checked"' : '')?>> <?=$v1?></label></div>
							<?php endforeach;?>
						</fieldset>
					<?php else:?>
						<div class="radio"><label><input type="radio" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k) ? 'checked="checked"' : '')?>> <?=$v?></label></div>
					<?php endif;?>
				<?php endforeach;?>
				</fieldset>
			<?php endif;?>
		<?php elseif ($type === 'multi_list'):
			$value = (array)$value;
			?>
			<?php if ($size !== -1):?>
				<select name="<?=$field_name?>" multiple="multiple" <?=$attributes?>>
				<?php foreach ($list as $k => $v):?>
					<?php if (is_array($v)):?>
						<optgroup label="<?=$k?>">
							<?php foreach ($v as $k1 => $v1):?>
								<option value="<?=$k1?>" <?=(in_array($k1, $value) ? 'selected="selected"' : '')?>><?=$v1?></option>
							<?php endforeach;?>
						</optgroup>
					<?php else:?>
						<option value="<?=$k?>" <?=(in_array($k, $value) ? 'selected="selected"' : '')?>><?=$v?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			<?php else:?>
				<fieldset class="form-list-wrapper">
				<?php foreach ($list as $k => $v):?>
					<?php if (is_array($v)):?>
						<fieldset>
							<legend><?=$k?></legend>
							<?php foreach ($v as $k1 => $v1):?>
								<div class="checkbox"><label><input type="checkbox" name="<?=$field_name?>" value="<?=$k?>" <?=(in_array($k1, $value) ? 'checked="checked"' : '')?>> <?=$v1?></label></div>
							<?php endforeach;?>
						</fieldset>
					<?php else:?>
						<div class="checkbox"><label><input type="checkbox" name="<?=$field_name?>" value="<?=$k?>" <?=(in_array($k, $value) ? 'checked="checked"' : '')?>> <?=$v?></label></div>
					<?php endif;?>
				<?php endforeach;?>
				</fieldset>
			<?php endif;?>
		<?php elseif($type === 'toggle'):?>
			<div data-toggle="buttons-radio" class="btn-group" style="display:inline-block;">
				<?php foreach ($list as $k => $v):?>
					<label class="btn" for="<?=($tmpId = makeSafeName($id . '-' . $k))?>"><input type="radio" id="<?=$tmpId?>" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k) ? 'checked="checked"' : '')?>> <?=$v?></label>
				<?php endforeach;?>
			</div>
		<?php elseif ($type === 'captcha'):?>
			<?php if (!empty($provider) && $provider === 'recaptcha'):?>
				<?=\ReCaptcha::getHTML()?>
			<?php else:?>
				<div class="input-group captcha">
					<a class="reload-captcha input-group-addon" href="#"><img class="captcha-image" src="/res/verify/?code=<?=$_SESSION['vCode']?>&amp;len=5" alt="Enable Images" ></a>
					<input name="<?=$field_name?>" type="text" <?=$attributes?> required="required" autocomplete="off" value="">
				</div>
			<?php endif;?>
		<?php elseif (!in_array($type, array('range', 'date', 'file', 'time')) && is_callable($type)):?>
			<?php call_user_func($type, $field, $opt, $placeholder, $fromFormField)?>
		<?php elseif ($type === 'uneditable'):?>
			<span <?=$attributes?>><?=$value?></span>
			<input type="hidden" name="<?=$field_name?>" value="<?=$value?>" />
		<?php elseif ($type === 'masked'):?>
			<input name="<?=$field_name?>" type="password" <?=$attributes?> value="<?=$value?>" />
		<?php elseif ($type === 'label'):?>
			<span class="input-label"><?=$value?></span>
		<?php elseif (in_array($type, ['date', 'datetime-local', 'time', 'month'])):?>
			<div class="form-control-datetime <?=empty($opt['disable_html5']) ? 'html5-datetime' : ''?>" data-type="<?=$type?>" data-name="<?=$field_name?>" data-prefix="<?=isset($opt['field_prefix']) ? $opt['field_prefix'] : "{$field_name}_"?>">
				<?php $this->datetimeFallback($field_name, $type, $value, $opt);?>
			</div>
		<?php else:?>
			<input name="<?=$field_name?>" type="<?=$type?>" <?=$attributes?> value="<?=$value?>">
		<?php endif;?>
	    <?php if ($hasError):?>
			<p class="help-block"><?=$this->messages->message[$field]['error'];?></p>
		<?php endif;
	}
	
	function datetimeFallback($field_name, $type, $value, $opt = null) {
		echo "<div class=\"form-input-$type\">";
        $attr = $opt['attr'];
        $field_prefix = isset($opt['field_prefix']) ? $opt['field_prefix'] : "{$field_name}_";
        
		if ($type === 'date' || $type === 'month') {
			if (empty($value)) $value = '--';
			$arMonth = [
                '' => 'Month', '1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', 
                '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec'
            ];
			list($tyear, $tmon, $tday) = explode('-', $value) + [2 => null];
			$minYear = isset($attr['min']) ? explode('-', $attr['min'])[0] : 1901;
			$maxYear = isset($attr['max']) ? explode('-', $attr['max'])[0] : 2025;
			$yRange = range($maxYear, $minYear);
			$yRange = ['' => 'Year'] + array_combine($yRange, $yRange);
			$this->formInput("{$field_prefix}year", ['type' => 'list', 'multi' => $yRange, 'value' => $tyear]);
			$this->formInput("{$field_prefix}month", ['type' => 'list', 'multi' => $arMonth, 'value' => $tmon]);
            if ($type !== 'month') {
				$dRange = ['' => 'Day'];
    			for ($i = 1; $i <= 31; $i++) $dRange[$i] = $i < 10 ? "0$i" : $i;
    			$this->formInput("{$field_prefix}day", ['type' => 'list', 'multi' => $dRange, 'value' => $tday]);
            }
		} elseif ($type === 'time') {
			if (empty($value)) $value = ':';
			list($thour, $tmin) = explode(':', $value);
			$tmer = ($thour > 12) ? 'pm' : 'am';
			$thour %= 12;
			if ($thour === 0) $thour = 12;
			$hRange = ['12' => '12'];
			for ($i = 1; $i < 12; $i++) $hRange[$i] = $i < 10 ? "0$i" : $i;
			for ($i = 0; $i < 60; $i++) $mRange[$i] = $i < 10 ? "0$i" : $i;
			$this->formInput("{$field_prefix}hour", ['type' => 'list', 'multi' => $hRange, 'value' => $thour]);
			$this->formInput("{$field_prefix}min", ['type' => 'list', 'multi' => $mRange, 'value' => $tmin]);
			$this->formInput("{$field_prefix}apm", ['type' => 'list', 'multi' => ['am' => 'AM', 'pm' => 'PM'], 'value' => $tmer]);
		} elseif ($type === 'datetime-local') {
			$tmp = explode('T', $value);
			$this->datetimeFallback($field_name, 'date', $tmp[0], $attr);
			$this->datetimeFallback($field_name, 'time', isset($tmp[1]) ? $tmp[1] : null, $attr);
		}
		echo '</div>';
	}
	
	public function printMessage($fields = []) {
		if (!empty($this->messages->message)) {
			$messages = array();
			$bPrintGeneral = ($this->options['print_message'] === self::PRINT_MESSAGE_GENERAL);
			if ($bPrintGeneral) {
				$flag = empty($fields);
				foreach ($this->messages->message as $idx => $message) {
					if (is_int($idx) || !($flag || in_array($idx, $fields))) $messages[] = $message;
				}
			} else {
				$messages = $this->messages->message;
			}
		} else {
			return ;
		}
		
		$arClass = array('error' => 'danger', 'warning' => 'warning', 'success' => 'success', 'info' => 'info');
		
		if (empty($this->messages->title) && empty($this->messages->button) && count($messages) < 5 && $bPrintGeneral):?>
			<?php foreach ($messages as $key => $message):?>
				<div class="alert alert-<?=$arClass[key($message)]?>"><?=current($message)?></div>
			<?php endforeach;?>
		<?php elseif (!empty($messages)):?>
			<div class="msgBox <?=$this->messages->type?>Msg alert alert-block alert-<?=$arClass[$this->messages->type]?>">
				<?php if (!empty($this->messages->title)):?>
					<h4 class='msgTitle alert-heading'><span><?=$this->messages->title?></span></h4>
				<?php endif;?>
				<div class="msgContent">
				<?php if (count($messages) > 1):?>
					<ul class="msgWrapper">
					<?php foreach ($messages as $key => $message):?>
						<li class="<?=key($message)?>"><?=current($message)?></li>
					<?php endforeach;?>
					</ul>
				<?php else:?>
					<div class="msgWrapper"><p><?=current(reset($messages))?></p></div>
				<?php endif;?>
				</div>
				<?php if (!empty($this->messages->button)):?>
					<div class="msgButton"><span>
					<?php foreach ($this->messages->button as $button):
						if ($button['value'][0] === '#'):
							$type = 'primary';
							$button['value'] = substr($button['value'], 1);
						else:
							$type = 'default';
						endif;
					?>
						<a class="btn btn-<?=$type?>" href="<?=$button['href']?>"><?=$button['value']?></a>
					<?php endforeach;?>
					</span></div>
				<?php endif;?>
			</div>
		<?php endif;
	}
	
}