<?php
\SiteManager::registerHook(['theme_init', $this], function () {
	$tpl = $this->getTemplate();
	if (\SiteManager::getTemplate() !== $tpl) {
		return;
	}

	\SiteManager::registerHook(['template_head', $tpl], function () use ($tpl) {
		// Runs at higher priority compared to following template_head
		// if (defined('SKIP_WIDGET')) return;
		$template_head_start_time = microtime(true);
		$layout = $this->getLayout();
		$arContentArea = $layout->getContentAreas();
		
		$path = Theme\ContentArea::getPath();
		$arWidgets = [];
		$arLazyWidget = [];
		$redisStatus = $lazyStatus = false;
		

		foreach ($arContentArea as $name => $contentArea) {
			$widgets = $contentArea->load($path);
			$arWidgets[$name] = $widgets;
			if ($redisStatus) {
				foreach ($widgets as $wid) {
					if (is_array($wid)) {
						$arLazyWidget[$wid[3]] = $wid[1];
					}
				}
			}
		}
		if ($arLazyWidget) {
			$hash = md5(implode("", array_keys($arLazyWidget)));
			$key = SITE_CODE . '_wdgtset_' . $hash;
			
			$lazyStatus = $redis->multi()
				->setNx($key, json_encode($arLazyWidget))
				->expire($key, 300)
				->exec();
			if ($lazyStatus) {
				RequestManager::setConfig('wdgtset', $hash);
			}
		}

		\SiteManager::registerHook(['template_content_area', $tpl], function ($contentArea) use ($arWidgets, $lazyStatus) {
			if (!$arWidgets[$contentArea]) return;

			foreach ($arWidgets[$contentArea] as $wid) {
				if (is_array($wid)) {
					echo $wid[0], ($lazyStatus ? '' : $wid[1]), $wid[2], PHP_EOL;
				} else {
					echo $wid, PHP_EOL;
				}
			}

			if ($GLOBALS['userAuth']->isAdmin()) {
				$layout = $this->getLayout();
				echo "<div hidden class=\"theme-content-area-info hide\" data-content-area=\"{$this->getName()}::{$layout->getName()}::$contentArea\" data-id=\"" . implode(',', array_keys($arWidgets[$contentArea])) . "\"></div>";
			}
		}, false);
		$template_head_elapsed_time = microtime(true) - $template_head_start_time;
		\Feedback\Stats::timing("timing.php.theme.modern.sidecontent", $template_head_elapsed_time * 1000);


		$userAuth = \Auth\Session::getInstance();
		?>
	<script>
		var _DRCB = _DRCB || [],  _CONF = {html5input: {}, grade: 'B'};;
			var _DRCB = _DRCB || [];
	<?php if ($userAuth instanceof \Auth\Session):?>
		var User = <?=json_encode($userAuth->getUserData())?>;
	<?php else:?>
		var User = null;
	<?php endif;?>
	</script>
	<?php if (empty($_COOKIE['adb_status'])):?><script src="/assets/js/adb-detect.js"></script><?php endif;?>
	<?php
	}, true, 0);
});
