<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?=$member_name?> <?=$family_name?> Family Member</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Padiyara Vallikattu Family Members list..." />
<?php $this->render('theme::headContent');?>
<style>
.member-name{
	font-weight: bold;
}

.tc{
	text-align: center;
}
.member{
	padding: 5px;
}
.m-fieldset {
	padding: 10px;
	border:1px solid #e6e6e6;
}
.m-fieldset > legend {
	width: auto;
	padding: 6px;
	font-size: 17px;
	line-height: 17px;	
}
.twitter-typeahead {
    width: 100%;
    vertical-align: middle
}

.tt-dropdown-menu {
    text-align: left;
    width: 100%
}

.typeahead,
.tt-query,
.tt-hint {
    outline: none
}

.typeahead {
    background-color: #fff
}

.tt-query {
    -webkit-transition: background-color .4s ease 0s;
    transition: background-color .4s ease 0s
}

.tt-menu {
    background-color: #fff;
    border: 1px solid #f2f2f2;
    width: 100%
}

.tt-hint {
    color: #999
}

.tt-dropdown-menu {
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ddd;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .15);
    margin-top: 1px;
    color: #000
}

.tt-suggestion {
    padding: 3px;
    padding-bottom: 10px;
    border-bottom: solid #ccc 1px;
    margin: 0
}

.tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf
}

.tt-suggestion:hover {
    color: #fff;
    background-color: #0097cf;
    cursor: pointer
}

.drop-tt-empty-message {
    padding: 5px 10px;
    text-align: center;
    color: #fff;
    background: red
}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div class="container">
	      		<div class="text-center my-2">
			      	<h1> Who Is Who </h1>
			      	<hr class="h-underline">
			    </div>
	    		<div class="m-2">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<div class="card">
								<?php if(!empty($image_pathname)):?>
									<img class="card-img-top" src="/members/assets/images/<?=$family_id?>/<?=$image_pathname?>" alt="<?=$member_name?>">
								<?php else:?>
									<img class="card-img-top" src="/assets/imgs/member.png" alt="<?=$member_name?>">
								<?php endif;?>
								<div class="card-body">
									<h5 class="card-title"><?=$member_name?></h5>
									<p class="card-text">
										<div><span class="fa fa-fw fa-home"></span> <?=$family_name?></div>
										<div><span class="fa fa-fw fa-birthday-cake"></span> <?=$dob?></div>
										<div><span class="fa fa-fw fa-phone"></span> <?=$phone?></div>
										<div><span class="fa fa-fw fa-envelope"></span> <?=$email?></div>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-8">
							<div class="card mb-2">
								<div class="card-body">
									<h5 class="card-title">Other Informations</h5>
									<p class="card-text">
										<?php if(!empty($parentDetails)):?>
											<div>Parent Name <span class="fa fa-fw fa-user"></span> : <?=$parentDetails['member_name']?></div>
										<?php endif;?>
										<?php if(!empty($address)):?>
											<div>Address <span class="fa fa-fw fa-edit"></span> : <?=$address?></div>
										<?php endif;?>
										<p></p>
										<?php if(!empty($bio)):?>
											<p>
												<?=$bio?>
											</p>
										<?php endif;?>
									</p>
								</div>
							</div>

							<?php if(!empty($spouse)):?>

								<fieldset class="m-fieldset">
									<legend>Spouse Details</legend>
									<div class="m-2">
										<div class="row">
											<div class="col-sm-12 col-md-4">
												<div class="card">
													<?php if(!empty($spouse_image_pathname)):?>
														<img class="card-img-top" src="/members/assets/images/<?=$family_id?>/<?=$spouse_image_pathname?>" alt="<?=$spouse?>">
													<?php else:?>
														<img class="card-img-top" src="/assets/imgs/woman.png" alt="<?=$spouse?>">
													<?php endif;?>
													<div class="card-body">
														<strong class="t-small"><?=$spouse?></strong>
														<p class="card-text">
															<?php if(!empty($spouse_dob)):?>
																<div><span class="fa fa-fw fa-birthday-cake"></span> : <?=$spouse_dob?></div>
															<?php endif;?>
															<?php if(!empty($spouse_dod)):?>
																<div><span class="fa fa-fw fa-calendar"></span> : <?=$spouse_dod?></div>
															<?php endif;?>
														</p>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-8">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">Other Details</h5>
														<p class="card-text">
															<?php if(!empty($spouse_details)):?>
																<p>
																	<?=$spouse_details?>
																</p>
															<?php else:?>
																<p>
																	No information available
																</p>
															<?php endif;?>
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
							<?php endif;?>
						</div>
					</div>
				</div>
	      	</div>
	      	<div class="container">
		      	<fieldset class="m-fieldset my-4">
					<legend>Search Members</legend>
					<form>
						<div class="form-row align-items-center">
							<div class="col">
								<input type="text" class="form-control mb-2" id="member_search" placeholder="Enter Member Name">
							</div>
							<div class="col-auto search-button">
								<button type="submit" class="btn btn-primary mb-2 btn-search"><span class="fa fa-search"></span></button>
							</div>
						</div>
					</form>
				</fieldset>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>

<script type="text/javascript" src="/assets/js/typeahead.bundle.min.js"></script>
<script>
	$(function() {
    	var memberLoad = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('member_name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			limit: 20,
			remote: {
			    url: '/members/search.php?q=%QUERY',
			    wildcard: '%QUERY'
			}
		});

		$('#member_search').typeahead(null, {
			displayKey: 'member_name',
			valueKey: 'member_pathname',
			limit: 20,
			source: memberLoad.ttAdapter(),
			templates: {
			    suggestion: function(data) {
			        return '<p><strong>' + data.member_name + '</strong><span class="d-block  text-muted"> <small> ' + data.member_name + '</small></span> </p>';
			    }
			}
		}).on('typeahead:selected', function(e, data) {
			var link = '/members/'+data.member_pathname+'-'+data.id+'.html';
			var code ='<a href="'+link+'" class="btn btn-primary mb-2 btn-search"><span class="fa fa-search"></span></a>';
			$('.search-button').html(code);
		}).on('typeahead:autocompleted', function(e, data) {
			//$('.festivals-search-form').submit();
		}).after('<div class="input-bar"></div>');

		memberLoad.initialize();
    	

	});
</script>
</body>
</html>