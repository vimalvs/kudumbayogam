<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Padiyara Vallikattu Family Members</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Padiyara Vallikattu Family Members list..." />
<?php $this->render('theme::headContent');?>
<style>
.member-name{
	font-weight: bold;
}

.tc{
	text-align: center;
}
.member{
	padding: 5px;
}
.m-fieldset {
	padding: 10px;
	border:1px solid #e6e6e6;
}
.m-fieldset > legend {
	width: auto;
	padding: 6px;
	font-size: 17px;
	line-height: 17px;	
}
.twitter-typeahead {
    width: 100%;
    vertical-align: middle
}

.tt-dropdown-menu {
    text-align: left;
    width: 100%
}

.typeahead,
.tt-query,
.tt-hint {
    outline: none
}

.typeahead {
    background-color: #fff
}

.tt-query {
    -webkit-transition: background-color .4s ease 0s;
    transition: background-color .4s ease 0s
}

.tt-menu {
    background-color: #fff;
    border: 1px solid #f2f2f2;
    width: 100%
}

.tt-hint {
    color: #999
}

.tt-dropdown-menu {
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ddd;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .15);
    margin-top: 1px;
    color: #000
}

.tt-suggestion {
    padding: 3px;
    padding-bottom: 10px;
    border-bottom: solid #ccc 1px;
    margin: 0
}

.tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf
}

.tt-suggestion:hover {
    color: #fff;
    background-color: #0097cf;
    cursor: pointer
}

.drop-tt-empty-message {
    padding: 5px 10px;
    text-align: center;
    color: #fff;
    background: red
}

</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div class="container">
	      		<div class="text-center my-2">
			      	<h1> Who Is Who </h1>
			      	<hr class="h-underline">
			    </div>
		    	<fieldset class="m-fieldset my-4">
					<legend>Search Members</legend>
					<form>
						<div class="form-row align-items-center">
							<div class="col">
								<input type="text" class="form-control mb-2" id="member_search" placeholder="Enter Member Name">
							</div>
							<div class="col-auto search-button">
								<button type="submit" class="btn btn-primary mb-2 btn-search"><span class="fa fa-search"></span></button>
							</div>
						</div>
					</form>
				</fieldset>
			    <?php if($mode == 'home'): ?>
				    <?php foreach($arShortList as $family_id => $listDetails) : ?>
				    	<fieldset class="m-fieldset my-4">
							<legend>
								<span class="fa fa-home"></span> <?=$arFamilyList[$family_id]['family_name']?>
							</legend>
							<div class="row mb-2">
						    <!-- <ul class="list-unstyled"> -->
						    	<?php foreach($listDetails as $detail): ?>
					    			<!-- <li class="media my-2 p-2 border">
										<?php if(!empty($detail['image_pathname'])):?>
							    				<img class="mr-3" src="/members/assets/images/<?=$detail['image_pathname']?>" alt="">
							    		<?php else:?>
							    				<img class="mr-3" src="/assets/imgs/member.png" alt="Padiyara Sub Stream" width="100" height="100">
							    		<?php endif; ?>
										<div class="media-body">
											<h5 class="mt-0 mb-1">
												<span class="fa fa-edit"></span>
												<a href="/members/<?=$detail['member_pathname']?>-<?=$detail['id']?>.html">
													Name: <?=$detail['member_name']?>
												</a>
											</h5>
											<div class="">
								    			<div>
								    				<span class="fa fa-address-card fa-fw"></span> <?=$detail['address']?>
								    			</div>
								    			<div>
								    				<span class="fa fa-phone-square fa-fw"></span> <?=$detail['phone']?>
								    			</div>
								    			<div>
								    				<span class="fa fa-envelope fa-fw"></span> <?=$detail['email']?>
								    			</div>
											</div>
										</div>
						    		</li> -->

									<div class="col-md-6">
										<div class="card flex-md-row mb-4 box-shadow h-md-250">
											<div class="card-body d-flex flex-column align-items-start">
												<h5 class="mb-0">
													<table>
														<tbody>
															<tr>
																<td class="p-2" style="font-size: 28px;">
																	<span class="fa fa-user"></span> 
																</td>
																<td>
																	<a class="text-dark" href="/members/<?=$detail['member_pathname']?>-<?=$detail['id']?>.html">
																		<?=$detail['member_name']?>
																	</a>
																</td>
															</tr>
														</tbody>
													</table>
												</h5>
												<div class="mb-1 text-muted"><?=$detail['address']?></div>
											</div>
										</div>
									</div>

							    <?php endforeach; ?>
						    <!-- </ul> -->
							</div>
						    <div class="tc my-2">
						    	<a href="/members/<?=$arFamilyList[$family_id]['family_pathname']?>-<?=$family_id?>/">See More Members In <?=$arFamilyList[$family_id]['family_name']?></a>
						    </div>
						</fieldset>
					 <?php endforeach; ?>

				<?php elseif($mode == 'family'): ?>

					<?php if (!empty($arMember)):?>

						<fieldset class="m-fieldset my-4">
							<legend>
								<span class="fa fa-home"></span> <?=$familyDetails['family_name']?> Family Members
							</legend>
							<div class="row mb-2">
								<?php foreach($arMember as $member): ?>
									<div class="col-md-6">
										<div class="card flex-md-row mb-4 box-shadow h-md-250">
											<div class="card-body d-flex flex-column align-items-start">
												<h5 class="mb-0">
													<table>
														<tbody>
															<tr>
																<td class="p-2" style="font-size: 28px;">
																	<span class="fa fa-user"></span> 
																</td>
																<td>
																	<a class="text-dark" href="/members/<?=$member['member_pathname']?>-<?=$member['id']?>.html">
																		 <?=$member['member_name']?>
																	</a>
																</td>
															</tr>
														</tbody>
													</table>
												</h5>
												<div class="mb-1 text-muted">
													<?=$member['address']?>
													<br>
													<?=$member['email']?>
												</div>
											</div>
										</div>
									</div>
							    <?php endforeach; ?>
							</div>
						</fieldset>
					<?php endif;?>
				<?php endif;?>
	      	</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>

<script type="text/javascript" src="/assets/js/typeahead.bundle.min.js"></script>
<script>
	$(function() {
    	var memberLoad = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('member_name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			limit: 20,
			remote: {
			    url: '/members/search.php?q=%QUERY',
			    wildcard: '%QUERY'
			}
		});

		$('#member_search').typeahead(null, {
			displayKey: 'member_name',
			valueKey: 'member_pathname',
			limit: 20,
			source: memberLoad.ttAdapter(),
			templates: {
			    suggestion: function(data) {
			        return '<p><strong>' + data.member_name + '</strong><span class="d-block  text-muted"> <small> ' + data.family_name + '</small></span> </p>';
			    }
			}
		}).on('typeahead:selected', function(e, data) {
			var link = '/members/'+data.member_pathname+'-'+data.id+'.html';
			var code ='<a href="'+link+'" class="btn btn-primary mb-2 btn-search"><span class="fa fa-search"></span></a>';
			$('.search-button').html(code);
		}).on('typeahead:autocompleted', function(e, data) {
			//$('.festivals-search-form').submit();
		}).after('<div class="input-bar"></div>');

		memberLoad.initialize();
    	

	});
</script>
</body>
</html>