<?php
include '../include/prepend.php';

$pdbo = \SiteManager::getDataBase();

extractCleanVars('q');

$query = $pdbo->quote("%{$q}%");
$stmt = $pdbo->query("SELECT m.id, m.member_name, m.member_pathname, f.family_name  FROM members m INNER JOIN family f ON (m.family_id = f.id)  WHERE m.member_name LIKE $query");

$arMember = $stmt ? $stmt->fetchAll() : [];

echo json_encode($arMember);

exit;