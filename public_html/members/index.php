<?php
include '../include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();
$pdbo = \SiteManager::getDataBase();

$mode = getCleanVar('mode', 'home');
extractCleanVars('family_name', 'id');

if($mode == 'home'){

	$tpl->setTemplate('home');

	$arFamilyList = $pdbo->search('members', false, false, null, 'family_id', 'family_id')->fetchAll();
	$arShortList = [];
	foreach($arFamilyList as $family_id){
		$arMembers  = $pdbo->search('members', ['family_id' => $family_id['family_id']], ['id', 'ASC'], 4)->fetchAll();
		$arShortList[$family_id['family_id']] = $arMembers ;
	}
	$arFamily = $pdbo->search('family')->fetchAll();
	$arFamilyList = [];
	foreach($arFamily as $family){
		$arFamilyList[$family['id']] = ['family_name' => $family['family_name'], 'family_pathname' => $family['family_pathname']];
	}
	$tpl->addData(compact('arFamilyList', 'arShortList'));
} elseif($mode == 'family') {
	$tpl->setTemplate('home');
	$familyDetails = $pdbo->getRecord('family', ['id' => $id]);
	if($familyDetails){
		$arMember = $pdbo->search('members', ['family_id' => $familyDetails['id']])->fetchAll();
		$tpl->addData(compact('familyDetails', 'arMember'));
	}
	
} elseif ($mode === 'member') {
	$tpl->setTemplate('member');
	extractCleanVars('member_name', 'id');
	$memberDetails = $pdbo->getRecord('members', ['id' => $id]);
	$parentDetails = $pdbo->getRecord('members', ['member_uid' => $memberDetails['parent_id'], 'family_id' => $memberDetails['family_id']]);
	
	if($memberDetails){
		$family = $pdbo->search('family', ['id' => $memberDetails['family_id']])->fetch();
		$tpl->addData(compact('parentDetails')+$memberDetails+$family);
	}
}

$tpl->addData(compact('mode'));

$tpl->generate();
