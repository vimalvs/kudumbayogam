<?php
define('SECTION_NAME', 'Committee-members');
define('SECTION_PATHNAME', '');
include 'include/prepend.php';
$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('committee-members');

$page = \Page\PageManager::loadById(4);
$pageData = $page->getData();

$page_content = ($lang === 'en') ? $pageData['page_content_en'] : $pageData['page_content_ml'];

$tpl->addData($pageData + compact('page_content'));
$tpl->generate();
