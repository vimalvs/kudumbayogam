<?php
unset($this->head['meta'], $this->head['style']);
$data_encoded = urlencode($data);
$data_escaped = htmlspecialchars($data);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><?=$title?></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<script  type="text/javascript" src="/assets/js/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/share.css"/>
<script type="text/javascript" src="/share/js/share.js"></script>
</head>

<body>
<div id="wrapper">
<div id="innerWrapper">
<div id="header"><?=$title?></div>
<div id="step">Step (<?=$step?> / <?=$steps?>)</div>
<div id="body">
<div id="content">
<div class="lnks"><a href="?mode=import&amp;data=<?=$data_encoded?>" class="insertContactsLink">Import Address Book</a> | <a href="?mode=manual&amp;data=<?=$data_encoded?>" class="insertContactsLink">Enter e-mails manually</a></div>
<?php $this->printMessage();?>
<?php if($this->messages->status === 'exit'):?>
	<!-- Do Nothing -->
<?php elseif($mode == 'manual'): ?>
	<form method="post" action="/share/">
        <!--table class="tblForm" cellspacing="0">
        <tr class="title"><td>&nbsp;</td><td style="width:190px;">Your Name</td><td style="width:190px;">Your Email</td></tr>
        <tr><td>&nbsp;</td><td><input type="text" class="inputField name" name="fromName" value="<?php //$fromName;?>"/></td><td><input class="inputField email" type="text" name="fromEmail" value="<?php //$fromEmail;?>" /></td></tr>
        </table-->
        <table id="tblFriends" class="tblForm" cellspacing="0">
        <tr class="title"><td>&nbsp;</td><td style="width:190px;">Friends Name</td><td style="width:190px;">Friends Email</td></tr>
        <?php foreach($friendName as $idx => $name):?>
            <tr><td>#<?=($idx+1)?></td><td><input type="text" class="inputField name" name="friendName[<?=$idx?>]" value="<?=$name?>"/></td><td><input class="inputField email" type="text" name="friendEmail[<?=$idx?>]" value="<?=$friendEmail[$idx]?>" /></td></tr>
        <?php endforeach;?>
        <tr><td colspan="3">
        	<input type="hidden" name="data" value="<?=$data_escaped?>" />
            <input type="hidden" name="mode" value="manual" />
            <input type="hidden" name="p" value="1" />
            <input class="button" type="submit" value="Continue" /></td></tr>
        </table>
        <p><a id="btnAddEmail" class="addQuestion" href="#">Add Another Friend</a></p>
    </form>
<?php elseif($mode == 'import'): ?>
	<form method="post" action="/share/">
    	<p>Enter your username and password to import contacts from your address book.</p>
    	<table class="tblForm" style="width:265px;" cellspacing="0">
        <tr class="title"><td colspan="2">Import Address Book</td></tr>
        <tr><td>Email</td><td><input type="text" name="userEmail"  value="<?= $userEmail;  ?>" class="inputField" /></td></tr>
        <tr><td>Password</td><td><input type="password" name="userPassword" class="inputField" /></td></tr>
        <tr><td>Provider</td><td><select name="provider" class="inputField">
        <?php
		foreach ($oi_services as $type => $providers):
			foreach ($providers as $service=>$details):
				echo "<option value='$service' ", (($service === $provider) ? 'selected="selected"' : ''), ">{$details['name']}</option>";
			endforeach;
		endforeach;
		?>
        </select></td></tr>
        <tr><td colspan="2"><input type="hidden" name="data" value="<?=$data_escaped?>" />
        	<input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="1" />
            <input type="submit" value="Get Contacts" class="button" /></td></tr>
        </table>
    	<p class="info">We will not store your username and/or password on our server for any purpose. The provided details will only be used for importing your contacts. Click <a target="_blank" href="/privacy.htm">here</a> to view our <a target="_blank" href="/privacy.htm">privacy policy</a>.</p>
    </form>
<?php elseif($mode == 'select'): ?>
	<form method="post" action="/share/">
        <table class="tblForm" cellspacing="0">
    	<tr class="title"><td id="checkAll"><input type='checkbox' /></td><td>Imported Contacts</td></tr>
    	<tr class="odd"><td colspan="2"><input class="inputField comment" id="addrFilter" value="Search for friend by entering name/email address" /></td></tr>
		<?php 
		$i = 0;
		foreach($contacts as $mailId => $name):?>
            <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td><input type='checkbox' id='arSelContacts[<?=$mailId?>]' name='arSelContacts[<?=$mailId?>]' value="<?=$name?>" /></td><td><label for="arSelContacts[<?=$mailId?>]"><span class="name"><?=$name?></span>, &lt; <span class="email"><?=$mailId?></span> &gt;</label></td></tr>
        <?php endforeach; ?>
        <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td colspan="2"><input type="hidden" name="data" value="<?=$data_escaped?>" /><input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="2" /><input class="button" type="submit" value="Add Selected Contacts" /></td></tr>
        </table>
    </form>
<?php elseif($mode == 'send'): 
	$verified = isset($verified) ? $verified : '';
?>
	<form id="frmSend" method="post" action="/share/">
        <div class="cb scroll" style="height:50px;"><b>To : </b><p class="pad"><?= htmlentities($toEmailString); ?></p></div>
        <?php if($edit == 'allow'): ?>
            <div style="font:normal 12px Arial, Helvetica, sans-serif;margin:5px;">Enter Your Message Below<br/><textarea name="message" rows="5" cols="45"><?= $message; ?></textarea></div>
        <?php else: ?>
            <div class="cb scroll" style="height:120px;"><b>Message : </b><div class="pad"><?=$message?></div></div>
        <?php endif; ?>
        <table class="tblForm" cellspacing="0">
        <tr class="title"><td>&nbsp;</td><td style="width:190px;">Your Name</td><td style="width:190px;">Your Email</td></tr>
        <tr><td>&nbsp;</td><td><input type="text" class="inputField name" name="fromName" value="<?=$fromName?>"/></td><td><input class="inputField email" type="text" name="fromEmail" value="<?=$fromEmail?>" /></td></tr>
        <tr><td colspan="3">     
			<?php if(!$verified): ?><?= recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, ($verified === '') ? '' : 'incorrect-captcha-sol' ); ?>
            <?php else: ?><input type="hidden" name="recaptcha_challenge_field" value="<?=$recaptcha_challenge_field?>" /><input type="hidden" name="recaptcha_response_field" value="<?=$recaptcha_response_field?>" />
            <?php endif; ?>
        </td></tr>
        <tr><td colspan="3"><input type="hidden" name="data" value="<?=$data_escaped?>" /><input type="hidden" name="mode" value="<?=$mode?>" /><input type="hidden" name="p" value="1" /><input class="button" type="submit" value="Send Mail" /></td></tr>
        </table>
        <div style="text-align:center;margin-top:5px;"></div>
    </form>
<?php endif; ?>
<p class="info">We respect your privacy and will not save your or your friend's email address.</p>
</div>
</div>
</div>
</div>
</body>
</html>