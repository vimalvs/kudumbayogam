<form class="nyroModal frmStatus" method="post" action="/members/member.php">
<?php if($this->data['mode'] === 'welcome'):?>
	<div id="loggedInContinue">
	<p>
		You are now logged in as <?=$this->data['name']?><br/>
		Email : <span class="hilite"><?=$this->data['email']?></span><br/>
		<input id="loginModalClose" class="button nyroModalClose" type="button" value="Continue" />
		<a class="nyroModal meta" href="/members/member.php?mode=logout&amp;ajax=1">Are you not <?=$this->data['name']?>? Click here to re-login.</a>
		<script type="text/javascript">
		$('#loginModalClose').click(function(){$('.loginDelaySubmit').removeClass('loginDelaySubmit').unbind().submit();});
		<?php if(!empty($this->config['autoclose'])):?>
			$('#loginModalClose').click();		
		<?php endif;?>
		</script>
	</p>
	</div>
<?php elseif($this->data['mode'] === 'register'):?>
    <table cellspacing="0">
    <tr><td class="title" colspan="2">Quick Signup</td></tr>
    <tr><td colspan="2" class="nError"><?php $this->printMessage();?></td></tr>
    <tr><td>Your Name</td><td><input type="text" name="name" value="<?=$this->data['form']['name']?>"/></td></tr>
    <tr><td>Email</td><td><input type="text" name="email" value="<?=$this->data['form']['email']?>"/></td></tr>
    <tr><td>Password</td><td><input type="password" name="password" /></td></tr>
    <tr><td>Confirm</td><td><input type="password" name="confirm" /></td></tr>
    <tr><td colspan="2"><input class="button" type="submit" value="Register"/>
    <input type="hidden" name="p" value="1"/><input type="hidden" name="ajax" value="1"/><input type="hidden" name="mode" value="register"/>
    </td></tr>
    <tr><td colspan="2" class="meta"><a class="nyroModal" href="/members/member.php?mode=login&amp;ajax=1">Already registered? Login</a> | <a class="nyroModal" href="/members/member.php?mode=sendpw&amp;ajax=1">Resend Password</a></td></tr>
    </table>
<?php elseif($this->data['mode'] === 'register_success'):?>
    <p class="success" style="text-align:center;">You have successfully registered. <br/><br/><br/>
    <input type="button" class="nyroModalClose button" value="Continue"/>
    <script type="text/javascript">$('.nyroModalClose').click(function(){$('#frmFriend').unbind().submit();});</script>
    </p>
<?php elseif($this->data['mode'] === 'sendpw'):?>
    <table cellspacing="0">
    <tr><td class="title">Email Password</td></tr>
        <tr><td colspan="2" class="nError"><?php $this->printMessage();?></td></tr>
    <tr><td>Enter your email address<br/><input type="text" name="email" value="<?=$this->data['form']['email']?>"/></td></tr>
    <tr><td><input class="button" type="submit" value="Continue"/>
    <input type="hidden" name="p" value="1"/><input type="hidden" name="ajax" value="1"/><input type="hidden" name="mode" value="sendpw"/>
    </td></tr>
    <tr><td class="meta"><a class="nyroModal" href="/members/member.php?mode=login&amp;ajax=1">Login</a> | <a class="nyroModal" href="/members/member.php?mode=register&amp;ajax=1">Register</a></td></tr>
    </table>
<?php else:?>
        <table cellspacing="0">
        <tr><td class="title" colspan="2">Login</td></tr>
        <tr><td colspan="2" class="nError"><?php $this->printMessage();?></td></tr>
        <tr><td>Email</td><td><input type="text" name="email" value="<?=$this->data['form']['email']?>"/></td></tr>
        <tr><td>Password</td><td><input type="password" name="password" /></td></tr>
        <tr><td colspan="2"><label><input class="chkInput" name="autologin" type="checkbox" value="1" <?=(!empty($this->data['form']['autologin']) ? 'checked="checked"' : '')?>/>
        </input>
        Remember me on this computer</label></td></tr>
        <tr><td colspan="2"><input class="button" type="submit" value="Login"/>
        <input type="hidden" name="p" value="1"/><input type="hidden" name="ajax" value="1"/>
        <input type="hidden" name="mode" value="login"/>
        </td></tr>
        <tr><td colspan="2" class="meta"><a class="nyroModal" href="/members/member.php?mode=register&amp;ajax=1">Quick Signup</a> | <a class="nyroModal" href="/members/member.php?mode=sendpw&amp;email=<?=$this->data['form']['email']?>&amp;ajax=1">Resend Password</a></td></tr>
        </table>
<?php endif;?>
</form>