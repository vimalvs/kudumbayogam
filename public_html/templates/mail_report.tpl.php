<div style="background:#F2F2F2;text-align:center;padding:20px;margin:0 auto;">
	<table cellpadding="0" cellspacing="0" style="width:100%;margin:0 auto;border-collapse:collapse;text-align:left;">
		<tr><td style="padding:10px;border:none;">
			<table cellpadding="0" cellspacing="0" style="margin:0 auto;background:#FFF;width:100%;font:12px Arial,Verdana,sans-serif;color:#000;">
			<tr><td style="width:100%;"><span style="display:block;color:#FFF;background:#0066CC;font:bold 14px/20px Verdana,Arial,sans-serif;padding:5px 10px;"><?=SITE_NAME?></span></td></tr>
			<tr><td style="padding:5px;width:100%;border:1px solid #CCC;border-top:none;">
				<div style="margin:0 auto;padding:10px 5px;">
					<p style="margin:0 0 10px;font:12px/20px Verdana,Arial,sans-serif;">
						Dear Webmaster,<br/><br/>
						The following alert from <?=SITE_NAME?> requires your attention.
					</p>
					<p style="font-style:italic;"><?=$description?></p>
					<div style="padding:10px 5px;margin:10px 20px;font:13px/1.5em 'Courier New', monotype;background:#EFFEFF;border:1px solid #82B2FF;">
						<?php if (is_array($message)):?>
							<?php $len = 0; foreach(array_keys($message) as $k) $len = max($len, strlen($k))?>
							<?php foreach($message as $k => $v):?>
								<div style="clear:left;font-weight:bold;width:20%;max-width:200px;float:left;text-align:right;"><?=ucwords(str_replace(array('-', '_'), ' ', $k))?><?=str_repeat('&nbsp;', $len - strlen($k) + 1)?>: &nbsp;</div>
								<div style="width:80%;float:left;"><?=$v?>&nbsp;</div>
							<?php endforeach;?>
							<div style="clear:both;"></div>
						<?php else:?>
							<p><?=$message?></p>
						<?php endif;?>
					</div>
					<?php if (isset($actions)):?>
					<div style="border:1px solid #FFCC33;padding:5px;background:#FFF4C2;;">
						<span style="font:bold 11px/1.5em Verdana,Arial,sans-serif;color:#333;display:block;">What would you like to do:</span>
						<?php foreach($actions as $k => $v):?>
							&raquo; <a href="<?=htmlspecialchars(strpos($v, '://') ? $v : ('http://www.' . SITE_DOMAIN . $v))?>" style="text-decoration:none;font:12px/2em Verdana,sans-serif;color:#0066CC;"><?=ucwords($k)?></a>
						<?php endforeach;?>
					</div>
					<?php endif;?>
					<div style="padding:5px 0 0;text-align:right;font-size:10px;font-style:italic;color:#666;">Message created on <?=date(DATE_RFC822, TIME_NOW)?><?php if (!empty($script)) echo " in <a href='" . htmlspecialchars($url) . "'>$script</a>";?>.</div>
				</div>
			</td></tr>
			</table>
		</td></tr>
	</table>
</div>