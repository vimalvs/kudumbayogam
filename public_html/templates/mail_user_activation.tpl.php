Hello <?=$name?>!

Thank you for joining MalluBar.com! You are just one step away from being a full fledged member of our site.
Please keep this email for your records. Your account information is as follows:
-------------------------------
Username : <?=$username?>

Password : [Hidden]
-------------------------------
Your account is now inactive. To activate your account, please visit the following url
http://www.mallubar.com/members/activation.php?code=<?=$code?>&username=<?=$username?>&p=1
If the web address above is not a clickable link, copy and paste the full address into your browser.

Alternatively, you may also visit http://www.mallubar.com/members/activation.php and enter the following code
-------------------------------
Username : <?=$username?>

Activation Code : <?=$code?>

-------------------------------
If you need any assistance, feel free to contact us at contact@mallubar.com.

Thanks again!
~MalluBar Team