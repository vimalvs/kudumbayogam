<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="gNR8fjaFTguF-fVDgvWH8XEJNoSMzLAO3i_XYRhoxuQ" />
<title>Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu, Padiyara Vallikattu genealogy" />
<meta name="description" content="Complete information about the Padiyara Vallikattu Family of Kottayam. Profile of Padiyara Vallikattu Family Members, Padiyara Vallikattu Family Genealogy and Family tree, details about Padiyara Vallikattu Kudumbayogam..." />
<?php $this->render('theme::headContent');?>
<link rel="stylesheet" href="assets/css/lightslider.css" />
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div id="home_carousel" class="carousel slide" data-ride="carousel">
			    <div class="carousel-inner">
			    	<?php foreach($arImage as $key => $image):?>
				        <div class="carousel-item <?=($key === 0) ? 'active':''?>">
				            <img class="d-block w-100" src="<?=$image['image_path']?>">
				        </div>
			        <?php endforeach;?>
			    </div>
			    <a class="carousel-control-prev" href="#home_carousel" role="button" data-slide="prev">
			        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			        <span class="sr-only">Previous</span>
			    </a>
			    <a class="carousel-control-next" href="#home_carousel" role="button" data-slide="next">
			        <span class="carousel-control-next-icon" aria-hidden="true"></span>
			        <span class="sr-only">Next</span>
			    </a>
			</div>
			<div class="bg-blue">
				<div class="container bg-blue p-3 fg-white">
					<div class="text-center">
						<span class="t-xlarge">Introduction To</span>	
						<h1>PADIYARA VALLIKATTU FAMILY</h1>
						<hr class="h-underline">
					</div>
					<p>St.Marthoma was separated from the Holy Spirit from Christianity. Because of the adverse conditions and adverse effects of Travancore retreats, which retained the status of priestly status until the third century AD
					</p>
					<p>
					Three sisters and their mother from Mahapili family at Chengannur Thalassery mosque had migrated to Central Travancore. The history of the historic event known as the Mappila Padampara Padamparai in 1700</p>
					<p class="text-right"><a class="fg-white" href="/aboutus.php">read more.. &raquo;</a></p>
				</div>
			</div>

			<div class="container p-2">
				<div class="p-2">
					<h2 class="text-center">WHO IS WHO ?</h2>
					<hr class="h-underline">
					<p>
						At Family Search, we preserve memories so they can be shared for generations to come.
					</p>
					<ul class="nav-rounded-list grid-col grid-col-xs-12 grid-col-sm-6 grid-col-lg-3 text-center">
						
						<?php foreach($arMember as $key => $value):?>

							<li class="list-item">
								<a href="/members/<?=$value['member_pathname']?>-<?=$value['id']?>.html">
									<?php if(!empty($value['image_pathname'])):?>
										<img class="card-img-top" src="/members/assets/images/<?=$value['family_id']?>/<?=$value['image_pathname']?>" alt="<?=$value['member_name']?>">
									<?php else:?>
										<img class="img-fluid rounded-circle" src="/assets/imgs/member.png" alt="<?=$spouse?>">
									<?php endif;?>
						  			
						  			<span  class="caption"><strong><?=$value['member_name']?></strong><br><?=$value['family_name']?></span>
						  		</a>
							</li>

			            <?php endforeach;?>

					</ul>
					<span class="d-block p-3 text-center"><a class="btn btn-info" href="/members/">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
				</div>
			</div>
			<!-- <div class="bg-blue">
				<div class="container bg-blue p-3 fg-white">
					<div class="text-center">
						<h2>GALLERY</h2>
						<hr class="h-underline">
					</div>
					<?php //if(!empty($arGalleryImage)):?>
						<p>
						Many of the most precious, heart-changing family photos and stories are tucked away in our homes and our minds. At FamilySearch, we preserve memories so they can be shared for generations to come.	
						</p>
						<ul id="content-slider" class="content-slider">
							<?php //foreach($arGalleryImage as $key => $value):?>
				                <li>
				                	<a href="/gallery/<?php //$value['category_pathname'] ?>-<?php //$value['gallery_id']?>.html">
				                    <img src="<?php //$value['thumbnail']?>" alt="<?php //$value['caption']?>" width="100%" height="100%" class=""></a>
				                </li>
			               <?php //endforeach;?>
			            </ul>
						<span class="d-block p-3 text-center"><a class="btn btn-info" href="/gallery/">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
					<?php //endif;?>
				</div>
			</div> -->
			<div class="container p-2">
				<div class="p-2">
					
					<h2 class="text-center">NEWS AND ANNOUNSMENT </h2>
					<hr class="h-underline">
					<?php if (!empty($arEvent)):?>
						<div class="grid-col grid-col-xs-12 grid-col-sm-6 grid-col-lg-4 ">
							<?php foreach($arEvent as $event):?>
								<div class="p-2">
									<div class="card rounded p-2">
										<h5 class="mt-0 mb-1">
											<span class="fa fa-edit"></span>
											<a href="<?=$event['event_pathname']?>-<?=$event['event_id']?>.html">
												<?=$event['event_title']?>
											</a>
										</h5>
										<p class="card-text">
											<?=$event['event_summery']?>
										</p>
									</div>
								</div>
							<?php endforeach;?>
						</div>
						<span class="d-block p-3 text-center"><a class="btn btn-info" href="/newsletter/">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
					<?php endif;?>
				</div>
				<marquee class="font-italic font-weight-light text-muted" BEHAVIOR=ALTERNATE>This page has been viewed <?=$site_view?> times. (Total <?=$page_view?> user visits.)</marquee>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>


	 <script src="/assets/js/lightslider.js"></script>
	<script>
		$(document).ready(function() {
			$("#content-slider").lightSlider({
                item:5,
		        auto: true,
        		loop: true,
		        slideMove:2,
		        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
		        speed:600,
		        responsive : [
		            {
		                breakpoint:800,
		                settings: {
		                    item:3,
		                    slideMove:1,
		                    slideMargin:6,
		                  }
		            },
		            {
		                breakpoint:480,
		                settings: {
		                    item:2,
		                    slideMove:1
		                  }
		            },
		            {
		                breakpoint:320,
		                settings: {
		                    item:1,
		                    slideMove:1
		                  }
		            }
		        ]
			});
		});
	</script>
</div>
</body>
</html>