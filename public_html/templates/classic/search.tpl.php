<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="gNR8fjaFTguF-fVDgvWH8XEJNoSMzLAO3i_XYRhoxuQ" />
<title>Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu" />
<meta name="description" content="Padiyara Vallikattu kudumpayogam" />
<?php $this->render('theme::headContent');?>
<style>
.search-result{
	list-style: none;
	width: 80%;
	padding: 0;
}
.data {
	background-color: #8080802b;
	padding: 10px;
	border-radius: 10px;
	margin-bottom: 10px;
}
.search {
  width: 100%;
  position: relative
}

.searchTerm {
  width: 100%;
  border: 3px solid #00B4CC;
  padding: 5px;
  height: 36px;
  border-radius: 5px;
  outline: none;
  color: #9DBFAF;
}

.searchTerm:focus{
  color: #00B4CC;
}

.searchButton {
  position: absolute;  
  right: -50px;
  top: 0;
  width: 40px;
  height: 36px;
  border: 1px solid #00B4CC;
  background: #00B4CC;
  text-align: center;
  color: #fff;
  border-radius: 5px;
  cursor: pointer;
  font-size: 20px;
}

</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="text-center">
						<h1>Site Search <?= (isset($q))? '- '.$q : '' ?></h1>
						<hr class="h-underline">
					</div>
				
					<form class="p-3" method="get" action="/search.php">
						<div class="form-group">
							<div class="input-group mb-3">
								<input type="text" class="form-control" name="q" placeholder="Search">
								<div class="input-group-append">
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-search"></i>
									</button>
								</div>
							</div>
						</div>
					</form>
					<div class="p-4">
						<?php if(!empty($arSearchData)): ?>
						<h4>Search Results</h4>
							<div class="list-group">
								<?php foreach($arSearchData as $searchData) : ?>
									<a href="<?=$searchData['pathname']?>" class="list-group-item list-group-item-action flex-column align-items-start">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">To see more click here</h5>
										</div>
										<p class="mb-1"><?=$searchData['title']?></p>
									</a>
								<?php endforeach; ?>
							</div>
						<?php else: ?>
							<span class="data">No Result Found</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>