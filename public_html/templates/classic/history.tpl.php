<!DOCTYPE html>
<html lang="en">
<head>
<title>History of Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu, Padiyara Vallikattu genealogy" />
<meta name="description" content="Complete information about the Padiyara Vallikattu Family of Kottayam. Profile of Padiyara Vallikattu Family Members, Padiyara Vallikattu Family Genealogy and Family tree, details about Padiyara Vallikattu Kudumbayogam..." />
<?php $this->render('theme::headContent');?>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="text-center">
						<span class="t-xlarge">History Of</span>	
						<h1>PADIYARA VALLIKATTU FAMILY</h1>
						<hr class="h-underline">
					</div>
					<?=$page_content?>	
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>