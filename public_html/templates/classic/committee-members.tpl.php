<!DOCTYPE html>
<html lang="en">
<head>
<title>Committee members</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu kudumbayogam Committee members" />
<meta name="description" content="Padiyara Vallikattu kudumbayogam Committee members" />
<?php $this->render('theme::headContent');?>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="text-center">
						<span class="t-xlarge">Padiyara Vallikattu kudumbayogam</span>	
						<h1>EXECUTIVE COMMITTEE</h1>
						<hr class="h-underline">
					</div>
					<p>
						<?=$page_content?>	
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>