<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="gNR8fjaFTguF-fVDgvWH8XEJNoSMzLAO3i_XYRhoxuQ" />
<title>Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu" />
<meta name="description" content="Padiyara Vallikattu udumpaogam sitemap" />
<?php $this->render('theme::headContent');?>
<style>

	h1 {
		font-weight: bold;
		text-transform: uppercase;
		font-size: 20px;
		margin: 0 0 5px 0;
	}
	h2 {
		font-family: "Lucida Grande", Verdana, sans-serif;
		font-size: 15px;
		font-weight: bold;
		color: #777777;
		margin-bottom: 33px;
	}

	.sitemap {
		margin: 0 0 40px 0;
		float: left;
		min-width: 300px;
		width: 100%;
	    padding: 40px;

	}
@media only screen and (min-width: 680px) {

	#primaryNav li {
		width:25%;
	}

	#primaryNav li ul li {
		width:100% !important;
	}

	#primaryNav.col1 li { width:99.9%; }
	#primaryNav.col2 li { width:50.0%; }
	#primaryNav.col3 li { width:33.3%; }
	#primaryNav.col4 li { width:25.0%; }
	#primaryNav.col5 li { width:20.0%; }
	#primaryNav.col6 li { width:16.6%; }
	#primaryNav.col7 li { width:14.2%; }
	#primaryNav.col8 li { width:12.5%; }
	#primaryNav.col9 li { width:11.1%; }
	#primaryNav.col10 li { width:10.0%; }

	/* ------------------------------------------------------------
		General Styles
	------------------------------------------------------------ */

	body {
		background: white;
		color: black;
		font-family: Gotham, Helvetica, Arial, sans-serif;
		font-size: 12px;
		line-height: 1;
	}
	

	a {
		text-decoration: none;
	}
	ol, ul {
		list-style: none;
	}


	/* ------------------------------------------------------------
		Site Map Styles
	------------------------------------------------------------ */

	#primaryNav {
		margin: 0;
		float: left;
		width: 100%;
	}
	#primaryNav #home {
		display: block;
		float: none;
		background: #ffffff url('/assets/imgs/L1-left.png') center bottom no-repeat;
		position: relative;
		z-index: 2;
		padding: 0 0 30px 0;
	}
	#primaryNav li {
		float: left;
		background: url('/assets/imgs/L1-center.png') center top no-repeat;
		padding: 30px 0;
		margin-top: -30px;
	}
	#primaryNav li a {
		margin: 0 20px 0 0;
		padding: 10px 0;
		display: block;
		font-size: 14px;
		font-weight: bold;
		text-align: center;
		color: black;	
		background: #c3eafb url('/assets/imgs/white-highlight.png') top left repeat-x;
		border: 2px solid #b5d9ea;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		-webkit-box-shadow: rgba(0,0,0,0.5) 2px 2px 2px; 
		-moz-box-shadow: rgba(0,0,0,0.5) 2px 2px 2px; /* FF 3.5+ */	
	}
	#primaryNav li a:hover {
		background-color: #e2f4fd;
		border-color: #97bdcf;
	}
	#primaryNav li:last-child {
		background: url('/assets/imgs/L1-right.png') center top no-repeat;
	}

	#primaryNav li a:link:before,
	#primaryNav li a:visited:before {
		color: #78a9c0;
	}

	/* --------	Second Level --------- */

	#primaryNav li li {
		width: 100%;
		clear: left;
		margin-top: 0;
		padding: 10px 0 0 0;
		background: url('/assets/imgs/vertical-line.png') center bottom repeat-y;
	}
	#primaryNav li li a {
		background-color: #cee3ac;
		border-color: #b8da83;
	}
	#primaryNav li li a:hover {
		border-color: #94b75f;
		background-color: #e7f1d7;
	}
	#primaryNav li li:first-child {
		padding-top: 30px;
	}
	#primaryNav li li:last-child {
		background: url('/assets/imgs/vertical-line.png') center bottom repeat-y;
	}
	#primaryNav li li a:link:before,
	#primaryNav li li a:visited:before {
		color: #8faf5c;
	}
}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="sitemap">
				<h1>Padiyaravallikattu Kudumbayogam</h1>
				<h2>Site Map</h2><br>
				<ul id="primaryNav" class="col4">
					<li id="home"><a href="http://www.padiyaravallikattu-kudumbayogam.com">Home</a></li>
					<li><a href="/aboutus.php">About Us</a>
						<ul>
							<li><a href="/history.php">Our History</a></li>
							<li><a href="#">Who Is Who</a></li>
							<li><a href="#">Family Genealogy</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</li>
					<li><a href="#">News &amp; Announcements</a></li>
					<li><a href="#">Tributes</a>	
					<li><a href="#">Memorial Pages</a></li>
					<li><a href="#">Photo Gallery</a></li>
					<li><a href="#">Committee Members</a></li>		
					
				</ul>
			</div>
	    </div>	
	</div>
</div>
</body>
</html>