<?php
define('SECTION_NAME', 'Home');
define('SECTION_PATHNAME', '');
include 'include/prepend.php';
$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('aboutus');

$page = \Page\PageManager::loadById(2);
$pageData = $page->getData();

$page_content = ($lang === 'en') ? $pageData['page_content_en'] : $pageData['page_content_ml'];

$tpl->addData($pageData + compact('page_content'));
$tpl->generate();
