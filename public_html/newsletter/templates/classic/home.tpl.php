<!DOCTYPE html>
<html lang="en">
<head>
<title>News Letter &amp; Announcement</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu kudumbayogam News Letter &amp; Announcement" />
<meta name="description" content="Padiyara Vallikattu kudumbayogam News Letter &amp; Announcement" />
<?php $this->render('theme::headContent');?>
<style>
.sub-message h3{
	text-decoration: underline;
}
.sub-message p{
	font-size: 19px;
}
.hide{
	display: none;
}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="text-center">
						<span class="t-xlarge">Padiyara Vallikattu kudumbayogam</span>	
						<h1>NEWSLETTERS SUBSCRIPTION &amp; ALERTS</h1>
						<hr class="h-underline">
					</div>
					<p>
						<?=$page_content?>	
					</p>
					<div class="alert alert-info sub-message hide">

					</div>
					<div class="card">
						<div class="card-body">
							<form>
								<div class="form-row align-items-center">
									<div class="col">
										<input type="text" class="form-control mb-2 mail-subscribe" id="inlineFormInput" placeholder="Enter your email ID">
									</div>
									<div class="col-auto">
										<button type="submit" class="btn btn-primary mb-2 btn-subscribe">SUBSCRIBE</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="container p-3">
					<div class="text-center">
						<h2>Newsletters</h2>
					</div>
					<div>
						<hr class="h-underline">
						<?php if(!empty($arEvent)):?>
							<ul class="list-unstyled">
								<?php foreach($arEvent as $key => $event):?>

									<li class="media my-2 p-2 card">
										
										<div class="media-body">
											<h5 class="mt-0 mb-1">
												<span class="fa fa-edit"></span>
												<a href="<?=$event['event_pathname']?>-<?=$event['event_id']?>.html">
													<?=$event['event_title']?>
												</a>
											</h5>
											<div class="">
												<?=$event['event_summery']?>
											</div>
										</div>
									</li>
								<?php endforeach;?>
							</ul>
						<?php else:?>
							<div class="alert alert-info">
								Currently there is no information to show.
							</div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>
<script>
$('.btn-subscribe').click(function(e){
	e.preventDefault();

	var email = $('.mail-subscribe').val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  	var valid = emailReg.test(email);
	if(!valid){
		alert("Please enter a valid email id");
	}else{
		$.ajax({
	  	  type: "POST",
		  url: "newsletter.php",
		  data: {email : email, mode : 'subscribe'},
		  success: function(data){
		  	if (data) {
		  		$('.sub-message').removeClass('hide');
				var code = "<h3>"+data['head']+"<h1><p>"+data['message']+"</h3>";
				$('.sub-message').html(code);
			} else {
				alert("Error to subscribe news");
			}
		  },
		  dataType: 'json'
		});
	}
	
});
</script>