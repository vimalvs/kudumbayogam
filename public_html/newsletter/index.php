<?php
define('SECTION_NAME', 'newsletter');
define('SECTION_PATHNAME', '');
include '../include/prepend.php';
$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();


$page = \Page\PageManager::loadById(5);

$pdbo = \SiteManager::getDataBase();

$mode = getCleanVar('mode', 'home');
// printr($mode);
// exit;
if($mode == 'home'){
	
	$tpl->setTemplate('home');

	$pageData = $page->getData();

	$page_content = ($lang === 'en') ? $pageData['page_content_en'] : $pageData['page_content_en'];
	$stmt = $pdbo->search("event");
	$arEvent = $stmt ? $stmt->fetchAll() : [];
	$tpl->addData(compact('arEvent'));

}elseif($mode == 'event'){
	extractCleanVars('event_id');
	$tpl->setTemplate('newsletter');

	$event = $pdbo->getRecord("event", ['event_id' => $event_id]);
	$tpl->addData(compact('event', 'event_id'));

}elseif($mode == 'subscribe'){
	if($email){
		$exist = $pdbo->getRecord('subscribe_list',['user_mail' => $email]);
		if($exist){
			$head = 'Info';
			$message = 'You have already subscribed with this email address before...';
		}else{
			$status = $pdbo->insertRecord('subscribe_list',['user_mail' => $email, 'time_joined' => TIME_NOW, 'is_unsubscribed' => 1]);
			if($status){
				$head = 'Success';
				$message = "You have successfully subscribed to the newsletter. The newsletter will be send to your e-mail address";
			}else{
				$head = 'Success';
				$message = "Sorry!! something went went wrong please try again later...";
			}
		}
		
		echo json_encode(array('message' => $message, 'head' => $head));   
		exit;
	}

}


$tpl->addData(compact('page_content', 'mode', 'action'));
$tpl->generate();
