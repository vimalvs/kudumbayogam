<?php
mb_internal_encoding("UTF-8");
date_default_timezone_set('Asia/Kolkata');
define('PREPENDED', 1);
define('TIME_NOW', time());

include __DIR__ . '/config.php';
include __DIR__ . '/common.php';
include ROOT . '/lib/SiteManager.php';
include ROOT . '/../composer/ifixit/statsd-php-client/src/iFixit/StatsD.php';
include ROOT . '/lib/Feedback/Stats.php';

SiteManager::addClassAlias('\\Query\\Condition', 'QC');
SiteManager::initialize();

// if (!isset($_GET['test'])) {
// 	\ResponseManager::redirect("/index.html");
// }

$arLang = array('en', 'ml');
$lang = null;
if(isset($_GET['lang']) && in_array($_GET['lang'], $arLang)) {
	$lang = $_GET['lang'];
} else if (!empty($_COOKIE['lang'])) {
	$lang = $_COOKIE['lang'];
} else {
	$lang = 'en';
}
if (empty($_COOKIE['lang']) || $_COOKIE['lang'] !== $lang) {
	setcookie('lang', $lang, TIME_NOW + 2592000, '/', COOKIE_DOMAIN, false, true);
	$_COOKIE['lang'] = $lang;
}
// update page views
$pdbo = \SiteManager::getDataBase();
$query = "INSERT INTO analytics (ip_address,page_view) VALUES (".$pdbo->quote($_SERVER['REMOTE_ADDR']).", 1) ON DUPLICATE KEY UPDATE page_view = page_view + 1";
$pdbo->exec($query);

$stmt = $pdbo->query("SELECT COUNT(a.page_view) as view_count FROM analytics a");
$page_view = $stmt ? $stmt->fetchColumn(0) : 1;
$stmt = $pdbo->query("SELECT SUM(a.page_view) as site_view FROM analytics a ;");
$site_view = $stmt ? $stmt->fetchColumn(0) : 1;

class_alias('\\Theme\\Theme', 'Theme');
	
if (PHP_SAPI !== 'cli') {
	RequestManager::initialize(SITE_HOME);
	$isMobileEdition = !empty($_COOKIE['ua_type']) && in_array($_COOKIE['ua_type'], ['MA', 'MB']);
	define('MOBILE_EDITION', $isMobileEdition);
	define('AJAX_REQUEST', RequestManager::isAjax());
	
	if (!defined('IS_SCRIPT')) {
		RequestManager::detectSlowConnection();
	}
	Theme\Theme::initialize(function($desktop, $mobile = null, $basic = null, $amp = null) {
		$theme = $desktop;
		$ua_type = \RequestManager::getUserAgentType();
		if ($ua_type && $ua_type[0] === 'M') {
			// $theme = ($ua_type[1] == 'A') ? $mobile : $basic;
		}
		return $theme ?: $desktop;
	});
	if (!defined('IS_SCRIPT')) {
		$userAuth = \Auth\Session::getInstance();

		if (AJAX_REQUEST) {
			header('PageSpeedFilters: -add_instrumentation,-trim_urls');
		} else {
			if (ENV_DEVELOPMENT) {
				
			}
		}
			
		\SiteManager::registerHook('theme_init', function ($args) use($userAuth) {
			$theme = $args[0];
			$tpl = $theme->getTemplate();
			$tpl->user = &$userAuth;
			\SiteManager::registerHook('user_auth_status_change', function ($user) use($tpl) {
				$tpl->user = $user;
			});
			
			if (!AJAX_REQUEST) {
				SiteManager::registerHook(['template_before_generation', $tpl], function () use($theme, $tpl) {
					$renderStart = microtime(true);
					
					SiteManager::registerHook(['template_after_generation', $tpl], function () use($theme, $renderStart) {
						
					});
				});
	
			}
		});
	} else {
		header('X-Robots-Tag: noindex,nofollow,noarchive');
	}
} else {
	define('AJAX_REQUEST', false);
}
