<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Padiyara Vallikattu : <?=$category['category']?></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Padiyara Vallikattu Family Gallery, <?=$category['description']?>" />
<?php $this->render('theme::headContent');?>
<style type="text/css">
	.nav-thumbnail-list {
	    margin: 0;
	    width: 100%;
	}
	.nav-thumbnail-list > div {
	    margin: 0;
	    background: hsla(0,0%,100%,.2);
	    display: block;
	    padding: 4px;
	    -webkit-transition: box-shadow .3s;
	    transition: box-shadow .3s;
	    outline: none;
	}
	.nav-thumbnail-list > div > a {
	    position: relative;
	    display: block;
	    min-height: 200px;
	}
	.nav-thumbnail-list > div {
	    display: block;
	}
	.nav-thumbnail-list  img {
	    position: absolute;
	    top: 0;
	    left: 0;
	    height: 100%;
	    width: 100%;
	    margin: 0 auto;
	    right: 0;
	}
	.share-buttons {

	}	
	.share-buttons > .icon-wrapper {
		background-color: #ddd;
		border-radius: 50%;
		width: 30px;
		height: 30px;
		display: inline-block;
		color: #fff;
		text-align: center;
		line-height: 28px;
	}
	.share-buttons > .icon-wrapper:hover {
		background-color: #000;
	}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="container">
						<div class="text-center">
							<h1><?=$category['category']?></h1>
							<hr class="h-underline">
						</div>
				      	<div class="row p-3">
				      		<?php foreach($arImage as $key => $image):?>
								<div class="nav-thumbnail-list col-xs-12 col-sm-6 col-md-4 p-3">
									<div class="card p-1 ">
										<a href="#show" data-toggle="modal" data-target="#imageModal">
											<img class="card-img-top" src="<?=$image['image_pathname']?>" alt="<?=$image['caption']?>">
										</a>
										<div class="card-body">
											<h5 class="card-title"><?=$image['caption']?></h5>
											<p class="card-text"><?=$image['description']?></p>
											<div class="share-buttons">
												<span class="icon-wrapper">
													<i class="fa fa-facebook da-fw"></i>
												</span>
												<span class="icon-wrapper">
													<i class="fa fa-twitter da-fw"></i>
												</span>
												<span class="icon-wrapper">
													<i class="fa fa-google-plus da-fw"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
				      		<?php endforeach;?>
				      	</div>
			      	</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>


<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="carouselGallery" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
      					<?php foreach($arImage as $key => $image):?>
							<li data-target="#carouselGallery" data-slide-to="<?=$key?>" class="<?=($key === 0) ? 'active' : ''?>"></li>
		      			<?php endforeach;?>
					</ol>
					<div class="carousel-inner">
	      				<?php foreach($arImage as $key => $image):?>
							<div class="carousel-item <?=($key === 0) ? 'active' : ''?>">
								<img src="<?=$image['image_pathname']?>" alt="<?=$image['caption']?>" width='100%' height='100%'>
								<div class="carousel-caption d-none d-md-block">
									<h5><?=$image['caption']?></h5>
									<p><?=$image['description']?></p>
								</div>
							</div>
		      			<?php endforeach;?>
					</div>

					 <a class="carousel-control-prev" href="#carouselGallery" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselGallery" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<script>
	// $('#myModal').modal('toggle');
	$('#imageModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('New message to ' + recipient)
	  modal.find('.modal-body input').val(recipient)
	})
</script>

</div>
</body>
</html>
