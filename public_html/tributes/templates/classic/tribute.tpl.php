<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?=$tribute['page_title']?></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="<?=$tribute['page_description']?>" />
<?php $this->render('theme::headContent');?>
<style type="text/css">
	
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="container">
						<div class="text-center">
							<h1><?=$tribute['page_content_title']?></h1>
							<hr class="h-underline">
						</div>
				      	<div class="row p-3">
				      		<?=$tribute['page_content']?>
				      	</div>
				      	<div class="card p-3 text-muted font-weight-light font-italic">
				      		<p>
				      			To a few of the leading lights of padiyara vallikattu, who left us for their heavenly abode &amp; to those who are still with us, who helped us in running the kudumbayogam.
				      		</p>
				      	</div>
			      	</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>

<script>
	
</script>

</div>
</body>
</html>
