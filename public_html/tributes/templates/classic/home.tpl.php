<!DOCTYPE html>
<html lang="en">
<head>
<title>Tributes : Padiyara Vallikattu Family</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="To a few of the leading lights of Padiyara Vallikattu Family, who left us for their heavenly abode & to those who are still with us, who helped us in running the kudumbayogam" />
<?php $this->render('theme::headContent');?>
<style>
	
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div class="container">
		      	<div class="p-3">

					<div class="text-center">
						<span class="t-xlarge">Padiyara Vallikattu kudumbayogam</span>	
						<h1>TRIBUTES</h1>
						<hr class="h-underline">
					</div>
					
					<div class="card p-3 text-muted font-weight-light font-italic">
			      		<p>
			      			To a few of the leading lights of padiyara vallikattu, who left us for their heavenly abode &amp; to those who are still with us, who helped us in running the kudumbayogam.
			      		</p>
			      	</div>
			      	<div class="my-4">
						<ul class="list-unstyled">
			      			<?php foreach($arTribute as $key => $tribute):?>
								<li class="media">
									<div class="card p-3 my-2">
										
										<div class="media-body">
											<h5 class="mt-0 mb-3">
												<span class="fa fa-fire" style="color: #f49330;font-size: 25px;"></span> 
												<a href="<?=$tribute['page_pathname']?>">
													<?=$tribute['page_content_title']?>
												</a>
											</h5>
											<?=$tribute['page_description']?>
										</div>
									</div>
								</li>
			      			<?php endforeach;?>
						</ul>
					</div>
		      	</div>
	      	</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>