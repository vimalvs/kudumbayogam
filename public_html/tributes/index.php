<?php
include '../include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$mode = getCleanVar('mode', 'list');

if ($mode === 'list') {
	$tpl->setTemplate('home');
	$stmt = $pdbo->search("tribute", null, ['position', 'ASC']);
	$arTribute = $stmt ? $stmt->fetchAll() : [];

	$tpl->addData(compact('arTribute'));
} else if ($mode === 'tribute') {
	extractCleanVars('pathname');
	$tpl->setTemplate('tribute');
	$tribute = $pdbo->getRecord("tribute", ['page_key' => $pathname]);
	$tpl->data['tribute'] = $tribute;
	$tpl->addData(compact('category'));
}

$tpl->generate();
